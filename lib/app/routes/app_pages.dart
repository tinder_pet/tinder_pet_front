import 'package:get/get.dart';
import 'package:tinder_pet_front/app/modules/home/bindings/chatroom_binding.dart';
import 'package:tinder_pet_front/app/modules/home/bindings/register_binding.dart';
import 'package:tinder_pet_front/app/modules/home/views/auth/login.dart';
import 'package:tinder_pet_front/app/modules/home/views/auth/register.dart';
import 'package:tinder_pet_front/app/modules/home/views/chatroom/chatroom_view.dart';
import 'package:tinder_pet_front/app/modules/home/views/edit_pet.dart';
import 'package:tinder_pet_front/app/modules/home/views/file_picker.dart';
import 'package:tinder_pet_front/app/modules/home/views/information.dart';
import 'package:tinder_pet_front/app/modules/home/views/location_view.dart';
import 'package:tinder_pet_front/app/modules/home/views/my_pet.dart';
import 'package:tinder_pet_front/app/modules/home/views/pet_profile.dart';
import 'package:tinder_pet_front/app/modules/home/views/prelogin/onboard.dart';
import 'package:tinder_pet_front/app/modules/home/views/profile_screen/profile_screen.dart';
import 'package:tinder_pet_front/app/modules/home/views/profile_screen/user_profile.dart';
import 'package:tinder_pet_front/app/modules/home/views/screens/billing.dart';
import 'package:tinder_pet_front/app/modules/home/views/screens/detail_pet.dart';
import 'package:tinder_pet_front/app/modules/home/views/splash_screen.dart';
import 'package:tinder_pet_front/app/modules/home/views/subscription.dart';

import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/match/home_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.SPLASHSCREEN;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => const HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.SPLASHSCREEN,
      page: () => SplashScreen(),
    ),
    GetPage(
      name: _Paths.PRELOGIN,
      page: () => const PetsOnBoardingScreen(),
    ),
    GetPage(
      name: _Paths.LOGIN,
      page: () => const LoginPage(),
    ),
    GetPage(
      name: _Paths.REGISTER,
      page: () => Register(),
      binding: RegisterBinding(),
    ),
    GetPage(
      name: _Paths.FILEPICKERPET,
      page: () => FilePickerScreen(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.PETPROFILE,
      page: () => PetProfile(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.LOCATION,
      page: () => LocationExample(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.USERPROFILE,
      page: () => UserProfile(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.PROFILESCREEN,
      page: () => ProfileScreen(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.CHATROOM,
      page: () => ChatroomPage(),
      binding: ChatroomBinding(),
    ),
    GetPage(
      name: _Paths.SUBSCRIPTION,
      page: () => const SUBSCRIPTION(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.INFORMATION,
      page: () => const INFORMATION(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.MYPET,
      page: () => const MyPet(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.PETDETAILS,
      page: () => const Detail_Pet(
        pets: {},
      ),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.BILLING,
      page: () => const Billing(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.EDITPET,
      page: () => EditPet(
        pets: {},
      ),
      binding: HomeBinding(),
    ),
  ];
}
