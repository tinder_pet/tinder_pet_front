part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const HOME = _Paths.HOME;
  static const PRELOGIN = _Paths.PRELOGIN;
  static const LOGIN = _Paths.LOGIN;
  static const REGISTER = _Paths.REGISTER;
  static const FILEPICKERPET = _Paths.FILEPICKERPET;
  static const PETPROFILE = _Paths.PETPROFILE;
  static const LOCATION = _Paths.LOCATION;
  static const USERPROFILE = _Paths.USERPROFILE;
  static const PROFILESCREEN = _Paths.PROFILESCREEN;
  static const CHATROOM = _Paths.CHATROOM;
  static const SUBSCRIPTION = _Paths.SUBSCRIPTION;
  static const MYPET = _Paths.MYPET;
  static const PETDETAILS = _Paths.PETDETAILS;
  static const BILLING = _Paths.BILLING;
  static const EDITPET = _Paths.EDITPET;
  static const SPLASHSCREEN = _Paths.SPLASHSCREEN;
}

abstract class _Paths {
  _Paths._();
  static const HOME = '/home';
  static const PRELOGIN = '/prelogin';
  static const LOGIN = '/login';
  static const REGISTER = '/register';
  static const FILEPICKERPET = '/filepicker-pet';
  static const PETPROFILE = '/pet-profile';
  static const LOCATION = '/get-location';
  static const USERPROFILE = '/edit-profile';
  static const PROFILESCREEN = '/profile';
  static const CHATROOM = '/chatrooms';
  static const SUBSCRIPTION = '/subscription';
  static const INFORMATION = '/information';
  static const MYPET = '/my-pet';
  static const PETDETAILS = '/pet-details';
  static const BILLING = '/billing-details';
  static const EDITPET = '/edit-pet';
  static const SPLASHSCREEN = '/splash';
}
