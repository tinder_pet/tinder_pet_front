import 'package:get/get.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/userprofile_controller.dart';


class UserProfileBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<UserProfileController>(() => UserProfileController());
  }
}
