import 'package:get/get.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/petprofile_controller.dart';

class PetprofileBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PetprofileController>(
      () => PetprofileController(),
    );
  }
}
