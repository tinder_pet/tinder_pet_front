import 'package:get/get.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/register_controller.dart';

class RegisterBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<RegisterController>(() => RegisterController());
  }
}
