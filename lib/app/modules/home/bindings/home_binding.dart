import 'package:get/get.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/location_controller.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/home_controller.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/user_controller.dart'; 

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeController>(() => HomeController());
    Get.put<UserController>(UserController(), permanent: true);
    Get.lazyPut<LocationController>(() => LocationController());
    
  }
}
