import 'package:get/get.dart';
import 'package:tinder_pet_front/app/data/service/match_service.dart';
import 'package:flutter/material.dart'; // Import this for BuildContext

class MatchController extends GetxController {
  final MatchService matchService = MatchService();

  @override
  void onInit() {
    super.onInit();
  }

  void LikedPet(BuildContext context, int pet1, int pet2, bool like) {
    if (pet2 != 0) {
      matchService.interaction(context: context, pet1: pet1, pet2: pet2, isLiked: like);
    } else {
      print("User Not Click Like Yet");
    }
  }
}

