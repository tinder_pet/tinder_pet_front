import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tinder_pet_front/app/data/service/register_service.dart';

class RegisterController extends GetxController {
  final userController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final confirmPasswordController = TextEditingController();

  final RegisterService apiService = RegisterService(); 

  @override
  void onClose() {
    userController.dispose();
    emailController.dispose();
    passwordController.dispose();
    confirmPasswordController.dispose();
    super.onClose();
  }

  void navigateToLogin() {
    Get.toNamed('/login');
  }
  bool validateInput(String username, String email, String password, String confirmPassword) {
    if (username.isEmpty || username.length < 4 || username.length > 20) {
      Get.snackbar('Error', 'Username must be between 4 and 20 characters.');
      return false;
    }

    if (email.isEmpty || !email.contains('@')) {
      Get.snackbar('Error', 'Please enter a valid email address.');
      return false;
    }

    if (password.isEmpty || password.length < 8 || password.length > 20) {
      Get.snackbar('Error', 'Password must be between 8 and 20 characters.');
      return false;
    }

    if (password != confirmPassword) {
      Get.snackbar('Error', 'Passwords do not match.');
      return false;
    }

    return true;
  }
 void register() async {
    final username = userController.text;
    final email = emailController.text;
    final password = passwordController.text;
    final confirmPassword = confirmPasswordController.text;

    if (!validateInput(username, email, password, confirmPassword)) {
      return;
    }

    try {
      await apiService.registerUser(
        username: username,
        email: email,
        password: password,
      );
      Get.toNamed('/login');
    } catch (e) {
      print('Registration failed: $e');
      Get.snackbar('Error', 'Registration failed: $e');
    }
  }
}

