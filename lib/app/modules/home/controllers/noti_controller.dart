import 'dart:async';

import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:tinder_pet_front/app/data/service/socket_io_client.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/user_controller.dart';
import 'package:tinder_pet_front/app/routes/app_pages.dart';

class NotificationController extends GetxController {
  final NotificationService socketService = NotificationService();
  final UserController userController = Get.put(UserController());
  bool _isSnackBarShowing = false;
  Timer? _snackBarTimer;

  @override
  void onInit() {
    super.onInit();
    Future.delayed(Duration.zero, () {
    });

    setupNotificationSocket();
  }

  void setupNotificationSocket() {
    socketService.setupNotificationSocket();

    socketService.notificationSocket.on('connect', (_) {
      print('Connected to notification server');
    });

    socketService.notificationSocket.on('newMessage', (data) {
      handleNewMessage(data);
    });
  }

  void handleNewMessage(Map<String, dynamic> data) {
    if (data['userID'] != userController.currentUser['userID']) {
      if (_isSnackBarShowing) return;

      Get.snackbar(
        "New Message",
        "Hello !! You have a new message in chat",
        snackPosition: SnackPosition.TOP,
      );

      _isSnackBarShowing = true;

      _snackBarTimer = Timer(Duration(seconds: 5), () {
        _isSnackBarShowing = false;
        _snackBarTimer = null;
      });
    }
  }

  @override
  void onClose() {
    socketService.notificationSocket.disconnect();
    super.onClose();
  }
}
