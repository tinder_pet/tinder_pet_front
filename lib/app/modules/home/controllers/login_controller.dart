import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:tinder_pet_front/app/data/service/login_service.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/home_controller.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/location_controller.dart';
import 'package:tinder_pet_front/app/routes/app_pages.dart';

class LoginController extends GetxController {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  var isLoading = false.obs;
  final LocationController locationController = Get.find<LocationController>();

  final LoginService apiService = LoginService();
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onClose() {
    emailController.dispose();
    passwordController.dispose();
    super.onClose();
  }

void login(BuildContext context) async {
  if (emailController.text.isEmpty || passwordController.text.isEmpty) {
    Get.snackbar('Error', 'Please enter both email and password',
        snackPosition: SnackPosition.BOTTOM);
    return;
  }

  isLoading.value = true;

  try {
    final userData = await apiService.loginUser(
      email: emailController.text,
      password: passwordController.text,
    );

    if (userData != null) {
      final box = GetStorage();
      final pets = box.read('pets');
      print('User Pet: $pets');

      // ตรวจสอบว่า LoginController ยังอยู่ก่อนที่จะเรียกใช้งาน
      if (Get.isRegistered<LoginController>()) {
        bool hasLocation = await locationController.hasLocation(userData['userID']);

        if (!hasLocation) {
          bool granted = await locationController.getCurrentLocation(userData['userID'], context);

          if (!granted) {
            Get.offAllNamed(Routes.LOCATION);
            return;
          }
        }

        if (pets == null || pets.isEmpty) {
          Get.offAllNamed(Routes.FILEPICKERPET);
        } else {
          Get.offAllNamed(Routes.HOME);          
        }
      }
    } else {
      Get.snackbar('Login Failed', 'Invalid credentials',
          snackPosition: SnackPosition.BOTTOM);
    }
  } catch (e) {
    Get.snackbar('Login Failed', e.toString(),
        snackPosition: SnackPosition.BOTTOM);
  } finally {
    isLoading.value = false;
  }
}



}
