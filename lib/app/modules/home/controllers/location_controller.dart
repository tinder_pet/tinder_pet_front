import 'package:get/get.dart';
import 'package:geolocator/geolocator.dart';
import 'package:tinder_pet_front/app/data/service/userprofile_service.dart';
import 'package:tinder_pet_front/app/routes/app_pages.dart'; 
import 'package:flutter/material.dart';

class LocationController extends GetxController {
  var location = 'Unknown'.obs;
  final UserProfileService userProfileService = UserProfileService();
Future<bool> hasLocation(int userId) async {
  // ดึงข้อมูลตำแหน่งจาก API หรือฐานข้อมูลที่คุณมี
  final locationData = await userProfileService.getUserLocation(userId);
  return locationData != null; // ถ้ามีข้อมูลตำแหน่งจะคืนค่า true
}

Future<bool> getCurrentLocation(int userId, BuildContext context) async {
  print("Getting current location...");
  bool serviceEnabled = await Geolocator.isLocationServiceEnabled();
  if (!serviceEnabled) {
    location.value = 'Location services are disabled.';
    print(location.value);
    return false;
  }

  LocationPermission permission = await Geolocator.checkPermission();
  if (permission == LocationPermission.denied) {
    await _showPermissionDialog(context);
    return false;
  }

  if (permission == LocationPermission.deniedForever) {
    location.value = 'Location permissions are permanently denied.';
    print(location.value);
    Get.toNamed(Routes.LOCATION);
    return false;
  }

  try {
    Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    location.value = 'Lat: ${position.latitude}, Long: ${position.longitude}';
    print(location.value);

    await userProfileService.updateUserLocation(
      userId: userId,
      latitude: position.latitude,
      longitude: position.longitude,
    );
    print('Location updated to API for userId: $userId');
    return true; // คืนค่า true เมื่อได้ตำแหน่ง
  } catch (e) {
    print('Error getting location or updating to API: $e');
    return false; // คืนค่า false ถ้ามีข้อผิดพลาด
  }
}


  Future<void> _showPermissionDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Location Permission'),
          content: Text('Please enable location access to use this feature.'),
          actions: [
            TextButton(
              child: Text('OK'),
              onPressed: () {
                Geolocator.requestPermission().then((value) {
                  Navigator.of(context).pop(); // ปิด dialog
                });
              },
            ),
          ],
        );
      },
    );
  }
}
