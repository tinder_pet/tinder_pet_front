import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;
import 'package:tinder_pet_front/app/modules/home/controllers/noti_controller.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/user_controller.dart';
import 'package:http/http.dart' as http;

class ChatController extends GetxController {
  var messages = <dynamic>[].obs;
  var isLoading = false.obs;
  final messageController = TextEditingController();
  late IO.Socket socket;
  final UserController userController = Get.find<UserController>();

  @override
  void onInit() {
    super.onInit();
    Get.delete<NotificationController>();
  }
    @override
  void onClose() {
    disconnectSocket();
    messageController.dispose();
    Get.delete<NotificationController>();
    Get.delete<UserController>();

    super.onClose();
  }
  void disconnectSocket() {
    if (socket != null && socket.connected) {
      socket.disconnect();
      print('Socket disconnected');
    }
  }
  void connectSocket(String chatRoomId) {
    socket = IO.io('http://localhost:3000', <String, dynamic>{
      'transports': ['websocket'],
      'autoConnect': false,
      'query': {'chatRoomID': chatRoomId},
    });

    socket.connect();

    socket.onConnect((_) {
      print('Connected to socket server');
    });

    socket.onDisconnect((_) {
      print('Disconnected from socket server');
    });

    socket.on('receiveMessage', (data) {
      _handleIncomingMessage(data);
    });
  }

  void _handleIncomingMessage(data) {
    if (data != null && data['content'] != null) {
      final message = {
        'content': data['content'],
        'chatRoomId': data['chatRoom']?['roomID'],
        'userId': data['sender']?['userID'],
        'sentAt': DateTime.now().toIso8601String(),
      };
      messages.add(message);
      print('Received message: $message');
    } else {
      print('Received null message or content, not adding to list');
    }
  }

  Future<void> sendMessage(String chatRoomId) async {
    final content = messageController.text.trim();
    if (content.isEmpty) {
      print('Message content is empty');
      return;
    }

    final currentUser = userController.getUser;
    if (currentUser == null) {
      print('Current user is null, cannot send message');
      return;
    }

    final userId = currentUser['userID'];
    if (userId == null) {
      print('User ID is null, cannot send message');
      return;
    }

    final message = {
      "content": content,
      "chatRoomID": chatRoomId,
      "userID": userId,
      'sentAt': DateTime.now().toIso8601String(),
    };

    try {
      socket.emit('sendMessage', message);
      messages.add({
        'content': content,
        'userId': userId,
        'sentAt': message['sentAt'],
      });
      messageController.clear();
      print('Message sent: $message');
    } catch (e) {
      print('Error sending message: $e');
    }
  }

  Future<void> fetchChatRoomData(String chatRoomId) async {
    isLoading.value = true;
    try {
      final response = await http.get(Uri.parse('http://localhost:3000/chatroom/$chatRoomId'));
      if (response.statusCode == 200) {
        final data = json.decode(response.body);
        messages.assignAll(data['messages']);
      } else {
        // Handle the error response
        print('Failed to load chat room data: ${response.statusCode}');
      }
    } catch (e) {
      print('Error fetching chat room data: $e');
    } finally {
      isLoading.value = false;
    }
  }

  Widget buildMessageBubble(String message, bool isSender, String sentAt) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 10),
      child: Align(
        alignment: isSender ? Alignment.centerRight : Alignment.centerLeft,
        child: Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: isSender ? Colors.blue[200] : Colors.grey[300],
            borderRadius: BorderRadius.circular(15),
          ),
          child: Column(
            crossAxisAlignment: isSender ? CrossAxisAlignment.end : CrossAxisAlignment.start,
            children: [
              Text(
                message,
                style: TextStyle(color: isSender ? Colors.white : Colors.black),
              ),
              SizedBox(height: 5),
              Text(
                _formatTimestamp(sentAt),
                style: TextStyle(fontSize: 12, color: Colors.black54),
              ),
            ],
          ),
        ),
      ),
    );
  }

  String _formatTimestamp(String timestamp) {
    final dateTime = DateTime.parse(timestamp);
    final formattedTime = "${dateTime.hour}:${dateTime.minute}"; // ปรับแต่งเวลาให้เหมาะสม
    return formattedTime;
  }


}
