import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tinder_pet_front/app/data/service/home_service.dart';
import 'package:tinder_pet_front/app/data/service/socket_io_client.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/filter_controller.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/location_controller.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/user_controller.dart';
import 'package:tinder_pet_front/app/routes/app_pages.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;
import '../../../data/service/type/pet.dart';

class HomeController extends GetxController {
  final FilterController controller = Get.put(FilterController());
  final UserController userController = Get.put(UserController());
  final LocationController locationController = Get.put(LocationController());
  final HomeService apiService = HomeService();
  var isLoading = false.obs;
  var isFetching = false.obs;
  var pets = <dynamic>[].obs;
  var pet = <Pet>[].obs;
  var selectedTraits = <String>[].obs;
  final List<String> allTraitsForDog = [
    'พลังงานสูง',
    'เป็นมิตร',
    'รักความปลอดภัย',
    'ชอบอยู่เงียบๆ'
  ];
  final List<String> allTraitsForCat = [
    'ขี้เล่นและซุกซน',
    'เป็นตัวของตัวเองสูง',
    'เป็นมิตร',
    'สงบ'
  ];
  var hasNewMessage = false.obs;

  var isLookingForFriend = false.obs;
  var isLookingForMate = false.obs;
  var ageRange = RangeValues(1, 10).obs;
  var coatColors = ['Black', 'White', 'Brown', 'Golden'].obs;
  var selectedCoatColors = <String>[].obs;
  var isIndoor = false.obs;
  var isOutdoor = false.obs;
  var selectedDogTraits = <String>[].obs;
  Map<String, double> weights = {
    'breed': 0.4,
    'color': 0.1,
    'age': 0.1,
    'weight': 0.1,
    'furLength': 0.1,
    'behavior': 0.2,
  };
  var dogTraits =
      ['พลังงานสูง', 'เป็นมิตร', 'รักความปลอดภัย', 'ชอบอยู่เงียบๆ'].obs;

  // Cat Traits
  var selectedCatTraits = <String>[].obs;
  var catTraits =
      ['ขี้เล่นและซุกซน', 'เป็นตัวของตัวเองสูง', 'เป็นมิตร', 'สงบ'].obs;
  @override
  void onInit() {
    super.onInit();
    Future.delayed(Duration.zero, () {
      checkUserAndNavigate();
    });

  }
 @override
  void onClose() {
    super.onClose();
  }
  void checkUserAndNavigate() {
    final userId = userController.getUser['userID'];
    if (userId != null) {
      final userPets = userController.getUser['pets'] ?? [];
      if (userPets.isEmpty) {
        print('User has no pets. Navigating to FILEPICKERPET...');
        Get.toNamed(Routes.FILEPICKERPET);
      } else {}
    } else {
      print('User ID is null.');
    }
  }

  void handleSwipeLeft(Pet pet) {
    print('Disliked: ${pet.petName}');
  }

  void handleSwipeRight(Pet pet) {
    print('Liked: ${pet.petName}');
  }

  Future<void> loadNearbyPets(int userId) async {
    try {
      var fetchedPets =
          await apiService.fetchNearbyPets(userId, weights: weights);
      if (fetchedPets.isNotEmpty) {
        pets.assignAll(fetchedPets);
      } else {
        pets.clear(); 
      }
    } catch (error) {
      print('Error fetching nearby pets: $error');
    }
  }

Future<List<Pet>> fetchNearbyPets(int userId, {Map<String, double>? weights}) async {
  isLoading.value = true;
  print("Fetching nearby pets for user ID: $userId with weights: $weights");

  try {
    List<Pet> fetchedPets = await apiService.fetchNearbyPets(userId, weights: weights);
    print("Fetched pets: ${fetchedPets.length}");
    print("Response body: $fetchedPets"); 

    if (fetchedPets.isEmpty) {
      pets.clear();
      print("No pets found. Clearing the pet list.");
      return [];
    }

    List<Pet> filteredPets = _filterPets(fetchedPets);
    print("Filtered pets: ${filteredPets.length}");

    if (filteredPets.isEmpty) {
      print("No pets match the filtering criteria.");
    }

    pets.assignAll(filteredPets);
    return filteredPets;
  } catch (error) {
    print("Error fetching pets: $error");
    return [];
  } finally {
    isLoading.value = false;
    print("Finished fetching pets.");
  }
}


List<Pet> _filterPets(List<Pet> petsList) {
  String lookingFor = controller.lookingFor.value;
  print("Filtering pets based on lookingFor: $lookingFor");

  if (lookingFor == 'Both') {
    print("Returning all pets without filtering.");
    return petsList;
  }

  if (lookingFor == 'Find Friends') {
    print("Filtering for neutered pets.");
    return petsList.where((pet) => 
      pet.sterileState == 'Neutered' && pet.objective != 'Find a Breeding Partner'
    ).toList();
  }

  if (lookingFor == 'Find a Breeding Partner') {
    print("Filtering for pets looking for a breeding partner.");
    return petsList.where((pet) => 
      pet.objective == 'Find a Breeding Partner' 
    ).toList();
  }

  print("Unknown lookingFor value: $lookingFor. Returning unfiltered pets.");
  return petsList;
}

  void resetFilters() {
    isLookingForFriend.value = false;
    isLookingForMate.value = false;
    ageRange.value = RangeValues(1, 10);
    selectedCoatColors.clear();
    isIndoor.value = false;
    isOutdoor.value = false;
    selectedDogTraits.clear();
    selectedCatTraits.clear();
  }

void refreshData() async {
  if (!isFetching.value) {
    isFetching.value = true;
    try {
      final userId = userController.getUser['userID']; 
      if (userId != null) {
        await loadNearbyPets(userId); 
      } else {
        print("User ID is null. Cannot refresh data.");
      }
    } catch (e) {
      print("Error fetching data: $e");
    } finally {
      isFetching.value = false;
    }
  }
}

  

}
