import 'dart:convert';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:http/http.dart' as http;

class UserController extends GetxController {
  var currentUser = <String, dynamic>{}.obs;

  @override
  void onInit() {
    super.onInit();
    loadUserData(); // โหลดข้อมูลผู้ใช้เมื่อเริ่มต้น
  }

  void loadUserData() {
    final box = GetStorage();
    final userId = box.read('userID');
    final userName = box.read('userName');
    final email = box.read('email');
    final userDes = box.read('userDes');
    final phoneNumber = box.read('phoneNumber');
    final sex = box.read('sex');
    final statusMember = box.read('statusMember');
    final userImage = box.read('userImage');
    final contact = box.read('contact');
    final age = box.read('age');
    final nationality = box.read('nationality');
    final religion = box.read('religion');
    final address = box.read('address');
    final deletedAt = box.read('deletedAt');
    final latitude = box.read('latitude');
    final longitude = box.read('longitude');
    final pets = List<Map<String, dynamic>>.from(box.read('pets') ?? []);
    final members = List<Map<String, dynamic>>.from(box.read('members') ?? []);

    if (userId != null && userName != null && email != null) {
      String province = 'Unknown Province'; // ค่าปริยาย
      String district = 'Unknown District'; // ค่าปริยาย
      String subDistrict = 'Unknown Subdistrict'; // ค่าปริยาย

      // แยก address เป็นจังหวัด, อำเภอ, และตำบล
      if (address != null) {
        final addressParts = address.split(','); // แยกที่อยู่ด้วย ','
        print('Split address: $addressParts'); // แสดงข้อมูลหลังแยก
        if (addressParts.length >= 3) {
          province = addressParts[0].trim(); // จังหวัด
          district = addressParts[1].trim(); // อำเภอ
          subDistrict = addressParts[2].trim(); // ตำบล
          print(
              'SubDistrict: $subDistrict, District: $district, Province: $province');
        } else {
          print('Address does not contain enough parts: $address');
        }
      }

      currentUser.value = {
        'userID': userId,
        'userName': userName,
        'email': email,
        'userDes': userDes,
        'phoneNumber': phoneNumber,
        'sex': sex,
        'statusMember': statusMember,
        'userImage': userImage,
        'contact': contact,
        'age': age,
        'nationality': nationality,
        'religion': religion,
        'address': address,
        'province': province, // เก็บจังหวัด
        'district': district, // เก็บอำเภอ
        'subDistrict': subDistrict, // เก็บตำบล
        'deletedAt': deletedAt,
        'latitude': latitude,
        'longitude': longitude,
        'pets': pets,
        'members': members
      };
    } else {
      print('User data is null');
    }
  }

  // void setUser(Map<String, dynamic> userData) {
  //   currentUser.value = userData;
  // }

  void setUser(Map<String, dynamic> userData) {
    currentUser.value = userData;
  }

  Map<String, dynamic> get getUser => currentUser.value;

  // ฟังก์ชันเพื่อเคลียร์ข้อมูลผู้ใช้
  void clearUser() {
    currentUser.clear();
  }

  Future<void> fetchUser() async {
    final box = GetStorage();
    final userId = box.read('userID');

    if (userId != null) {
      final url = Uri.parse('http://localhost:3000/user/$userId');

      try {
        final response = await http.get(url);

        if (response.statusCode == 200) {
          // ถ้าสำเร็จ, แปลง JSON เป็น Map
          final Map<String, dynamic> userData = json.decode(response.body);
          setUser(userData); // อัปเดตข้อมูลผู้ใช้
          print('User data loaded successfully: $userData');

          // อัปเดต GetStorage ด้วยข้อมูลล่าสุด
          userData.forEach((key, value) async {
            await box.write(key, value);
          });
        } else {
          print(
              'Failed to load user data: ${response.statusCode} - ${response.body}');
        }
      } catch (e) {
        print('Error fetching user data: $e');
      }
    } else {
      print('User ID is null');
    }
  }
}
