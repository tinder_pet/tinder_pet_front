
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
class FilterController extends GetxController {
var lookingFor = 'none'.obs;

var breedScore = 0.obs;
var colorScore = 0.obs;
var ageScore = 0.obs;
var weightScore = 0.obs;
var furLengthScore = 0.obs;
var behaviorScore = 0.obs;
  void selectLookingFor(String type) {
    lookingFor.value = type;
    print(lookingFor);
  }
void updateSelectedLevel(String type, int level) {
  switch (type) {
    case 'breed':
      breedScore.value = level; 
      break;
    case 'color':
      colorScore.value = level;
      break;
    case 'age':
      ageScore.value = level; 
      break;
    case 'weight':
      weightScore.value = level;
      break;
    case 'furLength':
      furLengthScore.value = level; 
      break;
    case 'behavior':
      behaviorScore.value = level; 
      break;
  }
}

int calculateTotalScore() {
  return breedScore.value +
         colorScore.value +
         ageScore.value +
         weightScore.value +
         furLengthScore.value +
         behaviorScore.value;
}
List<String> getScoresWithTotal() {

  int totalScore = calculateTotalScore();


  if (totalScore == 0) {
    return [
      'breed: ${breedScore.value} / $totalScore',
      'color: ${colorScore.value} / $totalScore',
      'age: ${ageScore.value} / $totalScore',
      'weight: ${weightScore.value} / $totalScore',
      'furLength: ${furLengthScore.value} / $totalScore',
      'behavior: ${behaviorScore.value} / $totalScore',
    ];
  }

  return [
    'breed: ${(breedScore.value / totalScore).toStringAsFixed(2)}',
    'color: ${(colorScore.value / totalScore).toStringAsFixed(2)}',
    'age: ${(ageScore.value / totalScore).toStringAsFixed(2)}',
    'weight: ${(weightScore.value / totalScore).toStringAsFixed(2)}',
    'furLength: ${(furLengthScore.value / totalScore).toStringAsFixed(2)}',
    'behavior: ${(behaviorScore.value / totalScore).toStringAsFixed(2)}',
  ];
}


}

