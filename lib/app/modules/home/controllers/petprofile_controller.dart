import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tinder_pet_front/app/data/service/petprofile_service.dart';

class PetprofileController extends GetxController {
  var nameController = TextEditingController();
  var desController = TextEditingController();
  var ageController = TextEditingController();
  var weightController = TextEditingController();
  var healthHistoryController = TextEditingController();
  var specialBehaviorsController = TextEditingController();
  var objectiveController = TextEditingController();
  var selectedColor = ''.obs;
  var selectedBreed = ''.obs;
  var selectedSex = ''.obs;
  var selectedFur = ''.obs;
  var selectedVaccines = <String>[].obs;
  var selectedFoods = <String>[].obs;

  var parentingStatus = ''.obs;
  var sterileState = ''.obs;
  var selectedobjective = ''.obs;

  void setPetData(Map<String, dynamic> pets) {
    nameController.text = pets['petName'] ?? '';
    desController.text = pets['petDes'] ?? '';
    ageController.text = pets['age'] ?? '';
    weightController.text = pets['weight'] ?? '';
    // selectedColor.text = pets['color'] ?? '';
    healthHistoryController.text = pets['healthHistory'] ?? '';
    specialBehaviorsController.text = pets['specialBehaviors'] ?? '';

    selectedSex.value = pets['sex'] ?? '';
    selectedBreed.value = pets['breed'] ?? '';
    selectedFur.value = pets['furLength'] ?? '';
    selectedColor.value = pets['color'] ?? '';

    // อัปเดตคอนโทรลเลอร์
    update();
  }

  Uint8List? images;
  String? fileName;

  // final RxList<String> selectedFoods = <String>[].obs;

  // final RxList<String> selectedVaccines = <String>[].obs;

  final PetprofileService apiService = PetprofileService();

  void setSex(String value) {
    selectedSex.value = value;
  }

  void setFur(String value) {
    selectedFur.value = value;
  }

  void setColor(String value) {
    selectedColor.value = value;
  }

  void setBreed(String value) {
    selectedBreed.value = value;
  }

  void setParentingStatus(String value) {
    parentingStatus.value = value;
  }

  void setSterileState(String value) {
    sterileState.value = value;
  }

  void setObjective(String value) {
    selectedobjective.value = value;
  }

  void setImage(Uint8List imageBytes, String name) {
    images = imageBytes;
    fileName = name;
    update();
  }

  void register(int userID, String? animalType) async {
    String userIDString = userID.toString();
    final ID = userID;
    final petname = nameController.text;
    final des = desController.text;
    final sex = selectedSex.value;
    final age = ageController.text;
    final weight = weightController.text;
    final color = selectedColor.value;
    final fur = selectedFur.value;
    final breed = selectedBreed.value;
    final species = animalType;

    if (images == null) {
      Get.snackbar('Error', 'Please select an image for the pet');
      return;
    }

    if (petname.isEmpty ||
        sex.isEmpty ||
        age.isEmpty ||
        weight.isEmpty ||
        color.isEmpty ||
        fur.isEmpty ||
        breed.isEmpty ||
        species!.isEmpty) {
      print("$petname $sex $age $weight $color $fur $breed $species");
      Get.snackbar('Error', 'Please fill in all required fields');
      return;
    }

    try {
      await apiService.registerPet(
          userid: userIDString,
          petname: petname,
          petdes: des,
          petsex: sex,
          petage: age,
          petweight: weight,
          petcolor: color,
          petfur: fur,
          petbreed: breed,
          petImages: images!,
          species: species);
      clearForm();
    } catch (e) {
      print('Registration failed: $e');
      Get.snackbar('Error', 'Failed to register pet: $e');
    }
  }

  void updatePetProfile(int petId) async {
    final petname = nameController.text;
    final des = desController.text;
    final sex = selectedSex.value;
    final age = ageController.text;
    final weight = weightController.text;
    final color = selectedColor.value;
    final fur = selectedFur.value;
    final breed = selectedBreed.value;
    // final species = '';

    final vaccine = selectedVaccines.value.join(", ");
    final foodsEaten = selectedFoods.value.join(", ");

    // ประกาศตัวแปรที่ใช้งาน
    final parentingStatus = this.parentingStatus.value; // ใช้ค่าที่กำหนดไว้
    final sterileState = this.sterileState.value; // ใช้ค่าที่กำหนดไว้

    final healthHistory = healthHistoryController.text;
    final specialBehaviors = specialBehaviorsController.text;
    final objective = selectedobjective.value;

    print("petname : ${petname}");
    print("des : ${des}");
    print("sex : ${sex}");
    print("age : ${age}");
    print("weight : ${weight}");
    print("color : ${color}");
    print("fur : ${fur}");
    print("breed : ${breed}");
    print("vaccine : ${vaccine}");
    print("foodsEaten : ${foodsEaten}");
    print("parentingStatus : ${parentingStatus}");
    print("sterileState : ${sterileState}");
    print("healthHistory : ${healthHistory}");
    print("specialBehaviors : ${specialBehaviors}");
    print("objective ${objective}");

    // print("species : ${species}");

    if (petname.isEmpty ||
            sex.isEmpty ||
            age.isEmpty ||
            weight.isEmpty ||
            color.isEmpty ||
            fur.isEmpty ||
            breed.isEmpty
        // species.isEmpty
        ) {
      Get.snackbar('Error', 'Please fill in all required fields');
      return;
    }

    try {
      await apiService.updatePet(
        petId: petId,
        petname: petname,
        petdes: des,
        petsex: sex,
        petage: age,
        petweight: weight,
        petcolor: color,
        petfur: fur,
        petbreed: breed,
        vaccine: vaccine,
        foodsEaten: foodsEaten,
        parentingStatus: parentingStatus,
        sterileState: sterileState,
        healthHistory: healthHistory,
        specialBehaviors: specialBehaviors,
        objective: objective,
      );
      clearForm();
    } catch (e) {
      Get.snackbar('Error', 'Failed to update pet: $e');
    }
  }

  void clearForm() {
    nameController.clear();
    desController.clear();
    ageController.clear();
    weightController.clear();
    selectedColor.value = '';
    selectedBreed.value = '';
    selectedSex.value = '';
    images = null;
    fileName = null;
    update();
  }

  @override
  void onClose() {
    nameController.dispose();
    desController.dispose();
    ageController.dispose();
    weightController.dispose();
    super.onClose();
  }
}
