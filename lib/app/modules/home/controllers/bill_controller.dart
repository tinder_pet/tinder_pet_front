import 'dart:convert';

import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:get_storage/get_storage.dart';
import 'package:http/http.dart' as http;

class BillController extends GetxController {
  var currentBill =
      <Map<String, dynamic>>[].obs; // ใช้เป็น List<Map<String, dynamic>>

  @override
  void onClose() {
    super.onClose();
  }

  Future<void> fetchMember() async {
    final box = GetStorage();
    final userId = box.read('userID');
    print("userId :  $userId");
    if (userId != null) {
      final url = Uri.parse('http://localhost:3000/member/user/$userId');
      print("url :  $url");
      try {
        final response = await http.get(url);
        print("statusCode ${response.statusCode}");
        if (response.statusCode == 200) {
          // ถ้าสำเร็จ, แปลง JSON เป็น Map
          final List<dynamic> userData = json.decode(response.body);
          if (userData.isNotEmpty) {
            currentBill.value = List<Map<String, dynamic>>.from(
                userData); // อัปเดต currentBill ด้วยข้อมูลทั้งหมด
          }
          print('Member data loaded successfully: $userData');
        } else {
          print(
              'Failed to load member data: ${response.statusCode} - ${response.body}');
        }
      } catch (e) {
        print('Error fetching member data: $e');
      }
    } else {
      print('User ID is null in bill_controller');
    }
  }
}
