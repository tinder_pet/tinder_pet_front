import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:thai_region/thai_region.dart';
import 'package:tinder_pet_front/app/data/service/userprofile_service.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/user_controller.dart';

class UserProfileController extends GetxController {
  // Reactive variables for user information
  var userDes = ''.obs;
  var phoneNumber = ''.obs;
  var religion = ''.obs;
  var nationality = ''.obs;
  var contact = ''.obs;
  var selectedSex = ''.obs;

  // Reactive variables for selected location
  var selectedProvince = Rxn<Province>();
  var selectedDistrict = Rxn<District>();
  var selectedSubDistrict = Rxn<SubDistrict>();

  // Service and UserController instances
  late final UserProfileService userProfileService;
  final UserController userController = Get.put(UserController());

  @override
  void onInit() {
    super.onInit();
    userProfileService = Get.put(UserProfileService());
    fetchUserProfile();
  }

  // User profile variables
  var username = ''.obs;
  var userdes = ''.obs;
  var userid = 0.obs;

  var selectedImage = Rxn<String>(); // เปลี่ยนเป็น String

  // Fetch user profile data
  void fetchUserProfile() async {
    try {
      final userId = userController.currentUser['userID'];
      final userName = userController.currentUser['userName'];
      final userDes = userController.currentUser['userDes'] ?? 'Hello';

      if (userId != null && userName != null) {
        final profile = await userProfileService.getUser(
          username: userName,
          userdes: userDes,
          userid: userId,
        );

        username.value = profile['userName'] ?? 'Name';
        userdes.value = profile['userDes'] ?? 'Hello';
        userid.value = profile['userID'] ?? 1;

        final imageUrl = profile['userImage'];

        if (imageUrl != null && imageUrl.isNotEmpty) {
          selectedImage.value = imageUrl;
          userController.currentUser['userImage'] = imageUrl;
          print(
              'Selected Image URL: $selectedImage'); // ตรวจสอบ URL ของภาพที่เลือก
        }
      } else {
        print('Error: userID or userName is null.');
      }
    } catch (e) {
      print('Error fetching user profile: $e');
    }
  }

  void updateUserName(String newUsername) async {
    if (newUsername.isEmpty) {
      print('Username cannot be empty.');
      return;
    }

    try {
      await userProfileService.updateUserName(
        userId: userid.value,
        newUsername: newUsername,
      );
      username.value = newUsername;
      print('Username updated successfully.');

      // อัปเดตข้อมูลใน GetStorage
      final box = GetStorage();
      await box.write('userName', newUsername);

      // อัปเดต currentUser ใน UserController
      userController.currentUser['userName'] = newUsername;
      userController.setUser(userController.currentUser.value);
    } catch (e) {
      print('Error updating username: $e');
    }
  }

  void updateUserDescription(String newDescription) async {
    if (newDescription.isEmpty) {
      print('Description cannot be empty.');
      return;
    }

    try {
      await userProfileService.updateUserDescription(
        userId: userid.value,
        newDescription: newDescription,
      );
      userdes.value = newDescription;
      print('Description updated successfully.');

      // อัปเดตข้อมูลใน GetStorage
      final box = GetStorage();
      await box.write('userDes', newDescription);

      // อัปเดต currentUser ใน UserController
      userController.currentUser['userDes'] = newDescription;
      userController.setUser(userController.currentUser.value);
    } catch (e) {
      print('Error updating user description: $e');
    }
  }

  // Update user profile information
  Future<void> updateUserProfile() async {
    if (selectedSex.value.isEmpty ||
        phoneNumber.value.isEmpty ||
        religion.value.isEmpty ||
        nationality.value.isEmpty ||
        contact.value.isEmpty ||
        username.value.isEmpty) {
      print('ทุกฟิลด์ในโปรไฟล์ผู้ใช้ต้องถูกกรอก');
      return;
    }

    try {
      await userProfileService.updateUserInfo(
        userId: userid.value,
        sex: selectedSex.value,
        phoneNumber: phoneNumber.value,
        religion: religion.value,
        nationality: nationality.value,
        contact: contact.value,
      );
      print('อัปเดตโปรไฟล์สำเร็จ');

      // อัปเดตข้อมูลใน GetStorage
      final box = GetStorage();
      await box.write('sex', selectedSex.value);
      await box.write('phoneNumber', phoneNumber.value);
      await box.write('religion', religion.value);
      await box.write('nationality', nationality.value);
      await box.write('contact', contact.value);

      // อัปเดต currentUser ใน UserController
      userController.currentUser['sex'] = selectedSex.value;
      userController.currentUser['phoneNumber'] = phoneNumber.value;
      userController.currentUser['religion'] = religion.value;
      userController.currentUser['nationality'] = nationality.value;
      userController.currentUser['contact'] = contact.value;
      userController.setUser(userController.currentUser.value);
    } catch (e) {
      print('เกิดข้อผิดพลาดในการอัปเดตโปรไฟล์: $e');
    }
  }

  // Update user address
  Future<void> updateUserAddress(String combinedAddress) async {
    try {
      await userProfileService.updateUserAddress(
        userId: userid.value,
        address: combinedAddress,
        province: selectedProvince.value?.name.th,
        district: selectedDistrict.value?.name.th,
        subDistrict: selectedSubDistrict.value?.name.th,
      );
      print('Address updated successfully: $combinedAddress');

      // อัปเดตข้อมูลใน GetStorage
      final box = GetStorage();
      await box.write('address', combinedAddress);
      await box.write(
          'province', selectedProvince.value?.name.th ?? 'Unknown Province');
      await box.write(
          'district', selectedDistrict.value?.name.th ?? 'Unknown District');
      await box.write('subDistrict',
          selectedSubDistrict.value?.name.th ?? 'Unknown Subdistrict');

      // อัปเดต currentUser ใน UserController
      userController.currentUser['address'] = combinedAddress;
      userController.currentUser['province'] =
          selectedProvince.value?.name.th ?? 'Unknown Province';
      userController.currentUser['district'] =
          selectedDistrict.value?.name.th ?? 'Unknown District';
      userController.currentUser['subDistrict'] =
          selectedSubDistrict.value?.name.th ?? 'Unknown Subdistrict';
      userController.setUser(userController.currentUser.value);
    } catch (e) {
      print('Error updating user address: $e');
    }
  }

  // ฟังก์ชันสำหรับการเลือกและอัปเดตรูปภาพโปรไฟล์
  void updateUserImage(String imagePath) async {
    try {
      if (imagePath.isEmpty) {
        print('Image path cannot be empty.');
        return;
      }

      // อัปโหลดภาพไปยังเซิร์ฟเวอร์
      await userProfileService.updateUserImage(
        userId: userid.value,
        imagePath: imagePath,
      );

      // แสดงผลภาพที่ถูกเลือก โดยตรงจาก imagePath
      selectedImage.value = imagePath; // กำหนดเป็น URL หรือ path ของภาพ

      print('Profile image updated successfully.');

      // อัปเดตข้อมูลใน GetStorage
      final box = GetStorage();
      await box.write('userImage', imagePath);

      // อัปเดต currentUser ใน UserController
      userController.currentUser['userImage'] = imagePath;
      userController.setUser(userController.currentUser.value);
    } catch (e) {
      print('Error updating profile image: $e');
    }
  }

  Future<void> showConfirmationDialog(BuildContext context, String imagePath) {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('ยืนยันการเปลี่ยนรูปภาพ'),
          content: const Text('คุณต้องการเปลี่ยนรูปภาพโปรไฟล์ใช่ไหม?'),
          actions: <Widget>[
            TextButton(
              child: const Text('ยกเลิก'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: const Text('ยืนยัน'),
              onPressed: () {
                updateUserImage(imagePath); // ใช้ imagePath แทน
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  // Logout function
  void logout() async {
    final box = GetStorage();
    await box.remove('userID');
    await box.remove('userName');
    userController.clearUser();
    Get.offAllNamed('/prelogin');
  }
}
