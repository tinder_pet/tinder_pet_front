import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:tinder_pet_front/models/models.dart';

class RegionController extends GetxController {
  var provinces = <Province>[].obs;
  var districts = <District>[].obs;
  var subdistricts = <SubDistrict>[].obs;

  var selectedProvince = Rxn<Province>();
  var selectedDistrict = Rxn<District>();
  var selectedSubdistrict = Rxn<SubDistrict>();

  @override
  void onInit() {
    super.onInit();
    loadProvinces();
    loadDistricts();
    loadSubdistricts();
  }

  Future<void> loadProvinces() async {
  try {
    final String response = await rootBundle.loadString('assets/provinces.json');
    final data = json.decode(response) as List;
    provinces.value = data.map((json) => Province.fromMap(json)).toList();
  } catch (e) {
    print('Error loading provinces: $e');
  }
}

Future<void> loadDistricts() async {
  try {
    final String response = await rootBundle.loadString('assets/districts.json');
    final data = json.decode(response) as List;
     districts.value = data.map((json) => District.fromMap(json)).toList();
  } catch (e) {
    print('Error loading districts: $e');
  }
}

Future<void> loadSubdistricts() async {
  try {
    final String response = await rootBundle.loadString('assets/districts.json');
    final data = json.decode(response) as List;
    subdistricts.value = data.map((json) => SubDistrict.fromMap(json)).toList(); 
  } catch (e) {
    print('Error loading subdistricts: $e');
  }
}

  List<District> get filteredDistricts {
    if (selectedProvince.value == null) {
      return [];
    }
    return districts.where((d) => d.provinceId == selectedProvince.value!.id).toList();
  }

  List<SubDistrict> get filteredSubdistricts {
    if (selectedDistrict.value == null) {
      return [];
    }
    return subdistricts.where((s) => s.districtId == selectedDistrict.value!.id).toList();
  }
}
