import 'dart:typed_data';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:tinder_pet_front/app/routes/app_pages.dart';

class FilePickerController extends GetxController {
  var selectedFile = ''.obs;
  var selectedImageBytes = Rx<Uint8List?>(null);

  Future<void> pickFile() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.image,
    );

    if (result != null) {
      if (GetPlatform.isWeb) {
        final fileBytes = result.files.first.bytes;
        final fileName = result.files.first.name;

        selectedFile.value = fileName;
        selectedImageBytes.value = fileBytes;
      } else {
        final filePath = result.files.first.path;
        selectedFile.value = filePath ?? 'No file selected';
        selectedImageBytes.value = result.files.first.bytes;
      }
    } else {
      selectedFile.value = 'No file selected';
    }
  }

  Future<void> uploadFile(BuildContext context) async {
    if (selectedImageBytes.value == null) {
      Get.snackbar('Error', 'Please select an image before uploading.');
      return;
    }

    try {
      var uri = Uri.parse('http://127.0.0.1:8000/classify/');
      var request = http.MultipartRequest('POST', uri);

      request.files.add(http.MultipartFile.fromBytes(
        'file',
        selectedImageBytes.value!,
        filename: selectedFile.value,
      ));

      var response = await request.send();

      if (response.statusCode == 200) {
        final responseBody = await response.stream.bytesToString();
        // final responseJson = json.decode(responseBody);

        // แปลง responseBody จาก JSON string เป็น Map
        final responseJson = json.decode(responseBody) as Map<String, dynamic>;

        String animalType =
            responseJson.keys.first.toLowerCase() == 'dog' ? 'Dog' : 'Cat';

        print(animalType); // Output: Dog หรือ Cat

        // Navigate to PetProfile and pass the image data
        Get.toNamed(Routes.PETPROFILE, arguments: {
          'imageBytes': selectedImageBytes.value,
          'fileName': selectedFile.value,
          'animalType': animalType, // ส่งประเภทสัตว์ไปด้วย
        });
      } else if (response.statusCode == 500) {
        final responseBody = await response.stream.bytesToString();
        Get.snackbar('Error', 'Server error occurred: $responseBody');
        selectedImageBytes.value = null;
        selectedFile.value = 'No file selected';
      } else {
        Get.snackbar('Error', 'File upload failed: ${response.statusCode}');
      }
    } catch (e) {
      Get.snackbar('Error', 'Something went wrong: ${e.toString()}');
    }
  }
}
