import 'package:get/get.dart';
import 'package:tinder_pet_front/app/data/service/type/pet.dart';

import '../../../data/service/pet_service.dart';

class PetController extends GetxController {
  var pets = <Pet>[].obs;
  final PetService petService;

  PetController(this.petService);

  void fetchNearbyPets(int userId) async {
    try {
      var nearbyPets = await petService.fetchNearbyPets(userId);
      pets.value = nearbyPets;
    } catch (e) {
      // Handle the error, e.g., show a message
      print(e);
    }
  }
}
