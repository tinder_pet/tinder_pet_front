import 'package:get/get.dart';
import 'package:tinder_pet_front/app/data/service/chatroom_service.dart';
import 'home_controller.dart';

class ChatroomController extends GetxController {
  final HomeController homeController = Get.find<HomeController>();
  final ChatroomService chatroomService = ChatroomService();

  var isLoading = false.obs;
  var chatrooms = <dynamic>[].obs;

  @override
  void onInit() {
    super.onInit();
    loadChatrooms();
  }

  void loadChatrooms() async {
    isLoading.value = true;

    final userId = homeController.userController.getUser['userID'];
    if (userId != null) {
      try {
        final List<dynamic> data = await chatroomService.fetchChatrooms(userId);
        chatrooms.value = data; 
      } catch (e) {
        print('Error loading chatrooms: $e');
        Get.snackbar(
          'Error',
          'Failed to load chatrooms. Please try again later.',
          snackPosition: SnackPosition.BOTTOM,
        );
      } finally {
        isLoading.value = false;
      }
    } else {
      print('User ID is null.');
      isLoading.value = false;
    }
  }
}
