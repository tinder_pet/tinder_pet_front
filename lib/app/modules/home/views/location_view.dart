import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/user_controller.dart';
import 'package:tinder_pet_front/app/modules/home/views/prelogin/const.dart';
import 'package:tinder_pet_front/app/routes/app_pages.dart';
import '../controllers/location_controller.dart';

class LocationExample extends StatelessWidget {
  final LocationController locationController = Get.put(LocationController());
  final UserController userController = Get.put(UserController());

  @override
  Widget build(BuildContext context) {
    final int userId = userController.getUser['userID'];
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 200,
              width: 200,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('pets-image/map.png'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Text(
              'Location Access',
              style: TextStyle(
                fontSize: 30,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 20),
            Center(
              child: Text(
                'Please enable location access to\nuse this feature',
                textAlign: TextAlign.center,
              ),
            ),

            SizedBox(height: 20),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: pawColor1,
                elevation: 0,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50),
                ),
                padding: EdgeInsets.symmetric(vertical: 16, horizontal: 32),
              ),
              onPressed: () async {
                bool granted = await locationController.getCurrentLocation(
                    userId, context);
                if (granted) {
                  final box = GetStorage();
                  final pets = box.read('pets');

                  if (pets == null || pets.isEmpty) {
                    Get.offAllNamed(Routes.FILEPICKERPET);
                  } else {
                    Get.offNamed(Routes.HOME);
                  }
                }
              },
              child: Text(
                'ENABLE',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

void main() => runApp(GetMaterialApp(home: LocationExample()));
