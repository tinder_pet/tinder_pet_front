import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/petprofile_controller.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/user_controller.dart';
import 'package:tinder_pet_front/app/modules/home/views/colors_data.dart';
import 'package:tinder_pet_front/app/modules/home/views/prelogin/const.dart';
import 'package:tinder_pet_front/app/modules/home/views/screens/breed_data.dart';
import 'package:tinder_pet_front/app/modules/home/views/screens/content.dart';

class PetProfile extends StatefulWidget {
  @override
  _PetProfileState createState() => _PetProfileState();
}

class _PetProfileState extends State<PetProfile> {
  final PetprofileController petprofileController =
      Get.put(PetprofileController());
  final UserController userController = Get.put(UserController());
  Uint8List? _imageBytes;
  String? _fileName;
  String? _animalType;

  @override
  void initState() {
    super.initState();
    final args = Get.arguments as Map<String, dynamic>?;
    if (args != null) {
      _imageBytes = args['imageBytes'] as Uint8List?;
      _fileName = args['fileName'] as String?;
      _animalType = args['animalType'] as String?;

      petprofileController.setImage(_imageBytes!, _fileName!);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 80.0,
        flexibleSpace: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                pawColor1,
                Color(0xFFF7F0D2),
              ],
            ),
          ),
          child: Stack(
            children: [
              Positioned(
                top: 0,
                left: 0,
                child: Image.asset(
                  "pets-image/pawpair.png",
                  color: pawColor1,
                  width: 100,
                  height: 100,
                ),
              ),
              Positioned(
                bottom: -10,
                right: 10,
                child: Image.asset(
                  "pets-image/pawpair.png",
                  color: pawColor1,
                  width: 50,
                  height: 50,
                ),
              ),
              Center(
                child: Text(
                  'Pet Profile',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    shadows: [
                      Shadow(
                        offset: Offset(1.0, 1.0),
                        blurRadius: 2.0,
                        color: Colors.black.withOpacity(0.5),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        elevation: 0,
      ),
      body: Container(
        height: double.maxFinite,
        width: double.maxFinite,
        child: Stack(
          children: [
            ClipPath(
              clipper: MyClipper(),
              child: Container(
                height: MediaQuery.of(context).size.width * 0.6,
                width: double.maxFinite,
                decoration: const BoxDecoration(
                  gradient: LinearGradient(
                    colors: gradientColor,
                    begin: Alignment.topLeft,
                    end: Alignment.topRight,
                  ),
                ),
              ),
            ),
            Positioned(
              top: 100,
              bottom: 30,
              right: 30,
              left: 30,
              child: // ปรับส่วนของ Card และข้อมูลข้างใน
                  Card(
                elevation: 8,
                margin: EdgeInsets.symmetric(
                    horizontal: 20, vertical: 10), // เพิ่ม margin
                child: Container(
                  padding: EdgeInsets.all(16), // เพิ่ม padding รอบ Card
                  child: Form(
                    child: Center(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          SizedBox(height: 50), // ปรับระยะห่าง
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Expanded(
                                child: TextFormField(
                                  decoration: InputDecoration(
                                    hintText: 'Enter name',
                                    contentPadding: EdgeInsets.symmetric(
                                        vertical: 10,
                                        horizontal: 10), // เพิ่มระยะห่างภายใน
                                    border: InputBorder.none, // เอาขอบออก
                                  ),
                                  style: TextStyle(fontSize: 15.0),
                                  textAlign: TextAlign.center,
                                  controller:
                                      petprofileController.nameController,
                                ),
                              ),
                              SizedBox(width: 6),
                              Icon(Icons.edit),
                            ],
                          ),
                          SizedBox(height: 8),
                          Text(
                            'Description : ${petprofileController.desController.text}',
                            style:
                                TextStyle(fontSize: 16.0), // ปรับขนาดตัวอักษร
                          ),
                          SizedBox(height: 6),
                          Center(
                            child: TextFormField(
                              maxLines: 2,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20.0),
                                ),
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 20.0, horizontal: 10.0),
                              ),
                              controller: petprofileController.desController,
                            ),
                          ),
                          SizedBox(height: 10),
                          Column(
                            children: [
                              Row(
                                children: [
                                  Expanded(
                                    child: DropdownButtonFormField<String>(
                                      value: petprofileController
                                              .selectedSex.value.isNotEmpty
                                          ? petprofileController
                                              .selectedSex.value
                                          : null,
                                      onChanged: (String? newValue) {
                                        petprofileController.setSex(newValue!);
                                      },
                                      items: ['Male', 'Female']
                                          .map<DropdownMenuItem<String>>(
                                              (String value) {
                                        return DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(value),
                                        );
                                      }).toList(),
                                      decoration: InputDecoration(
                                        labelText: 'Sex',
                                        border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(20.0),
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: 10),
                                  Expanded(
                                    child: TextFormField(
                                      decoration: InputDecoration(
                                        labelText: 'Age',
                                        suffixText: 'months',
                                        border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(20.0),
                                        ),
                                      ),
                                      controller:
                                          petprofileController.ageController,
                                      validator: (value) {
                                        if (value == null || value.isEmpty) {
                                          return 'Please enter Age';
                                        }
                                        return null;
                                      },
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 10),
                              Row(
                                children: [
                                  Expanded(
                                    child: TextFormField(
                                      decoration: InputDecoration(
                                        labelText: 'Weight',
                                        suffixText: 'kg',
                                        border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(20.0),
                                        ),
                                      ),
                                      controller:
                                          petprofileController.weightController,
                                      validator: (value) {
                                        if (value == null || value.isEmpty) {
                                          return 'Please enter Weight';
                                        }
                                        return null;
                                      },
                                    ),
                                  ),
                                  SizedBox(width: 10),
                                  Expanded(
                                    child: DropdownButtonFormField<String>(
                                      isExpanded: true,
                                      value: petprofileController
                                              .selectedColor.value.isNotEmpty
                                          ? petprofileController
                                              .selectedColor.value
                                          : null,
                                      onChanged: (String? newValue) {
                                        petprofileController
                                            .setColor(newValue!);
                                      },
                                      items: (_animalType == 'Dog'
                                              ? dogColors // ลิสต์สีสำหรับสุนัข
                                              : catColors) // ลิสต์สีสำหรับแมว
                                          .map<DropdownMenuItem<String>>(
                                              (String value) {
                                        return DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(value),
                                        );
                                      }).toList(),
                                      decoration: InputDecoration(
                                        labelText: 'Color',
                                        border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(20.0),
                                        ),
                                      ),
                                      validator: (value) {
                                        if (value == null || value.isEmpty) {
                                          return 'Please select a Color'; // ตรวจสอบหากไม่มีการเลือกสี
                                        }
                                        return null;
                                      },
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 10),
                              Row(
                                children: [
                                  Expanded(
                                    child: DropdownButtonFormField<String>(
                                      value: petprofileController
                                              .selectedFur.value.isNotEmpty
                                          ? petprofileController
                                              .selectedFur.value
                                          : null,
                                      onChanged: (String? newValue) {
                                        petprofileController.setFur(newValue!);
                                      },
                                      items: ['Short', 'Medium', 'Long']
                                          .map<DropdownMenuItem<String>>(
                                              (String value) {
                                        return DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(value),
                                        );
                                      }).toList(),
                                      decoration: InputDecoration(
                                        labelText: 'Fur',
                                        border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(20.0),
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: 10),
                                  Expanded(
                                    child: DropdownButtonFormField<String>(
                                      isExpanded: true,
                                      value: petprofileController
                                              .selectedBreed.value.isNotEmpty
                                          ? petprofileController
                                              .selectedBreed.value
                                          : null,
                                      onChanged: (String? newValue) {
                                        petprofileController
                                            .setBreed(newValue!);
                                      },
                                      items: (_animalType == 'Dog'
                                              ? breedDog
                                              : breedCat)
                                          .map<DropdownMenuItem<String>>(
                                              (String value) {
                                        return DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(value),
                                        );
                                      }).toList(),
                                      decoration: InputDecoration(
                                        labelText: 'Breed',
                                        border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(20.0),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          SizedBox(height: 10),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 40),
                            child: Container(
                              padding: EdgeInsets.only(top: 3, left: 3),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                border: Border.all(color: Colors.black),
                              ),
                              child: MaterialButton(
                                minWidth: double.infinity,
                                height: 45,
                                onPressed: () {
                                  showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return AlertDialog(
                                        title: Text('Confirm'),
                                        content: Text('Successfully Edit'),
                                        actions: [
                                          TextButton(
                                            onPressed: () {
                                              Navigator.pop(
                                                  context); // Close the dialog
                                            },
                                            child: Text('Cancel'),
                                          ),
                                          TextButton(
                                            onPressed: () {
                                              petprofileController.register(
                                                userController
                                                    .currentUser['userID'],
                                                _animalType,
                                              );
                                              petprofileController.clearForm();
                                              Navigator.pop(
                                                  context); // Close the dialog after action
                                            },
                                            child: Text('Succeed'),
                                          ),
                                        ],
                                      );
                                    },
                                  );
                                },
                                color: Colors.orange,
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(50),
                                ),
                                child: Text(
                                  "Confirm",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              top: 25,
              left: MediaQuery.of(context).size.width / 2 - 75,
              child: Container(
                width: 150,
                height: 150,
                child: ClipOval(
                  child: petprofileController.images != null
                      ? Image.memory(
                          petprofileController.images!,
                          fit: BoxFit.cover,
                          width: 150,
                          height: 150,
                        )
                      : Container(
                          color: Colors.white, // กำหนดพื้นหลังสีขาว
                          alignment: Alignment.center,
                          child: Text(
                            'No image selected',
                            style: TextStyle(
                                color:
                                    Colors.black), // เปลี่ยนสีข้อความเป็นสีดำ
                          ),
                        ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class MyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path()
      ..lineTo(0, size.height)
      ..lineTo(size.width, size.height - 30)
      ..lineTo(size.width, 0)
      ..close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => true;
}
