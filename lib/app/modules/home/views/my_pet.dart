import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:tinder_pet_front/app/modules/home/views/prelogin/const.dart';
import 'package:tinder_pet_front/app/modules/home/views/screens/my_pet_body.dart';
import 'package:tinder_pet_front/app/routes/app_pages.dart';

class MyPet extends StatefulWidget {
  static const nameRoute = '/my_pet';
  const MyPet({Key? key}) : super(key: key);

  @override
  _MyPetState createState() => _MyPetState();
}

class _MyPetState extends State<MyPet> {
  final List<Map<String, String>> pets = [
    {'name': 'มะแมว', 'image': 'pets-image/black_cat1.jpg'},
    {'name': 'มะแมว', 'image': 'pets-image/black_cat1.jpg'},
    {'name': 'มะแมว', 'image': 'pets-image/black_cat1.jpg'},
    {'name': 'มะแมว', 'image': 'pets-image/black_cat1.jpg'},
    // {'name': 'มะหมา', 'image': 'assets/dog_image.jpg'},
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: pawColor1,
      appBar: buildAppBar(),
      body: MyPetBody(),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      backgroundColor: pawColor1,
      elevation: 0,
      leading: Padding(
        padding: const EdgeInsets.only(left: 10.0),
        child: Container(
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.white,
          ),
          child: SizedBox(
            width: 50,
            height: 50,
            child: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Get.toNamed(Routes.PROFILESCREEN);
              },
            ),
          ),
        ),
      ),
      title: Text(
        "My Pet",
        style: GoogleFonts.raleway(
          textStyle: TextStyle(
            color: Colors.white,
            fontSize: 35,
            fontWeight: FontWeight.bold,
            shadows: [
              Shadow(
                offset: Offset(1.0, 1.0),
                blurRadius: 2.0,
                color: Colors.black.withOpacity(0.5),
              ),
            ],
            letterSpacing: 10,
            decorationColor: Colors.black,
          ),
        ),
      ),
      centerTitle: true,
    );
  }
}
