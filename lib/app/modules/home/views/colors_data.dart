final List<String> dogColors = [
  'White', 
  'Black',
  'Gray',
  'Golden',
  'Brown',
  'Light Brown', //น้ำตาลอ่อน
  'Brown-Black', // น้ำตาล-ดำ
  'Red',
  'Yellow',
  'Cream',
  'Spotted', //ลายจุด
  'Piebald', //ลายด่าง
  'Markings' //ลายมาร์กกิ้ง
];

final List<String> catColors = [
  'White',
  'Black',
  'Gray',
  'Orange',
  'Brown',
  'Golden',
  'Cream',
  'Tabby', // ลายสลิด
  'Tuxedo', // ทักซิโด้
  'Tortoiseshell', // ลายกระดองเต่า
  'Pointed', // ลายแต้มจุด
  'Calico', // ลายสามสี
  'Chocolate',
  'Snowshoe' // ลายหิมะ
];
