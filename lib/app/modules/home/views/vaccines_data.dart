final List<String> dogVaccines = [
    'Rabies', // วัคซีนป้องกันโรคพิษสุนัขบ้า
    'Parvovirus', // วัคซีนป้องกันโรคลำไส้อักเสบ
    'Distemper', // วัคซีนป้องกันโรคไข้หัดสุนัข
    'Hepatitis', // วัคซีนป้องกันโรคไวรัสตับอักเสบ
    'Leptospirosis', // วัคซีนป้องกันโรคไข้ฉี่หนู
    'Parainfluenza', // วัคซีนป้องกันโรคพาราอินฟลูเอนซ่า
  ];

  final List<String> catVaccines = [
    'Rabies', // วัคซีนป้องกันโรคพิษสุนัขบ้า
    'Feline Panleukopenia', // วัคซีนป้องกันโรคไข้หัดแมว
    'Feline Calicivirus, Feline Herpesvirus', // วัคซีนป้องกันโรคไข้หวัดแมว
    'Feline Leukemia Virus, FeLV', // วัคซีนป้องกันโรคลิวคีเมียในแมว
    'Feline Immunodeficiency Virus, FIV', // วัคซีนป้องกันโรคเอดส์แมว
  ];