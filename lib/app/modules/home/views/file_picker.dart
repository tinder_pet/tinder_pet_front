
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/file_picker_controller.dart';


class FilePickerScreen extends StatelessWidget {
  final FilePickerController controller = Get.put(FilePickerController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Positioned(
            bottom: 0,
            right: 0,
            left: 0,
            child: Image.asset(
              'pets-image/background2.png',
              height: 375,
              width: double.infinity,
              fit: BoxFit.fill,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Obx(() {
                  return Container(
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Add At',
                              style: TextStyle(
                                fontSize: 35,
                                color: Colors.black,
                                fontWeight: FontWeight.w900,
                                height: 1.2,
                              ),
                            ),
                            SizedBox(width: 10),
                            Text(
                              'Picture',
                              style: TextStyle(
                                fontSize: 35,
                                color: Colors.lightBlue,
                                fontWeight: FontWeight.w900,
                                height: 1.2,
                              ),
                            ),
                            SizedBox(width: 10),
                            Text(
                              'Of Your Pet',
                              style: TextStyle(
                                fontSize: 35,
                                color: Colors.black,
                                fontWeight: FontWeight.w900,
                                height: 1.2,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 20.0),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Let\'s let others get to know your lovely pets.',
                              style: TextStyle(
                                fontSize: 15.5,
                                color: Colors.black38,
                                fontWeight: FontWeight.w900,
                                height: 1.2,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 40.0),
                        Center(
                          child: Stack(
                            children: [
                              if (controller.selectedImageBytes.value == null)
                                Container(
                                  decoration: BoxDecoration(
                                      color: Colors.orange,
                                      shape: BoxShape.circle,
                                      boxShadow: [
                                        BoxShadow(
                                            blurRadius: 10,
                                            color: Colors.grey,
                                            spreadRadius: 5)
                                      ]),
                                  child: CircleAvatar(
                                    maxRadius: 150.0,
                                    minRadius: 150.0,
                                    backgroundColor: Colors.orange,
                                  ),
                                ),
                              if (controller.selectedImageBytes.value != null)
                                Container(
                                  decoration: BoxDecoration(
                                      color: Colors.orange,
                                      shape: BoxShape.circle,
                                      boxShadow: [
                                        BoxShadow(
                                            blurRadius: 10,
                                            color: Colors.grey,
                                            spreadRadius: 5)
                                      ]),
                                  child: CircleAvatar(
                                    maxRadius: 150.0,
                                    minRadius: 150.0,
                                    backgroundColor: Colors.orange,
                                    backgroundImage: MemoryImage(
                                        controller.selectedImageBytes.value!),
                                  ),
                                ),
                              Positioned(
                                bottom: 0,
                                right: 0,
                                height: 80,
                                width: 80,
                                child: ElevatedButton(
                                  onPressed: controller.pickFile,
                                  style: ElevatedButton.styleFrom(
                                    shape: CircleBorder(),
                                    padding: EdgeInsets.all(0),
                                  ),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Colors.orange,
                                      shape: BoxShape.circle,
                                      border: Border.all(
                                          width: 4, color: Colors.grey),
                                    ),
                                    alignment: Alignment.center,
                                    child: Icon(
                                      Icons.add_circle,
                                      color: Colors.white,
                                      size: 50.0,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                }),
                SizedBox(height: 80.0),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.7,
                  height: 50,
                  child: Container(
                    padding: EdgeInsets.only(top: 3, left: 3),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      border: Border.all(color: Colors.black),
                    ),
                    child: SizedBox(
                      width: double.infinity,
                      height: 60,
                      child: ElevatedButton(
                        onPressed: () {
                          controller.uploadFile(context);
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.orange,
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50),
                          ),
                        ),
                        child: Text(
                          'Next',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            color: Colors.black,
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
