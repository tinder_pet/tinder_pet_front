import 'package:flutter/material.dart';
import 'package:tinder_pet_front/app/modules/home/views/prelogin/const.dart';

class PetDetails extends StatefulWidget {
  static const nameRoute = '/detail_image_pet';
  const PetDetails({Key? key}) : super(key: key);

  @override
  _PetDetailsState createState() => _PetDetailsState();
}

class _PetDetailsState extends State<PetDetails> {
  final List<String> _imagePaths = [
    "pets-image/black_cat1.jpg",
    "pets-image/black_cat2.jpg",
    "pets-image/black_cat3.jpeg",
  ];

  int _currentPage = 0;
  final PageController _pageController = PageController();

  void _nextPage() {
    if (_currentPage < _imagePaths.length - 1) {
      _pageController.nextPage(
        duration: Duration(milliseconds: 300),
        curve: Curves.easeInOut,
      );
    }
  }

  void _previousPage() {
    if (_currentPage > 0) {
      _pageController.previousPage(
        duration: Duration(milliseconds: 300),
        curve: Curves.easeInOut,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    final imageSize = screenSize.width * 0.4;
    return Scaffold(
      appBar: AppBar(
        title: Text('My Pet'),
        backgroundColor: pawColor1,
        centerTitle: true,
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    IconButton(
                      icon: Icon(Icons.arrow_back_ios_sharp),
                      onPressed: _previousPage,
                    ),
                    SizedBox(
                      width: imageSize,
                      height: imageSize,
                      child: PageView.builder(
                        controller: _pageController,
                        onPageChanged: (int page) {
                          setState(() {
                            _currentPage = page;
                          });
                        },
                        itemCount: _imagePaths.length,
                        itemBuilder: (context, index) {
                          return CircleAvatar(
                            radius: imageSize / 2,
                            backgroundColor: Colors.white,
                            child: CircleAvatar(
                              radius: (imageSize / 2) - 5,
                              backgroundImage: AssetImage(_imagePaths[
                                  index]),
                            ),
                          );
                        },
                      ),
                    ),
                    IconButton(
                      icon: Icon(Icons.arrow_forward_ios_sharp),
                      onPressed: _nextPage,
                    ),
                  ],
                ),
                SizedBox(height: 16.0),
                Text(
                  "มะแมว",
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
                SizedBox(height: 16.0),
                Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  color: orangeContainer,
                  child: Container(
                    width: screenSize.width *
                        0.8, 
                    padding: const EdgeInsets.all(16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Sex : Female",
                          style: TextStyle(fontSize: 18, color: Colors.black),
                        ),
                        Text(
                          "Age : 4 year",
                          style: TextStyle(fontSize: 18, color: Colors.black),
                        ),
                        Text(
                          "Weight : 5 Kg.",
                          style: TextStyle(fontSize: 18, color: Colors.black),
                        ),
                        Text(
                          "Color : black",
                          style: TextStyle(fontSize: 18, color: Colors.black),
                        ),
                        Text(
                          "Fur : short",
                          style: TextStyle(fontSize: 18, color: Colors.black),
                        ),
                        Text(
                          "Breed : Mixed Breed",
                          style: TextStyle(fontSize: 18, color: Colors.black),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
