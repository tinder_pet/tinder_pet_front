import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/user_controller.dart';
import 'package:tinder_pet_front/app/modules/home/views/information.dart';
import 'package:tinder_pet_front/app/modules/home/views/prelogin/const.dart';
import 'package:tinder_pet_front/app/modules/home/views/profile_screen/profile_menu.dart';
import 'package:tinder_pet_front/app/routes/app_pages.dart';
import '../../controllers/userprofile_controller.dart';
import '../subscription.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final UserProfileController userProfileController =
      Get.put(UserProfileController());
  final UserController userController = Get.put(UserController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Padding(
          padding: const EdgeInsets.only(left: 10.0),
          child: Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.white,
            ),
            child: SizedBox(
              width: 50,
              height: 50,
              child: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  Get.toNamed(Routes.HOME);
                },
              ),
            ),
          ),
        ),
        title: Text(
          'Profile',
          style: TextStyle(
            fontWeight: FontWeight.w600,
            color: black,
            fontSize: 18,
          ),
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            /// -- IMAGE
            Stack(
              children: [
                SizedBox(
                  width: 120,
                  height: 120,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(100),
                    child: const Image(
                      image: AssetImage('pets-image/catcat.jpg'),
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 10),

            /// -- USERNAME AND EMAIL
            Obx(() => Text(
                  '${userController.currentUser['userName'] ?? 'N/A'}',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: black,
                    fontSize: 18,
                  ),
                )),
            Obx(() => Text(
                  '${userController.currentUser['email'] ?? 'N/A'}',
                )),

            const SizedBox(height: 20),

            /// -- BUTTON
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.7,
              height: 50,
              child: Container(
                padding: EdgeInsets.only(top: 3, left: 3),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  border: Border.all(color: Colors.black),
                ),
                child: SizedBox(
                  width: double.infinity,
                  height: 60,
                  child: ElevatedButton(
                    onPressed: () => Get.toNamed(Routes.USERPROFILE),
                    style: ElevatedButton.styleFrom(
                      backgroundColor: pawColor1,
                      elevation: 0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50),
                      ),
                    ),
                    child: Text(
                      'Edit Profile',
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        color: black,
                        fontSize: 18,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 30),
            const Divider(),
            const SizedBox(height: 10),

            /// -- MENU
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: ProfileMenuWidget(
                title: Text(
                  'My Pet',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.black,
                    fontSize: 16,
                  ),
                ),
                icon: Icons.pets_outlined,
                onPress: () {
                  Get.toNamed(Routes.MYPET);
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: ProfileMenuWidget(
                title: Text(
                  "Billing Details",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.black,
                    fontSize: 16,
                  ),
                ),
                icon: Icons.wallet_sharp,
                onPress: () {
                  Get.toNamed(Routes.BILLING);
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: ProfileMenuWidget(
                title: Text(
                  "Upgrade Member",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.black,
                    fontSize: 16,
                  ),
                ),
                icon: Icons.workspace_premium_rounded,
                onPress: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const SUBSCRIPTION()),
                  );
                },
              ),
            ),
            const Divider(),
            const SizedBox(height: 10),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: ProfileMenuWidget(
                title: Text(
                  "Information",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.black,
                    fontSize: 16,
                  ),
                ),
                icon: Icons.info,
                onPress: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const INFORMATION()),
                  );
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: ProfileMenuWidget(
                title: Text(
                  "Logout",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.red,
                    fontSize: 16,
                  ),
                ),
                icon: Icons.logout_rounded,
                textColor: Colors.red,
                endIcon: false,
                onPress: () {
                  Get.defaultDialog(
                    title: "LOGOUT",
                    titleStyle: const TextStyle(fontSize: 20),
                    content: const Padding(
                      padding: EdgeInsets.symmetric(vertical: 15.0),
                      child: Text("Are you sure, you want to Logout?"),
                    ),
                    confirm: ElevatedButton(
                      onPressed: () {
                        userProfileController
                            .logout(); // เรียกใช้ฟังก์ชัน logout
                      },
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.redAccent,
                        side: BorderSide.none,
                      ),
                      child: const Text("Yes"),
                    ),
                    cancel: OutlinedButton(
                      onPressed: () => Get.back(),
                      child: const Text("No"),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
