import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:thai_region/thai_region.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/user_controller.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/userprofile_controller.dart';
import 'package:tinder_pet_front/app/modules/home/views/prelogin/const.dart';
import 'package:tinder_pet_front/app/routes/app_pages.dart';

const List<String> sexes = ['Male', 'Female', 'Other'];

class UserProfile extends StatefulWidget {
  static const nameRoute = '/user_Profile';
  UserProfile({Key? key}) : super(key: key);

  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  final UserProfileController userProfileController =
      Get.put(UserProfileController());
  final UserController userController = Get.put(UserController());

  Province? selectedProvince;
  District? selectedDistrict;
  SubDistrict? selectedSubdistrict;
  String? selectedSex;

  List<Province> _provinces = [];
  List<District> _districts = [];
  List<SubDistrict> _subdistricts = [];

  late TextEditingController userDesController;
  late TextEditingController userNameController;
  late TextEditingController phoneController;
  late TextEditingController religionController;
  late TextEditingController nationalityController;
  late TextEditingController contactController;

  @override
  void initState() {
    super.initState();
    userDesController = TextEditingController(
        text: userController.currentUser['userDes'] ?? '');
    userNameController = TextEditingController(
        text: userController.currentUser['userName'] ?? '');
    phoneController = TextEditingController(
        text: userController.currentUser['phoneNumber'] ?? '');
    religionController = TextEditingController(
        text: userController.currentUser['religion'] ?? '');
    nationalityController = TextEditingController(
        text: userController.currentUser['nationality'] ?? '');
    contactController = TextEditingController(
        text: userController.currentUser['contact'] ?? '');

    // ตั้งค่า selectedSex
    String? currentSex = userController.currentUser['sex'];
    selectedSex = currentSex != null && sexes.contains(currentSex)
        ? currentSex
        : sexes.first; // ใช้ค่าแรกใน sexes หากค่าไม่ตรงหรือเป็น null
    _loadRegionData();
  }

  Future<void> _loadRegionData() async {
    var thaiRegion = ThaiRegion();
    _provinces = await thaiRegion.getProvince();

    final provinceName = userController.currentUser['province']?.trim() ?? '';
    final districtName = userController.currentUser['district']?.trim() ?? '';
    final subDistrictName =
        userController.currentUser['subDistrict']?.trim() ?? '';

    print('Province from currentUser: $provinceName');

    try {
      // ตั้งค่า selectedProvince โดยตรงจาก currentUser
      userProfileController.selectedProvince.value = _provinces.firstWhere(
        (province) =>
            province.getName(locale: Locale('th')).trim() == provinceName,
        orElse: () => _provinces.first, // ใช้จังหวัดแรกเป็นค่าเริ่มต้นถ้าไม่พบ
      );

      // โหลดข้อมูลอำเภอหลังจากตั้งค่า selectedProvince
      await _loadDistricts(userProfileController.selectedProvince.value!);

      // ตั้งค่า selectedDistrict
      userProfileController.selectedDistrict.value = _districts.firstWhere(
        (district) =>
            district.getName(locale: Locale('th')).trim() == districtName,
      );

      // โหลดข้อมูลตำบลถ้ามี selectedDistrict
      if (userProfileController.selectedDistrict.value != null) {
        await _loadSubDistricts(userProfileController.selectedDistrict.value!);
        userProfileController.selectedSubDistrict.value =
            _subdistricts.firstWhere(
          (subDistrict) =>
              subDistrict.getName(locale: Locale('th')).trim() ==
              subDistrictName,
        );
      }
    } catch (e) {
      print('Error finding region: $e');
    }

    // เรียก setState เพื่ออัปเดต UI
    setState(() {});
  }

  Future<void> _loadDistricts(Province province) async {
    var thaiRegion = ThaiRegion();
    _districts = await thaiRegion.getDistrict(provinceId: province.id);
    _subdistricts = []; // รีเซ็ตตำบล
    setState(() {}); // อัปเดต UI
  }

  Future<void> _loadSubDistricts(District district) async {
    var thaiRegion = ThaiRegion();
    _subdistricts = await thaiRegion.getSubDistrict(districtId: district.id);
    setState(() {}); // อัปเดต UI
  }

  // Function to show confirm dialog
  Future<void> _showConfirmationDialog() async {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('ยืนยัน'),
          content: Text('คุณแน่ใจหรือว่าต้องการอัปเดตโปรไฟล์และที่อยู่ของคุณ?'),
          actions: <Widget>[
            TextButton(
              child: Text('ยกเลิก'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('ยืนยัน'),
              onPressed: () async {
                // ตรวจสอบการเลือกข้อมูลที่อยู่
                userProfileController.updateUserName(userNameController.text);
                // อัปเดต UserProfileController ด้วยข้อมูลใหม่
                userProfileController
                    .updateUserDescription(userDesController.text);
                userProfileController.selectedSex.value = selectedSex ?? '';
                userProfileController.phoneNumber.value = phoneController.text;
                userProfileController.religion.value = religionController.text;
                userProfileController.nationality.value =
                    nationalityController.text;
                userProfileController.contact.value = contactController.text;

                // รวมที่อยู่จากจังหวัด, อำเภอ, ตำบล
                String combinedAddress =
                    '${userProfileController.selectedProvince.value!.name.th}, ${userProfileController.selectedDistrict.value!.name.th}, ${userProfileController.selectedSubDistrict.value!.name.th}';

                try {
                  // เรียก API อัปเดตโปรไฟล์ผู้ใช้
                  await userProfileController.updateUserProfile();

                  // บันทึกที่อยู่ใหม่
                  await userProfileController
                      .updateUserAddress(combinedAddress);

                  // แสดงข้อความสำเร็จ
                  Navigator.of(context).pop();
                  _showSuccessMessage(); // แสดงข้อความสำเร็จ
                } catch (e) {
                  // จัดการข้อผิดพลาดและแสดงข้อความ
                  Get.snackbar(
                    'ข้อผิดพลาด',
                    'ไม่สามารถอัปเดตโปรไฟล์ได้: $e',
                    snackPosition: SnackPosition.BOTTOM,
                    backgroundColor: Colors.red,
                    colorText: Colors.white,
                    margin: EdgeInsets.all(20),
                  );
                }
              },
            ),
          ],
        );
      },
    );
  }

  // Function to show the success message
  void _showSuccessMessage() {
    Get.snackbar(
      'Success',
      'Successfully Updated',
      snackPosition: SnackPosition.BOTTOM,
      backgroundColor: Colors.green,
      colorText: Colors.white,
      margin: EdgeInsets.all(20),
    );
  }

  Future<void> _pickImage() async {
    final ImagePicker picker = ImagePicker();
    final XFile? image = await picker.pickImage(source: ImageSource.gallery);
    if (image != null) {
      // อัปเดตรูปภาพใน userProfileController โดยใช้ path ของภาพ
      userProfileController.selectedImage.value = image.path;

      // เรียกใช้ฟังก์ชันยืนยันการเปลี่ยนรูปภาพ
      await userProfileController.showConfirmationDialog(
          context, image.path // ส่ง path แทน base64
          );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 80.0,
        flexibleSpace: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                pawColor1,
                Color(0xFFF7F0D2),
              ],
            ),
          ),
          child: Stack(
            children: [
              Positioned(
                top: 0,
                left: 0,
                child: Image.asset(
                  "assets/pets-image/pawpair.png",
                  color: pawColor1,
                  width: 100,
                  height: 100,
                ),
              ),
              Positioned(
                bottom: -10,
                right: 10,
                child: Image.asset(
                  "assets/pets-image/pawpair.png",
                  color: pawColor1,
                  width: 50,
                  height: 50,
                ),
              ),
            ],
          ),
        ),
        elevation: 0,
        title: Center(
          child: Text(
            'Edit Profile',
            style: GoogleFonts.raleway(
              textStyle: TextStyle(
                color: Colors.white,
                fontSize: 35,
                fontWeight: FontWeight.bold,
                shadows: [
                  Shadow(
                    offset: Offset(1.0, 1.0),
                    blurRadius: 2.0,
                    color: Colors.black.withOpacity(0.5),
                  ),
                ],
                letterSpacing: 10,
                decorationColor: Colors.black,
              ),
            ),
          ),
        ),
        leading: Padding(
          padding: const EdgeInsets.only(left: 10.0),
          child: Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.white,
            ),
            child: SizedBox(
              width: 50,
              height: 50,
              child: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  Get.toNamed(Routes.PROFILESCREEN);
                },
              ),
            ),
          ),
        ),
      ),
      body: Form(
        child: Row(
          children: [
            Expanded(
              flex: 1,
              child: ListView(
                padding: EdgeInsets.all(16),
                children: [
                  Center(
                    child: Stack(
                      children: [
                        Obx(() {
                          print(
                              'Selected Image Value: ${userProfileController.selectedImage.value}');
                          return SizedBox(
                            width: 120,
                            height: 120,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(100),
                              child: userProfileController
                                              .selectedImage.value !=
                                          null &&
                                      userProfileController
                                          .selectedImage.value!.isNotEmpty
                                  ? Image.network(
                                      userProfileController
                                          .selectedImage.value!,
                                      fit: BoxFit.cover,
                                      errorBuilder:
                                          (context, error, stackTrace) {
                                        return const Image(
                                          image: AssetImage(
                                              'pets-image/catcat.jpg'),
                                        );
                                      },
                                    )
                                  : const Image(
                                      image:
                                          AssetImage('pets-image/catcat.jpg'),
                                    ),
                            ),
                          );
                        }),
                        Positioned(
                          bottom: 0,
                          right: 0,
                          child: GestureDetector(
                            onTap: () async {
                              await _pickImage();
                            },
                            child: Container(
                              width: 35,
                              height: 35,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(100),
                                color: const Color.fromARGB(255, 255, 185, 33),
                              ),
                              child: const Icon(
                                Icons.edit,
                                color: Color.fromARGB(255, 0, 0, 0),
                                size: 20,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),

                  SizedBox(height: 16),
                  // username
                  Center(
                    child: IntrinsicWidth(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                            child: TextFormField(
                              controller: userNameController,
                              textAlign: TextAlign.center,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                              ),
                              style: GoogleFonts.kanit(fontSize: 25.0),
                            ),
                          ),
                          Icon(Icons.edit),
                        ],
                      ),
                    ),
                  ),

                  Center(
                    child: Obx(() {
                      // ตรวจสอบ statusMember
                      String statusMember =
                          userController.currentUser['statusMember'] ?? 'non';

                      return Text(
                        statusMember == 'active'
                            ? 'PAWS PAIR +'
                            : 'BASIC PAWS PAIR',
                        style: GoogleFonts.kanit(
                            fontSize: 20.0, fontWeight: FontWeight.bold),
                      );
                    }),
                  ),

                  SizedBox(height: 16),
                  Text(
                    "Description :",
                    style: GoogleFonts.kanit(fontSize: 16.0),
                  ),
                  SizedBox(height: 3),
                  // Text - UserDes
                  Center(
                    child: TextFormField(
                      controller: userDesController,
                      maxLines: 4,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderSide: BorderSide(width: 10),
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        contentPadding: EdgeInsets.symmetric(
                            vertical: 20.0, horizontal: 10.0),
                      ),
                    ),
                  ),

                  SizedBox(height: 20),
                  Text(
                    "Personal Information",
                    style: GoogleFonts.kanit(
                        fontSize: 22.0, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 6),
                  // Sex Dropdown
                  Text(
                    "Sex",
                    style: GoogleFonts.kanit(fontSize: 16.0),
                  ),
                  SizedBox(height: 5),
                  // DropdownButtonFormField สำหรับเพศ
                  DropdownButtonFormField<String>(
                    decoration: InputDecoration(
                      hintText: 'Select Sex', // ข้อความคำแนะนำ
                      border: OutlineInputBorder(
                        borderSide: BorderSide(width: 10),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),
                    value:
                        selectedSex, // ใช้ selectedSex เพื่อแสดงค่าใน Dropdown
                    items: sexes.map((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    hint: Text('Select'), // ข้อความคำแนะนำ
                    onChanged: (newValue) {
                      setState(() {
                        selectedSex =
                            newValue; // อัปเดต selectedSex เมื่อมีการเปลี่ยนค่า
                      });
                    },
                  ),
                  SizedBox(height: 10),
                  // Phone Number
                  Text(
                    "Phone Number",
                    style: GoogleFonts.kanit(fontSize: 16.0),
                  ),
                  SizedBox(height: 6),
                  TextFormField(
                    controller: phoneController,
                    decoration: InputDecoration(
                      hintText: 'Phone Number',
                      border: OutlineInputBorder(
                        borderSide: BorderSide(width: 10),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),
                    keyboardType: TextInputType.phone,
                  ),
                  SizedBox(height: 10),
                  // Religion
                  Text(
                    "Religion",
                    style: GoogleFonts.kanit(fontSize: 16.0),
                  ),
                  SizedBox(height: 6),
                  TextFormField(
                    controller: religionController,
                    decoration: InputDecoration(
                      hintText: 'Religion',
                      border: OutlineInputBorder(
                        borderSide: BorderSide(width: 10),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                  // Nationality
                  Text(
                    "Nationality",
                    style: GoogleFonts.kanit(fontSize: 16.0),
                  ),
                  SizedBox(height: 6),
                  TextFormField(
                    controller: nationalityController,
                    decoration: InputDecoration(
                      hintText: 'Nationality',
                      border: OutlineInputBorder(
                        borderSide: BorderSide(width: 10),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                  // Contact
                  Text(
                    "Contact",
                    style: GoogleFonts.kanit(fontSize: 16.0),
                  ),
                  SizedBox(height: 6),
                  TextFormField(
                    controller: contactController,
                    maxLines: 2,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderSide: BorderSide(width: 10),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      contentPadding: EdgeInsets.symmetric(
                          vertical: 20.0, horizontal: 10.0),
                    ),
                  ),

                  SizedBox(height: 20),
                  Text(
                    "Address",
                    style: GoogleFonts.kanit(
                        fontSize: 22.0, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 10),

                  // DropdownButtonFormField สำหรับ Province
                  // Dropdown สำหรับ Province
                  DropdownButtonFormField<Province>(
                    decoration: InputDecoration(
                      labelText: userProfileController
                              .selectedProvince.value?.name.th ??
                          'Province',
                      border: OutlineInputBorder(
                        borderSide: BorderSide(width: 1),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),
                    // ใช้ค่าที่มาจาก currentUser แทน
                    value: userProfileController.selectedProvince.value,
                    items: _provinces.map((Province province) {
                      return DropdownMenuItem<Province>(
                        value: province,
                        child: Text(
                          province.name.th,
                          overflow: TextOverflow.ellipsis,
                        ),
                      );
                    }).toList(),
                    hint: Text('เลือกจังหวัด'),
                    onChanged: (Province? newValue) {
                      if (newValue != null) {
                        setState(() {
                          userProfileController.selectedProvince.value =
                              newValue;
                          userProfileController.selectedDistrict.value =
                              null; // รีเซ็ตอำเภอ
                          userProfileController.selectedSubDistrict.value =
                              null; // รีเซ็ตตำบล
                          _districts = []; // เคลียร์รายการอำเภอ
                          _subdistricts = []; // เคลียร์รายการตำบล
                        });
                        _loadDistricts(newValue); // โหลดอำเภอที่เกี่ยวข้อง
                      }
                    },
                  ),

                  SizedBox(height: 16),

// Dropdown สำหรับ District
                  DropdownButtonFormField<District>(
                    decoration: InputDecoration(
                      labelText: 'District',
                      border: OutlineInputBorder(
                        borderSide: BorderSide(width: 1),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),
                    value: userProfileController.selectedDistrict.value,
                    items: _districts.map((District district) {
                      return DropdownMenuItem<District>(
                        value: district,
                        child: Text(
                          district.name.th,
                          overflow: TextOverflow.ellipsis,
                        ),
                      );
                    }).toList(),
                    hint: Text('เลือกอำเภอ'),
                    onChanged: (District? newValue) {
                      if (newValue != null) {
                        setState(() {
                          userProfileController.selectedDistrict.value =
                              newValue;
                          userProfileController.selectedSubDistrict.value =
                              null; // รีเซ็ตตำบล
                          _subdistricts = []; // เคลียร์รายการตำบล
                        });
                        _loadSubDistricts(newValue); // โหลดตำบลที่เกี่ยวข้อง
                      }
                    },
                  ),

                  SizedBox(height: 16),

// Dropdown สำหรับ SubDistrict
                  DropdownButtonFormField<SubDistrict>(
                    decoration: InputDecoration(
                      labelText: 'Subdistrict',
                      border: OutlineInputBorder(
                        borderSide: BorderSide(width: 1),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),
                    value: userProfileController.selectedSubDistrict.value,
                    items: _subdistricts.map((SubDistrict subDistrict) {
                      return DropdownMenuItem<SubDistrict>(
                        value: subDistrict,
                        child: Text(
                          subDistrict.name.th,
                          overflow: TextOverflow.ellipsis,
                        ),
                      );
                    }).toList(),
                    hint: Text('เลือกตำบล'),
                    onChanged: (SubDistrict? newValue) {
                      if (newValue != null) {
                        setState(() {
                          userProfileController.selectedSubDistrict.value =
                              newValue;
                        });
                      }
                    },
                  ),

                  SizedBox(height: 18),
                  // Save Button
                  Center(
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.7,
                      height: 50,
                      padding: EdgeInsets.only(top: 3, left: 3),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        border: Border.all(color: Colors.black),
                      ),
                      child: ElevatedButton(
                        onPressed: () {
                          _showConfirmationDialog(); // Show the confirmation dialog
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: orangeContainer,
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50),
                          ),
                        ),
                        child: Text(
                          'Save',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            color: Colors.black,
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
