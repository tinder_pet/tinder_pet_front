import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/home_controller.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/user_controller.dart';
import 'package:tinder_pet_front/app/modules/home/views/chatroom/ChatPage.dart';
import 'package:tinder_pet_front/app/modules/home/views/prelogin/const.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/chatroom_controller.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:tinder_pet_front/app/routes/app_pages.dart';
import 'package:intl/intl.dart';

class ChatroomPage extends StatefulWidget {
  @override
  _ChatroomPageState createState() => _ChatroomPageState();
}

class _ChatroomPageState extends State<ChatroomPage>
    with SingleTickerProviderStateMixin {
  final ChatroomController chatroomController = Get.put(ChatroomController());
  final UserController userController = Get.put(UserController());
  final HomeController homeController = Get.put(HomeController());
  late AnimationController _controller;
  late Animation<double> _animation;

  @override
  void initState() {
    super.initState();
    print('SomeScreen is being built. User data: ${userController.getUser}');

    _controller = AnimationController(
      duration: const Duration(milliseconds: 800),
      vsync: this,
    )..repeat(reverse: true);

    _animation = Tween<double>(begin: 1.0, end: 1.3).animate(_controller);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 80.0,
        flexibleSpace: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                pawColor1,
                Color(0xFFF7F0D2),
              ],
            ),
          ),
          child: Stack(
            children: [
              Positioned(
                top: 0,
                left: 0,
                child: Transform.rotate(
                  angle: -11,
                  child: Image.asset(
                    "pets-image/pawpair.png",
                    color: pawColor1,
                    width: 100,
                    height: 100,
                  ),
                ),
              ),
              Positioned(
                bottom: -10,
                right: 10,
                child: Transform.rotate(
                  angle: -12,
                  child: Image.asset(
                    "pets-image/pawpair.png",
                    color: pawColor1,
                    width: 50,
                    height: 50,
                  ),
                ),
              ),
              Center(
                child: Text(
                  'Paw Pair Chat',
                  style: GoogleFonts.raleway(
                    textStyle: const TextStyle(
                      color: Colors.white,
                      fontSize: 24,
                      fontWeight: FontWeight.w500,
                      letterSpacing: 1.5,
                      shadows: [
                        Shadow(
                          offset: Offset(1.0, 1.0),
                          blurRadius: 2.0,
                          color: Colors.black,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        elevation: 0,
        leading: Padding(
          padding: const EdgeInsets.only(left: 10.0),
          child: Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.white,
            ),
            child: SizedBox(
              width: 50,
              height: 50,
              child: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  Get.toNamed(Routes.HOME);
                },
              ),
            ),
          ),
        ),
      ),
      body: Obx(() {
        if (chatroomController.isLoading.value) {
          return Center(child: CircularProgressIndicator());
        }

        if (chatroomController.chatrooms.isEmpty) {
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ScaleTransition(
                  scale: _animation,
                  child: Icon(
                    Icons.favorite,
                    color: Colors.red,
                    size: 60,
                  ),
                ),
                SizedBox(height: 10),
                Text(
                  'No matches found! \nTry connecting with other pets.',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
              ],
            ),
          );
        }

        final currentUserId = userController.getUser['userID'];

        return ListView.builder(
          itemCount: chatroomController.chatrooms.length,
          itemBuilder: (context, index) {
            final chatroom = chatroomController.chatrooms[index];
            bool isUser1 = chatroom['user1']['userID'] == currentUserId;
            String opponentPetImageUrl = isUser1
                ? chatroom['user2']['pets']?.isNotEmpty == true
                    ? 'http://localhost:3000/image/image/${chatroom['user2']['pets'][0]['images'][0]['image']}'
                    : ''
                : chatroom['user1']['pets']?.isNotEmpty == true
                    ? 'http://localhost:3000/image/image/${chatroom['user1']['pets'][0]['images'][0]['image']}'
                    : '';

            return ListTile(
              leading: opponentPetImageUrl.isNotEmpty
                  ? ClipOval(
                      child: SizedBox(
                        width: 50,
                        height: 50,
                        child: Image.network(
                          opponentPetImageUrl,
                          fit: BoxFit.cover,
                          errorBuilder: (context, error, stackTrace) {
                            return Icon(Icons.error);
                          },
                        ),
                      ),
                    )
                  : null,
              title: Text(
                isUser1
                    ? chatroom['user2']['pets']?.isNotEmpty == true
                        ? chatroom['user2']['pets'][0]['petName']
                        : 'Unknown Pet'
                    : chatroom['user1']['pets']?.isNotEmpty == true
                        ? chatroom['user1']['pets'][0]['petName']
                        : 'Unknown Pet',
                style: TextStyle(fontSize: 18),
              ),
              subtitle: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Text(
                      chatroom['messages']?.isNotEmpty == true
                          ? chatroom['messages'].last['content']
                          : "Let's start the conversation!",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  if (chatroom['messages']?.isNotEmpty == true)
                    Text(
                      _formatTimestamp(chatroom['messages'].last['sentAt']),
                      style: TextStyle(fontSize: 12),
                    ),
                ],
              ),
              onTap: () {
                var roomIDString = chatroom['roomID'];
                String roomID = roomIDString.toString();
                Get.to(() => ChatPage(chatRoomId: roomID));
              },
              contentPadding: EdgeInsets.symmetric(
                  vertical: 10, horizontal: 16),
            );
          },
        );
      }),
    );
  }

  String _formatTimestamp(String timestamp) {
    final DateTime dateTime = DateTime.parse(timestamp).toLocal(); 
    final String formattedDate = DateFormat('yyyy-MM-dd HH:mm').format(dateTime); 
    return formattedDate; 
  }
}
