import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/chat_controller.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/user_controller.dart';
import 'package:tinder_pet_front/app/modules/home/views/prelogin/const.dart';
import 'package:tinder_pet_front/app/routes/app_pages.dart';
import 'package:google_fonts/google_fonts.dart';

class ChatPage extends StatefulWidget {
  final String chatRoomId;

  ChatPage({required this.chatRoomId});

  @override
  _ChatPageState createState() => _ChatPageState();
}
class _ChatPageState extends State<ChatPage> {
  final ChatController chatController = Get.put(ChatController());
  final UserController userController = Get.find<UserController>();
  final ScrollController _scrollController = ScrollController();
@override
void initState() {
  super.initState();
  chatController.fetchChatRoomData(widget.chatRoomId);
  chatController.connectSocket(widget.chatRoomId);
  
  // ใช้ post frame callback
  chatController.messages.listen((_) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _scrollToBottom();
    });
  });
}


@override
void dispose() {
  chatController.disconnectSocket();
  chatController.messages.close(); 
  chatController.dispose();
  _scrollController.dispose();
  super.dispose();
}


  void _scrollToBottom() {
    if (_scrollController.hasClients) {
      _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 80.0,
        flexibleSpace: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                pawColor1,
                Color(0xFFF7F0D2),
              ],
            ),
          ),
          child: Stack(
            children: [
              Positioned(
                top: 0,
                left: 0,
                child: Image.asset(
                  "assets/pets-image/pawpair.png",
                  color: pawColor1,
                  width: 100,
                  height: 100,
                ),
              ),
              Positioned(
                bottom: -10,
                right: 10,
                child: Image.asset(
                  "assets/pets-image/pawpair.png",
                  color: pawColor1,
                  width: 50,
                  height: 50,
                ),
              ),
            ],
          ),
        ),
        elevation: 0,
        title: Center(
          child: Text(
            'PawPair',
            style: GoogleFonts.raleway(
              textStyle: TextStyle(
                color: Colors.white,
                fontSize: 35,
                fontWeight: FontWeight.bold,
                shadows: [
                  Shadow(
                    offset: Offset(1.0, 1.0),
                    blurRadius: 2.0,
                    color: Colors.black.withOpacity(0.5),
                  ),
                ],
                letterSpacing: 10,
                decorationColor: Colors.black,
              ),
            ),
          ),
        ),
        leading: Padding(
          padding: const EdgeInsets.only(left: 10.0),
          child: Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.white,
            ),
            child: SizedBox(
              width: 50,
              height: 50,
              child: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  Get.toNamed(Routes.CHATROOM);
                },
              ),
            ),
          ),
        ),
      ),
      body: Obx(() {
        if (chatController.isLoading.value) {
          return Center(child: CircularProgressIndicator());
        }

        return Column(
          children: [
            Expanded(
              child: ListView.builder(
                controller: _scrollController,
                itemCount: chatController.messages.length,
                itemBuilder: (context, index) {
                  final messageData = chatController.messages[index];
                  final isSender = messageData['userId'] ==
                      userController.getUser['userID']; // ตรวจสอบว่าเป็นผู้ส่ง
                  return chatController.buildMessageBubble(
                    messageData['content'],
                    isSender,
                    messageData['sentAt'],
                  );
                },
              ),
            ),
            _buildMessageInputField(),
          ],
        );
      }),
    );
  }

  Widget _buildMessageInputField() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: [
          Expanded(
            child: TextField(
              controller: chatController.messageController,
              decoration: InputDecoration(
                hintText: 'Type your message here...',
                border: OutlineInputBorder(),
                contentPadding:
                    EdgeInsets.symmetric(vertical: 10, horizontal: 15),
              ),
            ),
          ),
          IconButton(
            icon: Icon(Icons.send),
            onPressed: () {
              chatController.sendMessage(widget.chatRoomId);
            },
          ),
        ],
      ),
    );
  }

  String _formatTimestamp(String timestamp) {
    DateTime utcTime = DateTime.parse(timestamp);
    DateTime thailandTime = utcTime.toUtc().add(Duration(hours: 7));
    String formattedTime = '${thailandTime.day.toString().padLeft(2, '0')}/'
        '${thailandTime.month.toString().padLeft(2, '0')}/'
        '${thailandTime.year} '
        '${thailandTime.hour.toString().padLeft(2, '0')}:'
        '${thailandTime.minute.toString().padLeft(2, '0')}:';
    return formattedTime;
  }
}
