import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:get/get.dart';
import 'package:tinder_pet_front/app/data/service/sub_service.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/user_controller.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/userprofile_controller.dart';
import 'package:tinder_pet_front/app/modules/home/views/prelogin/const.dart';
import '../../../routes/app_pages.dart';

class DetailSub {
  final String text, image;

  DetailSub({required this.text, required this.image});
}

List<DetailSub> sudData = [
  DetailSub(
    text: 'Sign up today to receive additional \nprivileges from us.',
    image: "assets/pets-image/sub3.png",
  ),
  DetailSub(
    text:
        'Members will be able to add more pets  \nto the system than regular users.',
    image: "assets/pets-image/sub2.png",
  ),
  DetailSub(
    text:
        "Members will have exclusive privileges \nto pair pets with other members only.",
    image: "assets/pets-image/sub5.png",
  ),
  DetailSub(
    text: "Special privileges for matching pets \nare waiting for you.",
    image: "assets/pets-image/sub4.png",
  ),
];

class SUBSCRIPTION extends StatefulWidget {
  const SUBSCRIPTION({Key? key}) : super(key: key);

  @override
  State<SUBSCRIPTION> createState() => _SUBSCRIPTIONState();
}

class _SUBSCRIPTIONState extends State<SUBSCRIPTION> {
  final PageController _pageController = PageController();
  final SubService service = Get.put(SubService());
  final UserProfileController userProfileController =
      Get.put(UserProfileController());
  final UserController userController = Get.put(UserController());
  int currentPage = 0;
  bool isSubscribed = false;




  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    String statusMember = userController.currentUser['statusMember'];
    print(statusMember);
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 80.0,
        flexibleSpace: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                pawColor1,
                Color(0xFFF7F0D2),
              ],
            ),
          ),
          child: Stack(
            children: [
              Positioned(
                top: 0,
                left: 0,
                child: Image.asset(
                  "assets/pets-image/pawpair.png",
                  color: pawColor1,
                  width: 100,
                  height: 100,
                ),
              ),
              Positioned(
                bottom: -10,
                right: 10,
                child: Image.asset(
                  "assets/pets-image/pawpair.png",
                  color: pawColor1,
                  width: 50,
                  height: 50,
                ),
              ),
            ],
          ),
        ),
        elevation: 0,
        title: Center(
          child: Text(
            'Upgrade Member',
            style: GoogleFonts.raleway(
              textStyle: const TextStyle(
                color: Colors.white,
                fontSize: 30,
                fontWeight: FontWeight.bold,
                shadows: [
                  Shadow(
                    offset: Offset(1.0, 1.0),
                    blurRadius: 2.0,
                    color: Colors.black,
                  ),
                ],
                letterSpacing: 10,
              ),
            ),
          ),
        ),
        leading: Padding(
          padding: const EdgeInsets.only(left: 10.0),
          child: Container(
            decoration: const BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.white,
            ),
            child: SizedBox(
              width: 50,
              height: 50,
              child: IconButton(
                icon: const Icon(Icons.arrow_back),
                onPressed: () {
                  Get.toNamed(Routes.PROFILESCREEN);
                },
              ),
            ),
          ),
        ),
      ),
      body: Stack(
        children: [
          Image.asset(
            'assets/pets-image/background3.png',
            fit: BoxFit.cover,
            width: size.width,
            height: size.height,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: size.height * 0.7,
                child: PageView.builder(
                  controller: _pageController,
                  itemCount: sudData.length,
                  onPageChanged: (value) {
                    setState(() {
                      currentPage = value;
                    });
                  },
                  itemBuilder: (context, index) {
                    return _buildPageWithImage(
                      imagePath: sudData[index].image,
                      title: 'Subscription Benefits',
                      description: sudData[index].text,
                    );
                  },
                ),
              ),
              GestureDetector(
                onTap: isSubscribed
                    ? null // ถ้า isSubscribed เป็น true จะไม่ทำอะไร
                    : () {
                        if (currentPage == sudData.length - 1) {
                          _showSubscriptionDialog(); // แสดง Dialog สำหรับการสมัคร
                        } else {
                          _pageController.nextPage(
                            duration: const Duration(milliseconds: 300),
                            curve: Curves.ease,
                          );
                        }
                      },
                child: Container(
                  height: 70,
                  width: size.width * 0.6,
                  decoration: BoxDecoration(
                    color: isSubscribed ? Colors.grey : buttonColor,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Center(
                    child: Text(
                      isSubscribed // แสดงข้อความตามสถานะ
                          ? "Already Subscribed" // ถ้าสมัครแล้วจะแสดงข้อความนี้
                          : (currentPage == sudData.length - 1
                              ? "Sign up now"
                              : "Next"),
                      style: const TextStyle(
                        fontSize: 20,
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: List.generate(
                  sudData.length,
                  (index) => indicatorForSlider(index: index),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildPageWithImage(
      {required String imagePath,
      required String title,
      required String description}) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(imagePath, height: 250, fit: BoxFit.cover),
        const SizedBox(height: 30),
        RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            style: const TextStyle(
              fontSize: 28.0,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
            children: [
              TextSpan(
                text: "Subscription ",
                style: TextStyle(
                  color: Colors.lightBlue,
                  fontWeight: FontWeight.w900,
                ),
              ),
              const TextSpan(text: "Benefits"),
            ],
          ),
        ),
        const SizedBox(height: 16.0),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
          padding: const EdgeInsets.all(16.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.orange.shade100,
          ),
          child: Text(
            description,
            style: const TextStyle(fontSize: 18.0, color: Colors.black),
            textAlign: TextAlign.center,
          ),
        ),
      ],
    );
  }

  Widget indicatorForSlider({required int index}) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 300),
      height: 10,
      width: currentPage == index ? 30 : 10,
      margin: const EdgeInsets.only(right: 5),
      decoration: BoxDecoration(
        color: currentPage == index ? Colors.orange : Colors.grey,
        borderRadius: BorderRadius.circular(5),
      ),
    );
  }

 void _showSubscriptionDialog() {
    String? selectedPaymentMethod;
    final TextEditingController cardNumberController = TextEditingController();
    final TextEditingController cvvController = TextEditingController();

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder(
          builder: (context, setState) {
            return AlertDialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              title: const Text(
                "Confirm your subscription",
                style: TextStyle(
                  fontSize: 22,
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              ),
              content: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const Text(
                      "Do you want to subscribe for \$179 Baht month?",
                      style: TextStyle(fontSize: 18, color: Colors.black87),
                    ),
                    const SizedBox(height: 20),
                    const Text(
                      "Select Payment Method:",
                      style: TextStyle(fontSize: 16, color: Colors.black54),
                    ),
                    Row(
                      children: [
                        Radio(
                          value: 'QR Code',
                          groupValue: selectedPaymentMethod,
                          onChanged: (value) {
                            setState(() {
                              selectedPaymentMethod = value as String?;
                            });
                          },
                        ),
                        const Text('QR Code'),
                      ],
                    ),
                    Row(
                      children: [
                        Radio(
                          value: 'Credit Card',
                          groupValue: selectedPaymentMethod,
                          onChanged: (value) {
                            setState(() {
                              selectedPaymentMethod = value as String?;
                            });
                          },
                        ),
                        const Text('Credit Card'),
                      ],
                    ),
                    if (selectedPaymentMethod == 'Credit Card') ...[
                      const SizedBox(height: 20),
                      TextField(
                        controller: cardNumberController,
                        keyboardType: TextInputType.number,
                        decoration: const InputDecoration(
                          labelText: 'Card Number',
                          border: OutlineInputBorder(),
                        ),
                      ),
                      const SizedBox(height: 20),
                      TextField(
                        controller: cvvController,
                        keyboardType: TextInputType.number,
                        decoration: const InputDecoration(
                          labelText: 'CVV',
                          border: OutlineInputBorder(),
                        ),
                      ),
                    ],
                    if (selectedPaymentMethod == 'QR Code') ...[
                      const SizedBox(height: 10),
                      Image.asset(
                        'assets/pets-image/qrcode.png',
                        height: 300,
                        fit: BoxFit.cover,
                      ),
                    ],
                  ],
                ),
              ),
              actions: [
                TextButton(
                  child: const Text(
                    "Cancel",
                    style: TextStyle(color: Colors.red),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                TextButton(
                  child: const Text(
                    "Confirm",
                    style: TextStyle(color: Colors.blue),
                  ),
                  onPressed: () {
                    if (selectedPaymentMethod == null) {
                      Navigator.of(context).pop(); // Close dialog
                      _showErrorDialog(); // Show error dialog
                    } else {
                      service.signInMember(selectedPaymentMethod!);
                      setState(() {
                        isSubscribed = true; // อัปเดตสถานะเมื่อสมัครเสร็จ
                      });
                      Navigator.of(context).pop(); // Close dialog
                      _showThankYouDialog(); // Show thank you dialog
                    }
                  },
                ),
              ],
            );
          },
        );
      },
    );
  }

  void _showErrorDialog() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: const [
              Icon(
                Icons.error_outline,
                color: Colors.red,
                size: 100,
              ),
              SizedBox(height: 20),
              Text(
                "Please select a payment method",
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              ),
            ],
          ),
        );
      },
    );

    Future.delayed(const Duration(seconds: 2), () {
      Navigator.of(context).pop(); // Close error dialog after 2 seconds
    });
  }

  void _showThankYouDialog() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const Icon(
                Icons.check_circle_outline,
                color: Colors.green,
                size: 100,
              ),
              const SizedBox(height: 20),
              const Text(
                "Thank you for subscribing",
                style: TextStyle(
                  fontSize: 22,
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              ),
            ],
          ),
        );
      },
    );

    Future.delayed(const Duration(seconds: 2), () {
      Navigator.of(context).pop(); // Close thank you dialog after 2 seconds
    });
  }
}
