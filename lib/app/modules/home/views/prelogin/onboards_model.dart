class OnBoards {
  final String text, image;

  OnBoards({required this.text, required this.image});
}

List<OnBoards> onBoardData = [
  OnBoards(
    text: 'Join us and discover the perfect pet \nthat matches your heart.',
    image: "assets/pets-image/image1.png",
  ),
  OnBoards(
    text: "We help people connect with pet \naround your Location",
    image: "assets/pets-image/woman-with-cat1.png",
  ),
  OnBoards(
    text: "We show the easy way to to find the perfect  \nmatch for your pet Just stay at home with us.",
    image: "assets/pets-image/image3.png",
  ),
];