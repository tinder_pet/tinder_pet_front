import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:tinder_pet_front/app/modules/home/views/screens/detail_mypet_body.dart';
import 'package:tinder_pet_front/app/routes/app_pages.dart';

class Detail_Pet extends StatelessWidget {
  final Map<String, dynamic> pets;

  const Detail_Pet({Key? key, required this.pets})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFFFF0F5),
      appBar: buildAppBar(),
      body: DetailMyPetBody(
        pets: pets,
      ),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      backgroundColor: Colors.amber,
      elevation: 0,
      title: Text(
        "Pet Datail",
        style: GoogleFonts.raleway(
          textStyle: TextStyle(
            color: Colors.white,
            fontSize: 35,
            fontWeight: FontWeight.bold,
            shadows: [
              Shadow(
                offset: Offset(1.0, 1.0),
                blurRadius: 2.0,
                color: Colors.black.withOpacity(0.5),
              ),
            ],
            letterSpacing: 10,
            decorationColor: Colors.black,
          ),
        ),
      ),
      leading: Padding(
        padding: const EdgeInsets.only(left: 10.0),
        child: Container(
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.white,
          ),
          child: SizedBox(
            width: 50,
            height: 50,
            child: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Get.toNamed(Routes.MYPET);
              },
            ),
          ),
        ),
      ),
      centerTitle: true,
    );
  }
}
