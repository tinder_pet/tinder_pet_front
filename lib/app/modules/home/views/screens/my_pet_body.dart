import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/user_controller.dart';
import 'package:tinder_pet_front/app/modules/home/views/screens/detail_pet.dart';
import 'package:tinder_pet_front/app/routes/app_pages.dart';

class MyPetBody extends StatelessWidget {
  final UserController userController = Get.put(UserController());

  @override
  Widget build(BuildContext context) {
    List<dynamic> pets = userController.currentUser['pets'];
    print("pets body : ${pets}");

    return SafeArea(
      bottom: false,
      child: Column(
        children: <Widget>[
          SizedBox(height: 10.0),
          Text(
            "Welcome to your pet space! Here, you can easily view \nand manage all of your pets",
            style: TextStyle(
              fontSize: 18,
              color: Colors.white,
              fontWeight: FontWeight.w900,
              height: 1.2,
            ),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 10.0),
          Expanded(
            child: Stack(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 70),
                  decoration: BoxDecoration(
                    color: Color(0xFFFFF0F5),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(40),
                      topRight: Radius.circular(40),
                    ),
                  ),
                ),
                ListView.builder(
                  itemCount: pets.length,
                  itemBuilder: (context, index) => PetCard(
                    itemIndex: index,
                    pets: pets[index],
                    onPress: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => Detail_Pet(
                            pets: pets[index],
                          ),
                        ),
                      );
                    },
                  ),
                ),
                // ปุ่ม Edit
                if (userController.currentUser["statusMember"] == "active")
                  Positioned(
                    bottom: 20, // ระยะห่างจากด้านล่าง
                    left: MediaQuery.of(context).size.width * 0.05, // จัดกลาง
                    child: Container(
                      padding: EdgeInsets.only(top: 3, left: 3),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        border: Border.all(color: Colors.black),
                      ),
                      child: SizedBox(
                        width: MediaQuery.of(context).size.width * 0.9,
                        height: 60,
                        child: ElevatedButton(
                          onPressed: () => Get.toNamed(Routes.FILEPICKERPET),
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.amber,
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(50),
                            ),
                          ),
                          child: Text(
                            'Add Pet',
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              color: Colors.black,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class PetCard extends StatelessWidget {
  const PetCard({
    super.key,
    required this.itemIndex,
    required this.onPress,
    required this.pets,
  });

  final int itemIndex;
  final VoidCallback onPress;
  final Map<String, dynamic> pets;

  @override
  Widget build(BuildContext context) {
    String? imageUrl;
    if (pets['images'] != null && pets['images'].isNotEmpty) {
      imageUrl =
          'http://localhost:3000/image/image/${pets["images"][0]["image"]}';
    } else if (pets['imageUrl'] != null) {
      imageUrl = '${pets["imageUrl"]}';
    } else {
      imageUrl = 'http://localhost:3000/image/image/default.jpg';
    }
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16.0, vertical: 1.0),
      height: 160,
      child: InkWell(
        onTap: onPress,
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            Container(
              height: 136,
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.2),
                    spreadRadius: 2,
                    blurRadius: 10,
                    offset: Offset(0, 5),
                  ),
                ],
                borderRadius: BorderRadius.circular(22.0),
                color: itemIndex.isEven ? Colors.black12 : Colors.amber,
              ),
              child: Container(
                margin: EdgeInsets.only(right: 10),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(22),
                ),
              ),
            ),
            Positioned(
              top: 40,
              right: 20,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 10),
                height: 100,
                width: 120,
                child: ClipOval(
                  child: imageUrl != null
                      ? Image.network(
                          imageUrl,
                          fit: BoxFit.cover,
                        )
                      : Icon(Icons.pets, size: 80, color: Colors.grey),
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              child: SizedBox(
                height: 136,
                width: MediaQuery.of(context).size.width - 200,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Spacer(),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      child: Text(
                        pets["petDes"],
                        style: Theme.of(context).textTheme.bodyMedium,
                      ),
                    ),
                    Spacer(),
                    Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: 10.0,
                        vertical: 5,
                      ),
                      decoration: BoxDecoration(
                        color: Colors.amber,
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(22),
                          topRight: Radius.circular(22),
                        ),
                      ),
                      child: Text(
                        pets["petName"],
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
