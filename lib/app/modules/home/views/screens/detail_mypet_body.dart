import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:tinder_pet_front/app/modules/home/views/edit_pet.dart';
import 'package:tinder_pet_front/app/modules/home/views/match/colors.dart';
import 'package:tinder_pet_front/app/modules/home/views/prelogin/const.dart';

class DetailMyPetBody extends StatelessWidget {
  final Map<String, dynamic> pets;

  const DetailMyPetBody({super.key, required this.pets});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    String imageUrl;

    if (pets == null || pets.isEmpty) {
      return Center(child: CircularProgressIndicator());
    }

    if (pets['images'] != null && pets['images'].isNotEmpty) {
      imageUrl =
          'http://localhost:3000/image/image/${pets["images"][0]["image"]}';
    } else if (pets['imageUrl'] != null) {
      imageUrl = '${pets["imageUrl"]}';
    } else {
      imageUrl =
          'http://localhost:3000/image/image/cd3b2327-b70a-41fc-a0d2-407eab6e2196.jpg';
    }

    return SingleChildScrollView(
      child: Column(
        children: [
          TreeBox(size, imageUrl),
          SizedBox(height: 8),
          Padding(
            padding: const EdgeInsets.all(16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 16.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: _moreInfo(color1, pets["sex"], "Sex"),
                    ),
                    Expanded(
                      child: _moreInfo(color2, "${pets["age"]} Month", "Age"),
                    ),
                    Expanded(
                      child:
                          _moreInfo(color3, "${pets["weight"]} KG", "Weight"),
                    ),
                  ],
                ),
                const SizedBox(height: 8),
                SizedBox(height: 20.0),
                Text(
                  'ABOUT MY PET',
                  style: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                    color: ColorConstants.secondary,
                  ),
                ),
                const SizedBox(height: 4),
                Text(
                  pets["petDes"],
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.grey[600],
                  ),
                ),
                const SizedBox(height: 16),
                Text(
                  'BEHAVIORS',
                  style: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                    color: ColorConstants.secondary,
                  ),
                ),
                const SizedBox(height: 4),
                Text(
                  pets["specialBehaviors"],
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.grey[600],
                  ),
                ),
                const SizedBox(height: 16),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'FUNDAMENTAL DETAILS',
                      style: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.bold,
                        color: ColorConstants.secondary,
                      ),
                    ),
                    const SizedBox(height: 4),
                    _buildDetailRow(pets["color"]),
                    _buildDetailRow(pets["furLength"]),
                    _buildDetailRow(pets["parentingStatus"]),
                    _buildDetailRow(pets["sterileState"]),
                  ],
                ),
                const SizedBox(height: 16),
                Text(
                  'HEALTH',
                  style: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                    color: ColorConstants.secondary,
                  ),
                ),
                const SizedBox(height: 4),
                Text(
                  pets["healthHistory"],
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.grey[600],
                  ),
                ),
                const SizedBox(height: 16),
                Column(
                  children: [
                    _buildVaccinesAndFoods(),
                  ],
                ),
              ],
            ),
          ),
          const SizedBox(height: 16),
          Container(
            padding: EdgeInsets.only(top: 3, left: 3),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              border: Border.all(color: Colors.black),
            ),
            child: SizedBox(
              width: MediaQuery.of(context).size.width * 0.9,
              height: 60,
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => EditPet(pets: pets),
                    ),
                  );
                },
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.amber,
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50),
                  ),
                ),
                child: Text(
                  'Edit',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.black,
                    fontSize: 18,
                  ),
                ),
              ),
            ),
          ),
          SizedBox(height: 20),
        ],
      ),
    );
  }

  Widget _buildDetailRow(String detail) {
    return Row(
      children: [
        CircleAvatar(
          radius: 10,
          backgroundColor: Colors.grey[400],
        ),
        const SizedBox(width: 8),
        Text(
          detail,
          style: TextStyle(
            fontSize: 16,
            color: Colors.grey[600],
          ),
        ),
      ],
    );
  }

  Widget _buildVaccinesAndFoods() {
    List<String> vaccines = pets["vaccine"].split(',');
    List<String> foods = pets["foodsEaten"].split(',');

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'VACCINES',
          style: TextStyle(
            fontSize: 22,
            fontWeight: FontWeight.bold,
            color: ColorConstants.secondary,
          ),
        ),
        const SizedBox(height: 4),
        for (String vaccine in vaccines)
          Row(
            children: [
              Icon(Icons.vaccines, color: Colors.green, size: 16),
              const SizedBox(width: 8),
              Text(
                vaccine.trim(),
                style: TextStyle(
                  fontSize: 16,
                  color: Colors.grey[600],
                ),
              ),
            ],
          ),
        const SizedBox(height: 16),
        Text(
          'FOODS EATEN',
          style: TextStyle(
            fontSize: 22,
            fontWeight: FontWeight.bold,
            color: ColorConstants.secondary,
          ),
        ),
        const SizedBox(height: 4),
        for (String food in foods)
          Row(
            children: [
              Icon(Icons.food_bank, color: Colors.orange, size: 16),
              const SizedBox(width: 8),
              Text(
                food.trim(),
                style: TextStyle(
                  fontSize: 16,
                  color: Colors.grey[600],
                ),
              ),
            ],
          ),
      ],
    );
  }

  Widget TreeBox(Size size, String image) {
    return Column(
      children: <Widget>[
        Container(
          width: double.infinity,
          padding: EdgeInsets.symmetric(horizontal: 16.0),
          decoration: BoxDecoration(
            color: Colors.amber,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(50),
              bottomRight: Radius.circular(50),
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Center(
                child: PetPoster(size: size, image: image),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 1),
                child: Row(
                  children: [
                    Text(
                      pets["petName"],
                      style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                          color: Colors.white,
                          fontSize: 22,
                          fontWeight: FontWeight.bold,
                          shadows: [
                            Shadow(
                              offset: Offset(1.0, 1.0),
                              blurRadius: 2.0,
                              color: Colors.black.withOpacity(0.5),
                            ),
                          ],
                          letterSpacing: 5,
                        ),
                      ),
                    ),
                    Icon(
                      pets["sex"] == 'Male' ? Icons.male : Icons.female,
                      size: 28,
                      color: ColorConstants.secondary,
                    ),
                  ],
                ),
              ),
              Text(
                "Description Pet",
                style: GoogleFonts.raleway(
                  textStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                    shadows: [
                      Shadow(
                        offset: Offset(1.0, 1.0),
                        blurRadius: 2.0,
                        color: Colors.black.withOpacity(0.5),
                      ),
                    ],
                    letterSpacing: 5,
                  ),
                ),
              ),
              SizedBox(height: 4),
              Row(
                children: [
                  Icon(
                    Icons.pets,
                    color: const Color.fromARGB(255, 83, 53, 19),
                  ),
                  Text(
                    '${pets["breed"]}',
                    style: TextStyle(
                        color: Colors.black.withOpacity(0.6), fontSize: 16),
                  ),
                ],
              ),
              SizedBox(height: 4),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 8.0),
                child: Text(
                  pets["petDes"],
                  style: TextStyle(color: Colors.black54),
                ),
              ),
              SizedBox(height: 16.0),
            ],
          ),
        ),
      ],
    );
  }
}

Widget _moreInfo(Color color, String value, String label) {
  return ClipRRect(
    borderRadius: BorderRadius.circular(20),
    child: Stack(
      children: [
        Positioned(
          bottom: -20,
          right: 0,
          child: Transform.rotate(
            angle: 12,
            child: Image.asset(
              'pets-image/pawpair.png',
              color: Colors.black12,
              height: 55,
            ),
          ),
        ),
        Container(
          height: 100,
          width: 150,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: color,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                value,
                style: const TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                label,
                style: const TextStyle(
                  fontSize: 16,
                  color: Colors.black54,
                ),
              ),
            ],
          ),
        ),
      ],
    ),
  );
}

class PetPoster extends StatelessWidget {
  const PetPoster({
    super.key,
    required this.size,
    required this.image,
  });

  final Size size;
  final String image;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20.0),
      height: size.height * 0.3,
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Center(
            child: Container(
              height: size.width * 0.6,
              width: size.width * 0.6,
              decoration: BoxDecoration(
                color: Colors.white,
                shape: BoxShape.circle,
              ),
            ),
          ),
          ClipOval(
            child: Image.network(
              image,
              height: size.width * 0.5,
              width: size.width * 0.5,
              fit: BoxFit.cover,
            ),
          ),
        ],
      ),
    );
  }
}
