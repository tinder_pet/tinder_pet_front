import 'package:flutter/material.dart';

const List<Color> gradientColor = [
  Color(0xFFf7b803),
  Color(0xFFf7d54f), // สีที่สอง (สีที่ไล่ระดับ)
  Color(0xFFf7e6a8), // สีที่สาม (สีที่ไล่ระดับ)
];

Color bubbleColor = const Color(0x26FFFFFF);
Color buttonColor = const Color(0xff5E72E4);