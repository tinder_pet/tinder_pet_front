import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/bill_controller.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/user_controller.dart';
import 'package:tinder_pet_front/app/modules/home/views/prelogin/const.dart';
import 'package:intl/intl.dart';
import 'package:tinder_pet_front/app/routes/app_pages.dart';

class Billing extends StatefulWidget {
  const Billing({Key? key}) : super(key: key);

  @override
  State<Billing> createState() => _BillingState();
}

class _BillingState extends State<Billing> {
  final UserController userController = Get.put(UserController());
  final BillController billController = Get.put(BillController());

  @override
  void initState() {
    super.initState();
    ever(billController.currentBill, (_) {
      setState(() {}); // รีเฟรชหน้าจอเมื่อข้อมูลใน currentBill เปลี่ยนแปลง
    });
    billController.fetchMember(); // โหลดข้อมูลผู้ใช้
  }

  @override
  Widget build(BuildContext context) {
    // Extract createdAt and endDate from user data
    String createdAtString = billController.currentBill.isNotEmpty
        ? billController.currentBill[0]["createdAt"]
        : "";

    String endDateString = billController.currentBill.isNotEmpty
        ? billController.currentBill[0]["endDate"]
        : "";

    // Parse dates
    DateTime? createdAt =
        createdAtString.isNotEmpty ? DateTime.tryParse(createdAtString) : null;
    DateTime? endDate =
        endDateString.isNotEmpty ? DateTime.tryParse(endDateString) : null;

    // Get current date in Thailand time (UTC+7)
    DateTime now = DateTime.now().toUtc().add(const Duration(hours: 7));

    // Calculate remaining days
    int differenceInDays = endDate != null ? endDate.difference(now).inDays : 0;

    print("endDateString : ${endDateString}");
    print("endDate : ${endDate}");
    print("now : ${now}");
    print("differenceInDays : ${differenceInDays}");
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 255, 248, 228),
      appBar: AppBar(
        toolbarHeight: 80.0,
        flexibleSpace: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [pawColor1, Color(0xFFF7F0D2)],
            ),
          ),
          child: Stack(
            children: [
              Positioned(
                top: 0,
                left: 0,
                child: Image.asset(
                  "assets/pets-image/pawpair.png",
                  color: pawColor1,
                  width: 100,
                  height: 100,
                ),
              ),
              Positioned(
                bottom: -10,
                right: 10,
                child: Image.asset(
                  "assets/pets-image/pawpair.png",
                  color: pawColor1,
                  width: 50,
                  height: 50,
                ),
              ),
            ],
          ),
        ),
        elevation: 0,
        title: Center(
          child: Text(
            'Billing',
            style: GoogleFonts.raleway(
              textStyle: const TextStyle(
                color: Colors.white,
                fontSize: 30,
                fontWeight: FontWeight.bold,
                shadows: [
                  Shadow(
                      offset: Offset(1.0, 1.0),
                      blurRadius: 2.0,
                      color: Colors.black)
                ],
                letterSpacing: 10,
              ),
            ),
          ),
        ),
        leading: Padding(
          padding: const EdgeInsets.only(left: 10.0),
          child: Container(
            decoration: const BoxDecoration(
                shape: BoxShape.circle, color: Colors.white),
            child: SizedBox(
              width: 50,
              height: 50,
              child: IconButton(
                icon: const Icon(Icons.arrow_back),
                onPressed: () => Get.toNamed(Routes.PROFILESCREEN),
              ),
            ),
          ),
        ),
      ),
      body: Column(
        children: [
          const SizedBox(height: 15.0),
          Obx(() {
            return Container(
              margin: const EdgeInsets.all(25),
              height: 100,
              padding: const EdgeInsets.fromLTRB(30, 20, 30, 20),
              decoration: BoxDecoration(
                color: const Color.fromARGB(255, 255, 234, 167),
                borderRadius: BorderRadius.circular(20),
                border: Border.all(
                    color: const Color.fromARGB(255, 0, 0, 0), width: 3.0),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: 100.0,
                    width: 80,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(16.0)),
                    child:
                        const Icon(Icons.wallet, color: Colors.blue, size: 50),
                  ),
                  const SizedBox(width: 16.0),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      if (userController.currentUser["statusMember"] ==
                          "active")
                        Column(
                          children: [
                            const Text(
                              "You can use Pawspair+",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              "$differenceInDays Day",
                              style: const TextStyle(
                                  color: Colors.black,
                                  fontSize: 22,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      if (userController.currentUser["statusMember"] == "none")
                        const Text(
                          "Your Pawspair+ subscription is inactive",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.bold),
                        ),
                    ],
                  ),
                ],
              ),
            );
          }),
          const SizedBox(height: 5.0),

          // Transactions title and divider
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  'Transactions',
                  style: TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.bold,
                      color: Color.fromARGB(255, 150, 150, 150)),
                ),
                const Divider(
                    thickness: 2.0, color: Color.fromARGB(255, 148, 148, 148)),
              ],
            ),
          ),
          Expanded(
            child: Obx(() {
              if (billController.currentBill.isNotEmpty) {
                return ListView.builder(
                  padding: const EdgeInsets.symmetric(horizontal: 25),
                  itemCount: billController.currentBill.length,
                  itemBuilder: (context, index) {
                    var transaction = billController.currentBill[index];
                    return _buildTransactionItem(transaction);
                  },
                );
              } else {
                return Container(
                  padding: const EdgeInsets.all(16.0),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(12)),
                  child: const Center(
                    child: Text(
                      'No transaction yet',
                      style: TextStyle(fontSize: 16.0, color: Colors.black54),
                    ),
                  ),
                );
              }
            }),
          ),
        ],
      ),
    );
  }

  Widget _buildTransactionItem(Map<String, dynamic> transaction) {
    return Container(
      margin: const EdgeInsets.only(bottom: 10),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(12),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 2,
            blurRadius: 5,
            offset: const Offset(0, 3),
          ),
        ],
      ),
      child: ListTile(
        contentPadding: const EdgeInsets.all(16),
        leading: const CircleAvatar(
          backgroundColor: Colors.blue,
          child: Text("T"),
        ),
        title: Text(
          '${transaction["payment"]}',
          style: const TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
        ),
        subtitle: Text(
          DateFormat('d/M/yyyy hh:mm a')
              .format(DateTime.parse(transaction["createdAt"]).toLocal()),
          style: const TextStyle(
              fontSize: 12.5,
              fontWeight: FontWeight.bold,
              color: Colors.black38),
        ),
        trailing: Text(
          '- ${transaction["totalPrice"]} Bath',
          style: const TextStyle(
              fontSize: 14.0, fontWeight: FontWeight.bold, color: Colors.red),
        ),
      ),
    );
  }
}
