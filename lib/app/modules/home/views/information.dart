import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:tinder_pet_front/app/modules/home/views/prelogin/const.dart';
import 'package:tinder_pet_front/app/routes/app_pages.dart';


class INFORMATION extends StatefulWidget {
  const INFORMATION({Key? key}) : super(key: key);

  @override
  State<INFORMATION> createState() => _INFORMATIONState();
}

class _INFORMATIONState extends State<INFORMATION> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 80.0,
        flexibleSpace: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                pawColor1,
                Color(0xFFF7F0D2),
              ],
            ),
          ),
          child: Stack(
            children: [
              Positioned(
                top: 0,
                left: 0,
                child: Image.asset(
                  "assets/pets-image/pawpair.png",
                  color: pawColor1,
                  width: 100,
                  height: 100,
                ),
              ),
              Positioned(
                bottom: -10,
                right: 10,
                child: Image.asset(
                  "assets/pets-image/pawpair.png",
                  color: pawColor1,
                  width: 50,
                  height: 50,
                ),
              ),
            ],
          ),
        ),
        elevation: 0,
        title: Center(
          child: Text(
            'Catalyst@Collective',
            style: GoogleFonts.raleway(
              textStyle: TextStyle(
                  color: Colors.white,
                fontSize: 25,
                fontWeight: FontWeight.bold,
                shadows: [
                  Shadow(
                    offset: Offset(1.0, 1.0),
                    blurRadius: 2.0,
                    color: Colors.black.withOpacity(0.5),
                  ),
                ],
                decorationColor: Colors.black,
              ),
            ),
          ),
        ),
        leading: Padding(
          padding: const EdgeInsets.only(left: 10.0),
          child: Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.white,
            ),
            child: SizedBox(
              width: 50,
              height: 50,
              child: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  Get.toNamed(Routes.PROFILESCREEN);
                },
              ),
            ),
          ),
        ),
      ),
      body: Center( // จัดให้องค์ประกอบอยู่กลางหน้าจอ
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start, // จัดให้องค์ประกอบเริ่มจากด้านบน
          crossAxisAlignment: CrossAxisAlignment.center, // จัดตรงกลางแนวขวาง
          children: [
            const SizedBox(height: 30), // ระยะห่างเล็กน้อยระหว่าง AppBar และ "Member"
            
            // กรอบวงรีสำหรับ Member
            Container(
              width: MediaQuery.of(context).size.width * 0.7,
              height: 50,
              padding: const EdgeInsets.only(top: 3, left: 3), // Padding ข้างในกรอบ
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50), // มุมโค้ง
                border: Border.all(color: Colors.black), // เส้นกรอบ
                color: orangeContainer, // สีพื้นหลังของ Container
              ),
              child: Center( // จัดให้อยู่กลาง
                child: Text(
                  'Member',
                  style: GoogleFonts.kanit(
                    fontSize: 32,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),

            const SizedBox(height: 5), // ระยะห่างเล็กน้อยระหว่าง "Member" และรายการสมาชิก
            
            // กรอบวงรีใหญ่ครอบรายการสมาชิกทั้งหมด
            Container(
              width: MediaQuery.of(context).size.width * 0.8, // กำหนดความกว้างของกรอบใหญ่
              padding: const EdgeInsets.all(16), // Padding ข้างในกรอบใหญ่
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30), // มุมโค้งของกรอบใหญ่
                border: Border.all(color: Colors.black), // เส้นกรอบ
                color: Colors.white, // สีพื้นหลังของกรอบใหญ่
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start, // จัดรายการให้เสมอกันทางซ้าย
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        '1.',
                        style: GoogleFonts.kanit(
                          fontSize: 20,
                        ),
                      ),
                      SizedBox(width: 10), // เพิ่มระยะห่างระหว่างเลขกับข้อความ
                      Expanded(
                        child: Text(
                          '64160040 กันตินัน ศรีเกษม \nBack-end, DB',
                          style: GoogleFonts.kanit(
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 10),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        '2.',
                        style: GoogleFonts.kanit(
                          fontSize: 20,
                        ),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: Text(
                          '64160233 กุลิสรา ทองงามขำ \nFull stack',
                          style: GoogleFonts.kanit(
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 10),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        '3.',
                        style: GoogleFonts.kanit(
                          fontSize: 20,
                        ),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: Text(
                          '64160245 นฤบดี มากวิสุทธิ์ \nFull stack',
                          style: GoogleFonts.kanit(
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 10),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        '4.',
                        style: GoogleFonts.kanit(
                          fontSize: 20,
                        ),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: Text(
                          '64160248 พิมพ์ลดา โสนาพูน \nFront-end, Tester',
                          style: GoogleFonts.kanit(
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 10),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        '5.',
                        style: GoogleFonts.kanit(
                          fontSize: 20,
                        ),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: Text(
                          '64160251 ยุทธนา ชัยมูล \nBack-end',
                          style: GoogleFonts.kanit(
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 10),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        '6.',
                        style: GoogleFonts.kanit(
                          fontSize: 20,
                        ),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: Text(
                          '64160261 พิเชษฐ์ ผาโท \nFront-end, DB',
                          style: GoogleFonts.kanit(
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 10),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        '7.',
                        style: GoogleFonts.kanit(
                          fontSize: 20,
                        ),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: Text(
                          '64160263 วรเชษฐ์ ศิริเธียรไชย \nBack-end, Tester',
                          style: GoogleFonts.kanit(
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}