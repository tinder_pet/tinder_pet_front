import 'package:appinio_swiper/appinio_swiper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tinder_pet_front/app/data/service/socket_io_client.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/filter_controller.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/home_controller.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/match_controller.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/noti_controller.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/user_controller.dart';
import 'package:tinder_pet_front/app/modules/home/views/match/action_button.dart';
import 'package:tinder_pet_front/app/modules/home/views/match/colors.dart';
import 'package:tinder_pet_front/app/modules/home/views/match/profile_card.dart';
import 'package:tinder_pet_front/app/modules/home/views/prelogin/const.dart';
import 'package:tinder_pet_front/app/routes/app_pages.dart';
import 'package:google_fonts/google_fonts.dart';
import 'dart:async';

class HomeView extends StatefulWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> with TickerProviderStateMixin {
  final HomeController controller = Get.put(HomeController());
  final MatchController matchController = Get.put(MatchController());
  final UserController userController = Get.put(UserController());
  final AppinioSwiperController swiperController = AppinioSwiperController();
  late AnimationController _likeController;
  late AnimationController _dislikeController;
  final FilterController filtercontroller = Get.put(FilterController());

  int petid = 0;
  @override
  void initState() {
    super.initState();
    Get.lazyPut<NotificationController>(() => NotificationController());
    WidgetsBinding.instance.addPostFrameCallback((_) {});
    _likeController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 500),
    );

    _dislikeController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 500),
    );
  }

  @override
  void dispose() {
    Get.delete<NotificationController>();
    _likeController.dispose();
    _dislikeController.dispose();
    super.dispose();
  }

  void _animateHeart() {
    _likeController.forward().then((_) {
      _likeController.reverse();
    });
  }

  void _animateCross() {
    _dislikeController.forward().then((_) {
      _dislikeController.reverse();
    });
  }

  @override
  Widget build(BuildContext context) {
    final NotificationController notificationController = Get.find<NotificationController>();
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 80.0,
        flexibleSpace: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                pawColor1,
                Color.fromARGB(255, 247, 240, 210),
              ],
            ),
          ),
          child: Stack(
            children: [
              Positioned(
                top: 0,
                left: 0,
                child: Image.asset(
                  "assets/pets-image/pawpair.png",
                  color: pawColor1,
                  width: 100,
                  height: 100,
                ),
              ),
              Positioned(
                bottom: -10,
                right: 10,
                child: Image.asset(
                  "assets/pets-image/pawpair.png",
                  color: pawColor1,
                  width: 50,
                  height: 50,
                ),
              ),
            ],
          ),
        ),
        elevation: 0,
        title: Center(
          child: Text(
            'PawPair',
            style: GoogleFonts.raleway(
              textStyle: TextStyle(
                color: Colors.white,
                fontSize: 35,
                fontWeight: FontWeight.bold,
                shadows: [
                  Shadow(
                    offset: Offset(1.0, 1.0),
                    blurRadius: 2.0,
                    color: Colors.black.withOpacity(0.5),
                  ),
                ],
                letterSpacing: 10,
                decorationColor: Colors.black,
              ),
            ),
          ),
        ),
        leading: Padding(
          padding: const EdgeInsets.only(left: 10.0),
          child: Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.white,
            ),
            child: SizedBox(
              width: 50,
              height: 50,
              child: IconButton(
                icon: Icon(Icons.person, color: ColorConstants.primaryColor),
                onPressed: () {
                  Get.toNamed(Routes.PROFILESCREEN);
                },
              ),
            ),
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 10.0),
            child: Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.white,
              ),
              child: SizedBox(
                width: 50,
                height: 50,
                child: IconButton(
                  icon: Icon(Icons.message, color: ColorConstants.primaryColor),
                  onPressed: () {
                    Get.toNamed(Routes.CHATROOM);
                  },
                ),
              ),
            ),
          ),
        ],
      ),
      body: Builder(
        builder: (context) {
          final userPets = userController.getUser['pets'] ?? [];
          if (userPets.isEmpty) {
            print('User has no pets.');
            return Center(child: Text('You have no pets.'));
          } else {
            final petID = userPets[0]['petID'];
            // print('User has the following pets: $userPets');
          }
          return FutureBuilder(
            future: controller.loadNearbyPets(userController.getUser['userID']),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(child: CircularProgressIndicator());
              } else if (snapshot.hasError) {
                return Center(child: Text('Error: ${snapshot.error}'));
              } else if (controller.pets.isEmpty) {
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.pets,
                        size: 50,
                        color: Colors.grey,
                      ),
                      SizedBox(height: 8),
                      Text(
                        'No pets found near you.',
                        style: GoogleFonts.raleway(
                          fontSize: 20,
                          color: Colors.black,
                        ),
                      ),
                    ],
                  ),
                );
              }
              return Stack(
                children: [
                  Obx(() {
                    return AppinioSwiper(
                      padding: const EdgeInsets.only(bottom: 130, top: 16),
                      cards: controller.pets.map((pet) {
                        return ProfileCard(pet: pet);
                      }).toList(),
                      controller: swiperController,
                      onSwipe: (int index, AppinioSwiperDirection direction) {
                        if (controller.pets.isNotEmpty) {
                          if (index >= 0 && index < controller.pets.length) {
                            final likedPetId = controller.pets[index].petID;
                            final userPetId = userController
                                    .currentUser['pets'].isNotEmpty
                                ? userController.currentUser['pets'][0]['petID']
                                : null;

                            if (direction == AppinioSwiperDirection.left) {
                              controller
                                  .handleSwipeLeft(controller.pets[index]);
                              print(
                                  'เลื่อนไปทางซ้ายที่สัตว์เลี้ยง index $index');

                              if (userPetId != null) {
                                matchController.LikedPet(
                                    context, userPetId, likedPetId, false);
                              } else {
                                print("ไม่มีสัตว์เลี้ยงของผู้ใช้");
                              }
                            } else if (direction ==
                                AppinioSwiperDirection.right) {
                              controller
                                  .handleSwipeRight(controller.pets[index]);
                              print(
                                  'เลื่อนไปทางขวาที่สัตว์เลี้ยง index $index');

                              if (userPetId != null) {
                                matchController.LikedPet(
                                    context, userPetId, likedPetId, true);
                              } else {
                                print("ไม่มีสัตว์เลี้ยงของผู้ใช้");
                              }

                              _animateHeart();
                            }

                            if (index == 0 && !controller.isFetching.value) {
                              controller.refreshData();
                            }
                          } else {
                            print("Index $index อยู่นอกขอบเขต");
                          }
                        } else {
                          print("ไม่มีสัตว์เลี้ยงในลิสต์");
                        }
                      },
                    );
                  }),
                  Positioned(
                    bottom: 50,
                    right: 0,
                    left: 0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        ActionButton(
                          icon: Icons.heart_broken_outlined,
                          color: ColorConstants.favorite,
                          onTap: () {
                            _animateCross();
                            swiperController.swipeLeft();
                            print("ปุ่มถูกกด");
                          },
                        ),
                        InkWell(
                          onTap: () {
                            showModalBottomSheet(
                              context: context,
                              isScrollControlled: true,
                              builder: (context) {
                                return DraggableScrollableSheet(
                                  expand: false,
                                  initialChildSize: 0.5,
                                  minChildSize: 0.3,
                                  maxChildSize: 0.9,
                                  builder: (BuildContext context,
                                      ScrollController scrollController) {
                                    return Container(
                                      padding: const EdgeInsets.all(16.0),
                                      decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                          colors: [
                                            const Color.fromRGBO(
                                                247, 236, 217, 1)!,
                                            const Color.fromARGB(
                                                255, 247, 234, 210)!
                                          ], // ไล่สีชมพูอ่อน
                                          begin: Alignment.topCenter,
                                          end: Alignment.bottomCenter,
                                        ),
                                        borderRadius: BorderRadius.vertical(
                                            top: Radius.circular(20)),
                                      ),
                                      child: Obx(() {
                                        return ListView(
                                          controller: scrollController,
                                          children: [
                                            // หาคู่หรือเพื่อน
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Text(
                                                  'Find friends or partners',
                                                  style:
                                                      TextStyle(fontSize: 18),
                                                ),
                                                Icon(Icons.group,
                                                    size: 32,
                                                    color: Colors.orange),
                                              ],
                                            ),
                                            SizedBox(height: 16),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                ChoiceChip(
                                                  label: Text('friends'),
                                                  selected: filtercontroller
                                                          .lookingFor.value ==
                                                      'Find Friends',
                                                  onSelected: (selected) {
                                                    if (selected)
                                                      filtercontroller
                                                          .selectLookingFor(
                                                              'Find Friends');
                                                  },
                                                ),
                                                SizedBox(width: 8),
                                                ChoiceChip(
                                                  label: Text('partners'),
                                                  selected: filtercontroller
                                                          .lookingFor.value ==
                                                      'Find a Breeding Partner',
                                                  onSelected: (selected) {
                                                    if (selected)
                                                      filtercontroller
                                                          .selectLookingFor(
                                                              'Find a Breeding Partner');
                                                  },
                                                ),
                                                SizedBox(width: 8),
                                                ChoiceChip(
                                                  label: Text(
                                                      'friends & partners'),
                                                  selected: filtercontroller
                                                          .lookingFor.value ==
                                                      'Both',
                                                  onSelected: (selected) {
                                                    if (selected)
                                                      filtercontroller
                                                          .selectLookingFor(
                                                              'Both');
                                                  },
                                                ),
                                              ],
                                            ),
                                            SizedBox(height: 20),
                                            Text(
                                              'YOUR INTERESTS',
                                              style: TextStyle(fontSize: 18),
                                            ),
                                            SizedBox(height: 16),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Icon(Icons.pets,
                                                    color: Colors.orange,
                                                    size: 32),
                                                Expanded(
                                                    child: buildLevelSelection(
                                                        'BREED', 'breed')),
                                              ],
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Icon(Icons.color_lens,
                                                    color: Colors.orange,
                                                    size: 32),
                                                Expanded(
                                                    child: buildLevelSelection(
                                                        'COLOR', 'color')),
                                              ],
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Icon(
                                                    Icons
                                                        .calendar_today_rounded,
                                                    color: Colors.orange,
                                                    size: 32),
                                                Expanded(
                                                    child: buildLevelSelection(
                                                        'AGE', 'age')),
                                              ],
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Icon(Icons.fitness_center,
                                                    color: Colors.orange,
                                                    size: 32),
                                                Expanded(
                                                    child: buildLevelSelection(
                                                        'WEIGHT', 'weight')),
                                              ],
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Icon(
                                                    Icons
                                                        .switch_access_shortcut,
                                                    color: Colors.orange,
                                                    size: 32),
                                                Expanded(
                                                    child: buildLevelSelection(
                                                        'FURLENGTH',
                                                        'furLength')),
                                              ],
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Icon(
                                                    Icons
                                                        .emoji_emotions_outlined,
                                                    color: Colors.orange,
                                                    size: 32),
                                                Expanded(
                                                    child: buildLevelSelection(
                                                        'BEHAVIOR',
                                                        'behavior')),
                                              ],
                                            ),
                                            SizedBox(height: 20.0),
                                            SizedBox(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.7,
                                              height: 50,
                                              child: Container(
                                                padding: EdgeInsets.only(
                                                    top: 3, left: 3),
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(50),
                                                  border: Border.all(
                                                      color: Colors.black),
                                                ),
                                                child: SizedBox(
                                                  width: double.infinity,
                                                  height: 60,
                                                  child: ElevatedButton(
                                                    onPressed: () {
                                                      submitData();
                                                    },
                                                    style: ElevatedButton
                                                        .styleFrom(
                                                      backgroundColor:
                                                          Colors.amber,
                                                      elevation: 0,
                                                      shape:
                                                          RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(50),
                                                      ),
                                                    ),
                                                    child: Text(
                                                      'Submit',
                                                      style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        color: Colors.black,
                                                        fontSize: 18,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        );
                                      }),
                                    );
                                  },
                                );
                              },
                            );
                          },
                          child: Container(
                            width: 56,
                            height: 56,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              shape: BoxShape.circle,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.black.withOpacity(0.1),
                                  blurRadius: 8,
                                  offset: const Offset(0, 4),
                                ),
                              ],
                            ),
                            child: Icon(Icons.pets_sharp,
                                color: const Color.fromARGB(255, 255, 179, 2),
                                size: 32),
                          ),
                        ),
                        ActionButton(
                          icon: Icons.favorite,
                          color: ColorConstants.favorite,
                          onTap: () {
                            _animateHeart();
                            print("ปุ่มถูกกด");
                            swiperController.swipeRight();
                          },
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    top: 100,
                    left: MediaQuery.of(context).size.width / 2 - 50,
                    child: ScaleTransition(
                      scale: Tween(begin: 0.0, end: 1.0).animate(
                        CurvedAnimation(
                          parent: _likeController,
                          curve: Curves.elasticOut,
                        ),
                      ),
                      child: Icon(Icons.favorite, size: 100, color: Colors.red),
                    ),
                  ),
                  Positioned(
                    top: 100,
                    left: MediaQuery.of(context).size.width / 2 - 50,
                    child: ScaleTransition(
                      scale: Tween(begin: 0.0, end: 1.0).animate(
                        CurvedAnimation(
                          parent: _dislikeController,
                          curve: Curves.elasticOut,
                        ),
                      ),
                      child: Icon(Icons.cancel, size: 100, color: Colors.red),
                    ),
                  ),
                ],
              );
            },
          );
        },
      ),
    );
  }

  void showFilterDialog(BuildContext context) {
    final TextEditingController breedController = TextEditingController();
    final TextEditingController colorController = TextEditingController();
    final TextEditingController ageController = TextEditingController();
    final TextEditingController weightController = TextEditingController();
    final TextEditingController furLengthController = TextEditingController();
    final TextEditingController behaviorController = TextEditingController();

    void _validateWeights() {
      double total = [
        breedController,
        colorController,
        ageController,
        weightController,
        furLengthController,
        behaviorController,
      ].fold(0.0,
          (sum, controller) => sum + (double.tryParse(controller.text) ?? 0));

      if (total > 10) {
        print('Total weight exceeds 10');
      }
    }

    Widget _buildWeightField(String label, TextEditingController controller) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(label),
          SizedBox(
            width: 50,
            child: TextField(
              controller: controller,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(hintText: '0'),
              onChanged: (value) => _validateWeights(),
            ),
          ),
        ],
      );
    }
  }

  void applyFilters(String breed, String color, String age, String weight,
      String furLength, String behavior) {
    var weights = {
      'breed': breed.isNotEmpty ? double.parse(breed) : 0.0,
      'color': color.isNotEmpty ? double.parse(color) : 0.0,
      'age': age.isNotEmpty ? double.parse(age) : 0.0,
      'weight': weight.isNotEmpty ? double.parse(weight) : 0.0,
      'furLength': furLength.isNotEmpty ? double.parse(furLength) : 0.0,
      'behavior': behavior.isNotEmpty ? double.parse(behavior) : 0.0,
    };

    controller.fetchNearbyPets(userController.getUser['userID'],
        weights: weights);
  }

  Widget buildLevelSelection(String label, String type) {
    return Column(
      children: [
        Text(label, style: TextStyle(fontSize: 16)),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: List.generate(5, (i) {
            return GestureDetector(
              onTap: () {
                //
                filtercontroller.updateSelectedLevel(type, i + 1);
              },
              child: Container(
                margin: EdgeInsets.all(4),
                child: Icon(
                  Icons.pets,
                  color: (getScoreByType(type) >= (i + 1))
                      ? Colors.orange[700]
                      : const Color.fromARGB(255, 190, 190, 190),
                  size: 32,
                ),
              ),
            );
          }),
        ),
      ],
    );
  }

  void submitData() {
    List<String> scoresWithTotal = filtercontroller.getScoresWithTotal();
    String breed = '';
    String color = '';
    String age = '';
    String weight = '';
    String furLength = '';
    String behavior = '';

    for (var score in scoresWithTotal) {
      var parts = score.split(': ');
      if (parts.length == 2) {
        var key = parts[0];
        var value = parts[1];

        switch (key) {
          case 'breed':
            breed = value;
            break;
          case 'color':
            color = value;
            break;
          case 'age':
            age = value;
            break;
          case 'weight':
            weight = value;
            break;
          case 'furLength':
            furLength = value;
            break;
          case 'behavior':
            behavior = value;
            break;
        }
      }
    }

    applyFilters(breed, color, age, weight, furLength, behavior);

    Get.snackbar('สำเร็จ', 'ส่งข้อมูลเรียบร้อยแล้ว!');
  }

  int getScoreByType(String type) {
    switch (type) {
      case 'breed':
        return filtercontroller.breedScore.value;
      case 'color':
        return filtercontroller.colorScore.value;
      case 'age':
        return filtercontroller.ageScore.value;
      case 'weight':
        return filtercontroller.weightScore.value;
      case 'furLength':
        return filtercontroller.furLengthScore.value;
      case 'behavior':
        return filtercontroller.behaviorScore.value;
      default:
        return 0;
    }
  }
}
