import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tinder_pet_front/app/data/service/type/pet.dart';
import 'package:tinder_pet_front/app/modules/home/views/match/colors.dart';
import 'package:tinder_pet_front/app/modules/home/views/match/pet_info_page.dart';

class ProfileCard extends StatelessWidget {
  const ProfileCard({Key? key, required this.pet}) : super(key: key);

  final Pet? pet;

  @override
  Widget build(BuildContext context) {
    return pet == null ? _buildNoDataCard(context) : _buildPetCard(context);
  }

  Widget _buildNoDataCard(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        color: Colors.grey[300],
      ),
      child: Center(
        child: Text(
          'ไม่มีข้อมูลสัตว์เลี้ยง',
          style: TextStyle(
            fontSize: 24,
            color: Colors.grey[700],
          ),
        ),
      ),
    );
  }

  Widget _buildPetCard(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (pet != null) {
          Get.to(() => PetInfoPage(pet: pet!), transition: Transition.fade);
        }
      },
      child: Container(
        padding: const EdgeInsets.all(16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
        ),
        child: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.2),
                    spreadRadius: 2,
                    blurRadius: 10,
                    offset: Offset(0, 5),
                  ),
                ],
                image: DecorationImage(
                  image: NetworkImage(
                    pet?.images?.isNotEmpty == true
                        ? 'http://localhost:3000/image/image/${pet!.images[0].image}'
                        : 'http://localhost:3000/image/image/default.jpg',
                  ),
                  fit: BoxFit.cover,
                ),
              ),
            ),
Positioned(
  bottom: 20,
  right: 0,
  child: Container(
    height: 100,
    width: MediaQuery.of(context).size.width / 1.3,
    padding: const EdgeInsets.all(8),
    decoration: BoxDecoration(
      color: Colors.white.withOpacity(0.95),
      borderRadius: const BorderRadius.only(
        topLeft: Radius.circular(20),
        bottomLeft: Radius.circular(20),
      ),
      boxShadow: [
        BoxShadow(
          color: Colors.black.withOpacity(0.2),
          blurRadius: 12,
          offset: Offset(0, 6),
        ),
      ],
    ),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
  mainAxisAlignment: MainAxisAlignment.spaceBetween,
  children: [
    Row(
      children: [
        Icon(
          Icons.pets, 
          color: ColorConstants.secondary,
          size: 24, 
        ),
        const SizedBox(width: 6), 
        Text(
          '${pet!.petName}',
          style: TextStyle(
            color: ColorConstants.secondary,
            fontSize: 24, 
            fontWeight: FontWeight.bold,
          ),
        ),
      ],
    ),
    Row(
      children: [
   
        const SizedBox(width: 6),
        Text(
          '${pet!.age} month',
          style: TextStyle(
            color: ColorConstants.secondary,
            fontSize: 15, 
            fontWeight: FontWeight.bold,
          ),
        ),
      ],
    ),
  ],
)
,
            Row(
              children: [
                Icon(
                  pet!.sex == 'Male' ? Icons.male : Icons.female,
                  color: ColorConstants.secondary.withOpacity(0.7),
                ),
                const SizedBox(width: 6),
                Text(
                  pet!.sex,
                  style: TextStyle(
                    color: ColorConstants.secondary.withOpacity(0.7),
                    fontSize: 20, 
                    fontStyle: FontStyle.italic,
                  ),
                ),
              ],
            ),
          ],
        ),
        const SizedBox(height: 6), 
   Container(
  padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 4), 
  decoration: BoxDecoration(
    gradient: LinearGradient(
      colors: [
        pet!.compatibility != null
          ? (pet!.compatibility! < 40
              ? Colors.grey 
              : pet!.compatibility! <= 70
                ? Colors.yellow
                : Colors.pink) 
          : Colors.grey, 
        pet!.compatibility != null
          ? (pet!.compatibility! < 40
              ? Colors.grey.withOpacity(0.8)
              : pet!.compatibility! <= 70
                ? Colors.yellow.withOpacity(0.8)
                : Colors.pink.withOpacity(0.8))
          : Colors.grey.withOpacity(0.8),
      ],
      begin: Alignment.topLeft,
      end: Alignment.bottomRight,
    ),
    borderRadius: BorderRadius.circular(12),
  ),
  child: Text(
    '${pet!.compatibility?.toStringAsFixed(2) ?? '0.00'}%',
    style: TextStyle(
      color: Colors.white,
      fontSize: 20, 
      fontWeight: FontWeight.bold,
    ),
  ),
),

        const SizedBox(height: 6),
      ],
    ),
  ),
),



          ],
        ),
      ),
    );
  }
}
