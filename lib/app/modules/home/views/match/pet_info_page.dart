import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tinder_pet_front/app/data/service/type/pet.dart' hide Image;
import 'package:tinder_pet_front/app/modules/home/views/match/colors.dart';
import 'package:tinder_pet_front/app/modules/home/views/prelogin/const.dart';
import 'package:tinder_pet_front/app/routes/app_pages.dart';
import 'package:google_fonts/google_fonts.dart';
class PetInfoPage extends StatelessWidget {
  const PetInfoPage({Key? key, required this.pet}) : super(key: key);

  final Pet pet;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 80.0,
        flexibleSpace: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                pawColor1,
                Color(0xFFF7F0D2),
              ],
            ),
          ),
          child: Stack(
            children: [
              Positioned(
                top: 0,
                left: 0,
                child: Image.asset(
                  "assets/pets-image/pawpair.png",
                  color: pawColor1,
                  width: 100,
                  height: 100,
                ),
              ),
              Positioned(
                bottom: -10,
                right: 10,
                child: Image.asset(
                  "assets/pets-image/pawpair.png",
                  color: pawColor1,
                  width: 50,
                  height: 50,
                ),
              ),
            ],
          ),
        ),
        elevation: 0,
        title: Center(
          child: Text(
pet.petName,            style: GoogleFonts.raleway(
              textStyle: TextStyle(
                color: Colors.white,
                fontSize: 35,
                fontWeight: FontWeight.bold,
                shadows: [
                  Shadow(
                    offset: Offset(1.0, 1.0),
                    blurRadius: 2.0,
                    color: Colors.black.withOpacity(0.5),
                  ),
                ],
                letterSpacing: 10,
                decorationColor: Colors.black,
              ),
            ),
          ),
        ),
        leading: Padding(
          padding: const EdgeInsets.only(left: 10.0),
          child: Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.white,
            ),
            child: SizedBox(
              width: 50,
              height: 50,
              child: IconButton(
                icon: Icon(Icons.arrow_back, color: ColorConstants.primaryColor),
                onPressed: () {
                  Get.toNamed(Routes.HOME);
                },
              ),
            ),
          ),
        ),

      ),

      body: SizedBox(
        height: size.height,
        child: SingleChildScrollView(
          child: Column(
            children: [
              _buildPetImage(),
              _buildPetDetails(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildPetImage() {
    return Container(
      height: 250,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: NetworkImage(
            'http://localhost:3000/image/image/${pet.images.isNotEmpty ? pet.images[0].image : ''}',
          ),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Widget _buildPetDetails(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(30), // ขอบโค้งด้านบน
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 2,
            blurRadius: 7,
            offset: Offset(0, 3), // การตั้งค่าเงา
          ),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text(
                  '${pet.petName}  ',
                  style: TextStyle(
                    fontSize: 28,
                    fontWeight: FontWeight.bold,
                    color: ColorConstants.secondary,
                  ),
                ),
                Icon(
                  pet.sex == 'Male' ? Icons.male : Icons.female,
                  size: 28,
                  color: ColorConstants.secondary,
                ),
              ],
            ),
            const SizedBox(height: 8),
              Row(
              children: [
                Icon(
                  Icons.pets,
                  color: const Color.fromARGB(255, 83, 53, 19),size: 16
                ),
                Text(
                  '${pet.breed}',
                  style: TextStyle(color: black.withOpacity(0.6), fontSize: 16),
                ),
              ],
            ),
            Row(
              children: [
                Icon(
                  Icons.location_on_outlined,
                  color: blue,size: 16
                ),
                Text(
                  '${pet.user.address}, ( ${pet.distance} )',
                  style: TextStyle(color: black.withOpacity(0.6), fontSize: 16),
                ),
              ],
            ),
            const SizedBox(height: 8),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: _moreInfo(
                    color1,
                    pet.sex,
                    "Sex",
                  ),
                ),
                Expanded(
                  child: _moreInfo(
                    color2,
                    "${pet.age} Years",
                    "Age",
                  ),
                ),
                Expanded(
                  child: _moreInfo(
                    color3,
                    "${pet.weight.toString()} KG",
                    "Weight",
                  ),
                ),
              ],
            ),
            const SizedBox(height: 16),
            Row(
              children: [
                CircleAvatar(
                  radius: 30,
                  backgroundColor: color1,
                  backgroundImage: AssetImage("pets-image/catcat.jpg"),
                ),
                SizedBox(width: 10),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        pet.user.userName,
                        style: const TextStyle(
                          fontSize: 20,
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        "${pet.petName} Owner",
                        style: const TextStyle(
                          fontSize: 16,
                          color: Colors.black54,
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(12),
                  decoration: BoxDecoration(
                    color: Colors.red.withOpacity(0.1),
                    borderRadius: BorderRadius.circular(12),
                    border: Border.all(color: Colors.red, width: 1),
                  ),
                  child: Row(
                    children: [
                      const Icon(
                        Icons.search,
                        color: Colors.red,
                        size: 18,
                      ),
                      const SizedBox(width: 10),
                      Text(
                        "${pet.objective}",
                        style: const TextStyle(
                          color: Colors.red,
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(height: 20.0),
            Text(
              'ABOUT MY PET',
              style: TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.bold,
                color: ColorConstants.secondary,
              ),
            ),
            const SizedBox(height: 4),
            Text(
              pet.petDes,
              style: TextStyle(
                fontSize: 16,
                color: Colors.grey[600],
              ),
            ),
            const SizedBox(height: 16),
            Text(
              'BEHAVIORS',
              style: TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.bold,
                color: ColorConstants.secondary,
              ),
            ),
            const SizedBox(height: 4),
            Text(
              pet.specialBehaviors,
              style: TextStyle(
                fontSize: 16,
                color: Colors.grey[600],
              ),
            ),
            const SizedBox(height: 4),
Column(
  crossAxisAlignment: CrossAxisAlignment.start,
  children: [
    Text(
      'FUNDAMENTAL DETAILS',
      style: TextStyle(
        fontSize: 22,
        fontWeight: FontWeight.bold,
        color: ColorConstants.secondary,
      ),
    ),
    const SizedBox(height: 4),
    _buildDetailRow('Color: ${pet.color}'), 
    _buildDetailRow('Fur Length: ${pet.furLength}'), 
    _buildDetailRow('Parenting Status: ${pet.parentingStatus}'), 
    _buildDetailRow('Sterile State: ${pet.sterileState}'),
  ],
),

            const SizedBox(height: 16),
            Text(
              'HEALTH',
              style: TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.bold,
                color: ColorConstants.secondary,
              ),
            ),
             const SizedBox(height: 4),
            Text(
              pet.healthHistory,
              style: TextStyle(
                fontSize: 16,
                color: Colors.grey[600],
              ),
            ),
            Column(
  children: [

    _buildVaccinesAndFoods(),
 
  ],
),

          ],
        ),
      ),
    );
  }

  Widget _moreInfo(Color color, String value, String label) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: Stack(
        children: [
          Positioned(
            bottom: -20,
            right: 0,
            child: Transform.rotate(
              angle: 12,
            ),
          ),
          Container(
            height: 100,
            width: 150,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: color,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  value,
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  label,
                  style: const TextStyle(
                    fontSize: 16,
                    color: Colors.black54,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
Widget _buildVaccinesAndFoods() {
  List<String> vaccines = pet.vaccine.split(',');
  List<String> foods = pet.foodsEaten.split(',');

  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text(
        'VACCINES',
        style: TextStyle(
          fontSize: 22,
          fontWeight: FontWeight.bold,
          color: ColorConstants.secondary,
        ),
      ),
      const SizedBox(height: 4),
      for (String vaccine in vaccines)
        Row(
          children: [
            Icon(Icons.vaccines, color: Colors.green, size: 16), 
            const SizedBox(width: 8),
            Text(
              vaccine.trim(),
              style: TextStyle(
                fontSize: 16,
                color: Colors.grey[600],
              ),
            ),
          ],
        ),
      const SizedBox(height: 16),
      Text(
        'FOODS EATEN',
        style: TextStyle(
          fontSize: 22,
          fontWeight: FontWeight.bold,
          color: ColorConstants.secondary,
        ),
      ),
      const SizedBox(height: 4),
      for (String food in foods)
        Row(
          children: [
            Icon(Icons.food_bank, color: Colors.orange, size: 16), 
            const SizedBox(width: 8),
            Text(
              food.trim(),
              style: TextStyle(
                fontSize: 16,
                color: Colors.grey[600],
              ),
            ),
          ],
        ),
    ],
  );
}

  Widget _buildDetailRow(String detail) {
    return Row(
      children: [
           Icon(
        Icons.pets_sharp, color: const Color.fromARGB(14, 0, 0, 0), size: 16,
      ),
        const SizedBox(width: 8),
        Text(
          detail,
          style: TextStyle(
            fontSize: 16,
            color: const Color.fromARGB(255, 56, 21, 16),
          ),
        ),
      ],
    );
  }
}
