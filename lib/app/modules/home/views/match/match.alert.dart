import 'dart:math';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'dart:async';

import 'package:get/get_core/src/get_main.dart';
import 'package:tinder_pet_front/app/routes/app_pages.dart';

class PetPage extends StatefulWidget {
  final dynamic response2data;

  PetPage({required this.response2data});

  @override
  _PetPageState createState() => _PetPageState();
}

class _PetPageState extends State<PetPage> with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Timer _timer;
  bool _isHeartVisible = false;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: Duration(milliseconds: 500),
      vsync: this,
    );

    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {
        _isHeartVisible = !_isHeartVisible;
      });
    });
  }

  @override
  void dispose() {
    _timer.cancel();
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final pet = widget.response2data;

    return Scaffold(
      backgroundColor: Colors.amber,
      body: Padding(
        padding: const EdgeInsets.only(top: 80),
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40), topRight: Radius.circular(40)),
              color: Color(0xFFFFF0F5)),
          child: Stack(
            children: [
              Positioned(
                bottom: 0,
                right: 0,
                left: 0,
                child: Image.asset(
                  'pets-image/background4.png',
                  height: 450,
                  width: double.infinity,
                  fit: BoxFit.fill,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        text:
                            "Congratulations,\n", // ข้อความแรกที่ไม่มีการเปลี่ยนสี
                        style: TextStyle(
                          fontSize: 40,
                          fontWeight: FontWeight.bold,
                          color: const Color.fromARGB(
                              255, 255, 196, 100), // สีของข้อความทั่วไป
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                "successfully", // เฉพาะข้อความนี้ที่ต้องการให้เป็นสีน้ำเงิน
                            style: TextStyle(
                              fontSize: 40,
                              color: Colors.orange[700], // สีฟ้า
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          TextSpan(
                            text:
                                " matched!", // เฉพาะข้อความนี้ที่ต้องการให้เป็นสีน้ำเงิน
                            style: TextStyle(
                              fontSize: 40,
                              color: const Color.fromARGB(255, 255, 196, 100),
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'I’m ready to chat with you now!',
                          style: TextStyle(
                            color: Colors.orange,
                            fontSize: 15.5,
                            fontWeight: FontWeight.w900,
                            height: 1.2,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 20),
                    Center(
                      child: Stack(
                        children: [
                          Container(
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(
                                // เพิ่มกรอบสีขาว
                                color: Colors.white, // สีกรอบ
                                width: 10.0, // ความหนาของกรอบ
                              ),
                              boxShadow: [
                                BoxShadow(
                                  blurRadius: 10,
                                  color: Colors.grey,
                                  spreadRadius: 5,
                                ),
                              ],
                            ),
                            child: CircleAvatar(
                              backgroundImage: NetworkImage(
                                  "http://localhost:3000/image/image/${pet['images'][0]['image']}"),
                              radius: 180, // เพิ่มขนาดของภาพให้ใหญ่ขึ้น
                              child: ClipOval(
                                child: Image.network(
                                  "http://localhost:3000/image/image/${pet['images'][0]['image']}",
                                  fit: BoxFit.cover,
                                  width: 360, // ปรับความกว้างของภาพ
                                  height: 360, // ปรับความสูงของภาพ
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 20),
                    Text(
                      pet['petName'],
                      style: TextStyle(
                        fontSize: 50,
                        fontWeight: FontWeight.bold,
                        color: Colors.orange[700],
                        shadows: [
                          Shadow(
                            color: Colors.black38
                                .withOpacity(0.8), // เพิ่มความเข้มของเงา
                            offset: Offset(2, 2), // ปรับระยะห่างเงา
                            blurRadius: 5, // เพิ่มขนาดของเบลอ
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 80.0),
                    SizedBox(
                      width: MediaQuery.of(context).size.width * 0.7,
                      height: 50,
                      child: Container(
                        padding: EdgeInsets.only(top: 3, left: 3),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          border: Border.all(color: Colors.black),
                        ),
                        child: SizedBox(
                          width: double.infinity,
                          height: 60,
                          child: ElevatedButton(
                            onPressed: () => Get.toNamed(Routes.HOME),
                            style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.amber,
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(50),
                              ),
                            ),
                            child: Text(
                              'Close',
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                                color: Colors.black,
                                fontSize: 18,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
