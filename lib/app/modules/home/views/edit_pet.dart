import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/petprofile_controller.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/user_controller.dart';
import 'package:tinder_pet_front/app/modules/home/views/colors_data.dart';
import 'package:tinder_pet_front/app/modules/home/views/prelogin/const.dart';
import 'package:tinder_pet_front/app/modules/home/views/screens/breed_data.dart';
import 'package:tinder_pet_front/app/modules/home/views/vaccines_data.dart';
import 'package:tinder_pet_front/app/routes/app_pages.dart';

class EditPet extends StatefulWidget {
  final Map<String, dynamic> pets;
  const EditPet({Key? key, required this.pets}) : super(key: key);
  @override
  _EditPetState createState() => _EditPetState();
}

class _EditPetState extends State<EditPet> {
  final PetprofileController petprofileController =
      Get.put(PetprofileController());

  // รายการตัวเลือกอาหาร
  final List<String> foodOptions = [
    'Dry Food', // อาหารเม็ด
    'Wet Food', // อาหารเปียก
    'Homemade Food', // อาหารปรุงเอง
    'Prescription Diet', // อาหารเฉพาะโรค
    'Other', // อื่นๆ
  ];

  final UserController userController = Get.put(UserController());
  Uint8List? _imageBytes;
  String? _fileName;
  String? _animalType;

  @override
  void initState() {
    super.initState();
    final args = Get.arguments as Map<String, dynamic>?;
    if (args != null) {
      _imageBytes = args['imageBytes'] as Uint8List?;
      _fileName = args['fileName'] as String?;
      _animalType = args['animalType'] as String?;

      petprofileController.setImage(_imageBytes!, _fileName!);

      // เซ็ตข้อมูลสัตว์เลี้ยงให้กับ controller
      petprofileController.setPetData(widget.pets);
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<bool?> _showConfirmationDialog() async {
    return showDialog<bool>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('ยืนยัน'),
          content:
              Text('คุณแน่ใจหรือว่าต้องการอัปเดตรายละเอียดสัตว์เลี้ยงของคุณ?'),
          actions: <Widget>[
            TextButton(
              child: Text('ยกเลิก'),
              onPressed: () {
                Get.back(result: false);
              },
            ),
            TextButton(
              child: Text('ยืนยัน'),
              onPressed: () async {
                // เรียกฟังก์ชัน updatePetProfile แทน updatePetData
                petprofileController.updatePetProfile(widget.pets['petID']);

                // แสดงข้อความสำเร็จหลังจากอัปเดตเสร็จ
                Get.back(result: true); // ปิด dialog พร้อมคืนค่าผลลัพธ์ true
                Get.snackbar(
                  'สำเร็จ',
                  'อัปเดตรายละเอียดสัตว์เลี้ยงเรียบร้อยแล้ว',
                  snackPosition: SnackPosition.BOTTOM,
                  backgroundColor: Colors.green,
                  colorText: Colors.white,
                  margin: EdgeInsets.all(20),
                );
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    String imageUrl;

    if (widget.pets['images'] != null && widget.pets['images'].isNotEmpty) {
      imageUrl =
          'http://localhost:3000/image/image/${widget.pets["images"][0]["image"]}';
    } else if (widget.pets['imageUrl'] != null) {
      imageUrl = '${widget.pets["imageUrl"]}';
    } else {
      imageUrl =
          'http://localhost:3000/image/image/cd3b2327-b70a-41fc-a0d2-407eab6e2196.jpg';
    }

    final List<String> vaccineOptions =
        _animalType == 'Dog' ? dogVaccines : catVaccines;

    petprofileController.nameController.text = widget.pets['petName'] ?? '';
    petprofileController.desController.text = widget.pets['petDes'] ?? '';
    petprofileController.selectedSex.value = widget.pets['sex'] ?? '';
    petprofileController.ageController.text =
        widget.pets['age']?.toString() ?? '';
    petprofileController.weightController.text =
        widget.pets['weight']?.toString() ?? '';
    petprofileController.selectedColor.value = widget.pets['color'] ?? '';
    petprofileController.selectedFur.value = widget.pets['furLength'] ?? '';
    petprofileController.selectedBreed.value = widget.pets['breed'] ?? '';

    // vacine
    var selectedVaccines = <String>[].obs;
    String vaccine = widget.pets['vaccine'] ?? '';
    if (vaccine == '-') {
      petprofileController.selectedVaccines.value = [];
    } else {
      petprofileController.selectedVaccines.value = [vaccine];
    }

    // food
    var selectedFoods = <String>[].obs;
    String food = widget.pets['foodsEaten'] ?? '';
    if (food == '-') {
      petprofileController.selectedFoods.value = [];
    } else {
      petprofileController.selectedFoods.value = [food];
    }

    petprofileController.parentingStatus.value =
        widget.pets['parentingStatus'] ?? '';
    petprofileController.sterileState.value = widget.pets['sterileState'] ?? '';

    petprofileController.healthHistoryController.text =
        widget.pets['healthHistory'] ?? '';
    petprofileController.specialBehaviorsController.text =
        widget.pets['specialBehaviors'] ?? '';

    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 80.0,
        flexibleSpace: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                pawColor1,
                Color(0xFFF7F0D2),
              ],
            ),
          ),
          child: Stack(
            children: [
              Positioned(
                top: 0,
                left: 0,
                child: Image.asset(
                  "assets/pets-image/pawpair.png",
                  color: pawColor1,
                  width: 100,
                  height: 100,
                ),
              ),
              Positioned(
                bottom: -10,
                right: 10,
                child: Image.asset(
                  "assets/pets-image/pawpair.png",
                  color: pawColor1,
                  width: 50,
                  height: 50,
                ),
              ),
            ],
          ),
        ),
        elevation: 0,
        title: Center(
          child: Text(
            'Edit Pet',
            style: GoogleFonts.raleway(
              textStyle: TextStyle(
                color: Colors.white,
                fontSize: 35,
                fontWeight: FontWeight.bold,
                shadows: [
                  Shadow(
                    offset: Offset(1.0, 1.0),
                    blurRadius: 2.0,
                    color: Colors.black.withOpacity(0.5),
                  ),
                ],
                letterSpacing: 10,
                decorationColor: Colors.black,
              ),
            ),
          ),
        ),
        leading: Padding(
          padding: const EdgeInsets.only(left: 10.0),
          child: Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.white,
            ),
            child: SizedBox(
              width: 50,
              height: 50,
              child: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ),
          ),
        ),
      ),
      body: Form(
        child: ListView(
          padding: EdgeInsets.all(16),
          children: [
            Center(
              child: Stack(
                children: [
                  SizedBox(
                    width: 120,
                    height: 120,
                    child: ClipRRect(
                        borderRadius: BorderRadius.circular(100),
                        child: Image.network(
                          imageUrl,
                          fit: BoxFit.cover,
                          loadingBuilder: (BuildContext context, Widget child,
                              ImageChunkEvent? loadingProgress) {
                            if (loadingProgress == null) return child;
                            return Center(
                              child: CircularProgressIndicator(
                                value: loadingProgress.expectedTotalBytes !=
                                        null
                                    ? loadingProgress.cumulativeBytesLoaded /
                                        (loadingProgress.expectedTotalBytes ??
                                            1)
                                    : null,
                              ),
                            );
                          },
                          errorBuilder: (BuildContext context, Object error,
                              StackTrace? stackTrace) {
                            return Text('Error loading image');
                          },
                        )),
                  ),
                  Positioned(
                    bottom: 0,
                    right: 0,
                    child: GestureDetector(
                      onTap: () {
                        print("Edit icon tapped!");
                      },
                      child: Container(
                        width: 35,
                        height: 35,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(100),
                          color: const Color.fromARGB(255, 255, 185, 33),
                        ),
                        child: const Icon(
                          Icons.edit,
                          color: Color.fromARGB(255, 0, 0, 0),
                          size: 20,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),

            SizedBox(height: 16),
            // petName
            Center(
              child: IntrinsicWidth(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: TextFormField(
                        controller: petprofileController.nameController,
                        textAlign: TextAlign.center,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                        ),
                        style: GoogleFonts.kanit(fontSize: 25.0),
                      ),
                    ),
                    Icon(Icons.edit),
                  ],
                ),
              ),
            ),
            SizedBox(height: 16),
            // Description
            Text('Description :'),
            SizedBox(height: 16),
            Center(
              child: TextFormField(
                maxLines: 3,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderSide: BorderSide(width: 10),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  contentPadding:
                      EdgeInsets.symmetric(vertical: 20.0, horizontal: 10.0),
                ),
                controller: petprofileController.desController,
              ),
            ),
            SizedBox(height: 16),
            // Sex
            Column(
              children: [
                Row(
                  children: [
                    Expanded(
                      child: DropdownButtonFormField<String>(
                        value: widget.pets['sex'] ??
                            (petprofileController.selectedSex.value.isNotEmpty
                                ? petprofileController.selectedSex.value
                                : null),
                        onChanged: (String? newValue) {
                          petprofileController.setSex(newValue!);
                        },
                        items: ['Male', 'Female']
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                        decoration: InputDecoration(
                          labelText: 'Sex',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 16),
                    // Age
                    Expanded(
                      child: TextFormField(
                        decoration: InputDecoration(
                          labelText: 'Age',
                          suffixText: 'months',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                        ),
                        controller: TextEditingController(
                            text: widget.pets['age'] != null
                                ? widget.pets['age'].toString()
                                : ''),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter Age';
                          }
                          return null;
                        },
                      ),
                    ),
                  ],
                ),

                SizedBox(height: 16),
                // Weight and Color row
                Row(
                  children: [
                    Expanded(
                      child: TextFormField(
                        decoration: InputDecoration(
                          labelText: 'Weight',
                          suffixText: 'kg',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                        ),
                        controller: TextEditingController(
                            text: widget.pets['weight'] != null
                                ? widget.pets['weight'].toString()
                                : ''),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter Weight';
                          }
                          return null;
                        },
                      ),
                    ),
                    SizedBox(width: 16),
                    // Color
                    Expanded(
                      child: DropdownButtonFormField<String>(
                        isExpanded: true,
                        value: widget.pets['color'] ??
                            (petprofileController.selectedColor.value.isNotEmpty
                                ? petprofileController.selectedColor.value
                                : null),
                        onChanged: (String? newValue) {
                          petprofileController.setColor(newValue!);
                        },
                        items: (_animalType == 'Dog'
                                ? dogColors // ลิสต์สีสำหรับสุนัข
                                : catColors) // ลิสต์สีสำหรับแมว
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                        decoration: InputDecoration(
                          labelText: 'Color',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please select a Color'; // ตรวจสอบหากไม่มีการเลือกสี
                          }
                          return null;
                        },
                      ),
                    ),
                  ],
                ),

                SizedBox(height: 16),
                // Fur and Breed row
                Row(
                  children: [
                    Expanded(
                      child: DropdownButtonFormField<String>(
                        value: widget.pets['furLength'] ??
                            (petprofileController.selectedFur.value.isNotEmpty
                                ? petprofileController.selectedFur.value
                                : null),
                        onChanged: (String? newValue) {
                          petprofileController.setFur(newValue!);
                        },
                        items: ['Short', 'Medium', 'Long']
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                        decoration: InputDecoration(
                          labelText: 'Fur',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 16),
                    Expanded(
                      child: DropdownButtonFormField<String>(
                        isExpanded: true,
                        value: widget.pets['breed'] != null
                            ? widget.pets['breed']
                            : (petprofileController
                                    .selectedBreed.value.isNotEmpty
                                ? petprofileController.selectedBreed.value
                                : null),
                        onChanged: (String? newValue) {
                          petprofileController.setBreed(newValue!);
                        },
                        items: (_animalType == 'Dog' ? breedDog : breedCat)
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                        decoration: InputDecoration(
                          labelText: 'Breed',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(height: 25),
            Row(
              children: <Widget>[
                Expanded(
                  child: Divider(
                    color: Colors.black, // สีของเส้น
                    thickness: 2, // ความหนาของเส้น
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Text(
                    "Information",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Expanded(
                  child: Divider(
                    color: Colors.black, // สีของเส้น
                    thickness: 2, // ความหนาของเส้น
                  ),
                ),
              ],
            ),
            SizedBox(height: 16),
            // Vaccine List Section
            Text(
              "Vaccine",
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            Column(
              children: vaccineOptions.map((vaccine) {
                return Obx(() => CheckboxListTile(
                      title: Text(vaccine),
                      value: petprofileController.selectedVaccines
                              .contains(vaccine) ||
                          (widget.pets['vaccine'] != null &&
                              widget.pets['vaccine'].contains(
                                  vaccine)), // ตรวจสอบว่ามีอยู่ใน pets หรือไม่
                      onChanged: (bool? value) {
                        if (value == true) {
                          petprofileController.selectedVaccines
                              .add(vaccine); // เพิ่มในรายการเมื่อถูกเลือก
                        } else {
                          petprofileController.selectedVaccines.remove(
                              vaccine); // ลบออกจากรายการเมื่อยกเลิกการเลือก
                        }
                      },
                      controlAffinity: ListTileControlAffinity.leading,
                    ));
              }).toList(),
            ),

            SizedBox(height: 16),
            // FoodsEaten
            Text(
              "Foods Eaten",
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            Column(
              children: foodOptions.map((food) {
                return Obx(() => CheckboxListTile(
                      title: Text(food),
                      value:
                          petprofileController.selectedFoods.contains(food) ||
                              (widget.pets['foodsEaten'] != null &&
                                  widget.pets['foodsEaten'].contains(
                                      food)), // ตรวจสอบว่ามีอยู่ใน pets หรือไม่
                      onChanged: (bool? value) {
                        if (value == true) {
                          petprofileController.selectedFoods
                              .add(food); // เพิ่มในรายการเมื่อถูกเลือก
                        } else {
                          petprofileController.selectedFoods.remove(
                              food); // ลบออกจากรายการเมื่อยกเลิกการเลือก
                        }
                      },
                      controlAffinity: ListTileControlAffinity.leading,
                    ));
              }).toList(),
            ),

            SizedBox(height: 16),
            // Parenting Status
            Text(
              "Parenting Status",
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 6),
            Column(
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Obx(() => RadioListTile<String>(
                            title: const Text('Closed System'),
                            value: 'Closed System',
                            groupValue: petprofileController
                                    .parentingStatus.value.isNotEmpty
                                ? petprofileController.parentingStatus.value
                                : (widget.pets['parentingStatus'] != null
                                    ? widget.pets['parentingStatus']
                                    : null), // ดึงค่าจาก pets ถ้ามี
                            onChanged: (String? value) {
                              petprofileController.setParentingStatus(value!);
                            },
                          )),
                    ),
                    Expanded(
                      child: Obx(() => RadioListTile<String>(
                            title: const Text('Open System'),
                            value: 'Open System',
                            groupValue: petprofileController
                                    .parentingStatus.value.isNotEmpty
                                ? petprofileController.parentingStatus.value
                                : (widget.pets['parentingStatus'] != null
                                    ? widget.pets['parentingStatus']
                                    : null), // ดึงค่าจาก pets ถ้ามี
                            onChanged: (String? value) {
                              petprofileController.setParentingStatus(value!);
                            },
                          )),
                    ),
                  ],
                ),
              ],
            ),

            SizedBox(height: 16),
            // Sterile State
            Text(
              "Sterile State",
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 6),
            Column(
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Obx(() => RadioListTile<String>(
                            title: const Text('Neutered'),
                            value: 'Neutered',
                            groupValue: petprofileController
                                    .sterileState.value.isNotEmpty
                                ? petprofileController.sterileState.value
                                : (widget.pets['sterileState'] != null
                                    ? widget.pets['sterileState']
                                    : null), // ดึงค่าจาก pets ถ้ามี
                            onChanged: (String? value) {
                              petprofileController.setSterileState(value!);
                            },
                          )),
                    ),
                    Expanded(
                      child: Obx(() => RadioListTile<String>(
                            title: const Text('Not Neutered'),
                            value: 'Not Neutered',
                            groupValue: petprofileController
                                    .sterileState.value.isNotEmpty
                                ? petprofileController.sterileState.value
                                : (widget.pets['sterileState'] != null
                                    ? widget.pets['sterileState']
                                    : null), // ดึงค่าจาก pets ถ้ามี
                            onChanged: (String? value) {
                              petprofileController.setSterileState(value!);
                            },
                          )),
                    ),
                  ],
                ),
              ],
            ),

            SizedBox(height: 16),
            // Health History
            Text(
              "Health History",
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 6),
            TextFormField(
              maxLines: 3,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20.0),
                ),
                hintText: 'Enter health history',
              ),
              controller: petprofileController.healthHistoryController,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter Health History';
                }
                return null;
              },
            ),
            SizedBox(height: 16),
            // Special Behaviors
            Text(
              "Special Behaviors",
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 6),
            Expanded(
              child: TextFormField(
                maxLines: 3,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                ),
                controller: petprofileController.specialBehaviorsController,
                // Remove initialValue
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter Special Behaviors';
                  }
                  return null;
                },
              ),
            ),

            SizedBox(height: 16),
            // Objective
            Text(
              "Objective",
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 6),
            Column(
              children: [
                Obx(() => RadioListTile<String>(
                      title: const Text('Find Friends'),
                      value: 'Find Friends',
                      groupValue: petprofileController
                              .selectedobjective.value.isNotEmpty
                          ? petprofileController.selectedobjective.value
                          : widget.pets[
                              'objective'], // ดึงค่าจาก pets หาก objective ยังไม่มีค่า
                      onChanged: (String? value) {
                        petprofileController.setObjective(value!);
                      },
                    )),
                Obx(() => RadioListTile<String>(
                      title: const Text('Find a Breeding Partner'),
                      value: 'Find a Breeding Partner',
                      groupValue: petprofileController
                              .selectedobjective.value.isNotEmpty
                          ? petprofileController.selectedobjective.value
                          : widget.pets[
                              'objective'], // ดึงค่าจาก pets หาก objective ยังไม่มีค่า
                      onChanged: (String? value) {
                        petprofileController.setObjective(value!);
                      },
                    )),
                Obx(() => RadioListTile<String>(
                      title: const Text('Both'),
                      value: 'Both',
                      groupValue: petprofileController
                              .selectedobjective.value.isNotEmpty
                          ? petprofileController.selectedobjective.value
                          : widget.pets[
                              'objective'], // ดึงค่าจาก pets หาก objective ยังไม่มีค่า
                      onChanged: (String? value) {
                        petprofileController.setObjective(value!);
                      },
                    )),
              ],
            ),

            SizedBox(height: 25),
            //Save Button
            Center(
              child: Container(
                width: MediaQuery.of(context).size.width * 0.7,
                height: 50,
                padding: EdgeInsets.only(top: 3, left: 3),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  border: Border.all(color: Colors.black),
                ),
                child: ElevatedButton(
                  onPressed: () {
                    _showConfirmationDialog(); // Show the confirmation dialog
                  },
                  style: ElevatedButton.styleFrom(
                    backgroundColor: orangeContainer,
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50),
                    ),
                  ),
                  child: Text(
                    'Save',
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: Colors.black,
                      fontSize: 18,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
