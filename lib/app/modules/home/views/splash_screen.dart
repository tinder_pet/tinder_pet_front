import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:tinder_pet_front/app/routes/app_pages.dart';


class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Future.delayed(Duration(seconds: 1), checkLoginStatus);

    return Scaffold(
      body: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  void checkLoginStatus() {
    final box = GetStorage();
    String? token = box.read('token');

    if (token != null) {
      Get.offNamed(Routes.HOME);
    } else {
      Get.offNamed(Routes.PRELOGIN);
    }
  }
}