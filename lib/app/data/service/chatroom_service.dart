import 'dart:convert';
import 'package:http/http.dart' as http;

class ChatroomService {
  final String baseUrl = 'http://localhost:3000';

  Future<List<dynamic>> fetchChatrooms(int userId) async {
    final url = Uri.parse('$baseUrl/chatroom/user/$userId/chatrooms');
    final headers = {'Content-Type': 'application/json'};

    try {
      final response = await http.get(url, headers: headers);

      if (response.statusCode == 200) {
        final responseData = jsonDecode(response.body);
        return responseData; 
      } else {
        print('Failed to fetch chatrooms: ${response.body}');
        return [];
      }
    } catch (e) {
      print('Error: $e');
      return [];
    }
  }
}
