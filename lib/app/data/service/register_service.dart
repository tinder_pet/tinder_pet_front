import 'package:http/http.dart' as http;
import 'dart:convert';

class RegisterService {
  final String baseUrl = 'http://localhost:3000';

  Future<void> registerUser({
    required String username,
    required String email,
    required String password,
  }) async {
    final url = Uri.parse('$baseUrl/user');

    try {
      final response = await http.post(
        url,
        headers: {
          'Content-Type': 'application/json',
        },
        body: jsonEncode({
          'userName': username,
          'email': email,
          'password': password,
          'sex': 'Not specified', 
          'phoneNumber': '000000000', 
          'statusMember': 'none', 
          'userDes': 'Hello ! My name '+ username, 
        }),
      );

      if (response.statusCode == 201) {
        print('User registered successfully');
      } else {
        print('Failed to register user: ${response.body}');
      }
    } catch (e) {
      print('Error: $e');
    }
  }
}
