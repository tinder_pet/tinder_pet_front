import 'dart:convert';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:get_storage/get_storage.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/user_controller.dart';

class SubService {
  final String baseUrl = 'http://localhost:3000';
  final UserController currentUser = Get.find<UserController>();

  Future<void> signInMember(String payment) async {
    final userID = currentUser.getUser["userID"];
    final url = Uri.parse('$baseUrl/member');
    final response = await http.post(
      url,
      headers: {'Content-Type': 'application/json'},
      body:
          jsonEncode({'userID': userID, 'payment': payment, 'totalPrice': 179}),
    );

    if (response.statusCode == 201) {
      print('Successfully signed in as a member');

      // Update the user's statusMember in GetStorage
      final box = GetStorage();

      // Debug: Print all data in GetStorage before the update
      print('Data in GetStorage before update:');
      final keys = box.getKeys();
      for (var key in keys) {
        print('$key: ${box.read(key)}');
      }

      // Ensure that userID exists before updating
      if (box.read('userID') != null) {
        // Update only the statusMember field
        await box.write('statusMember', 'active');
      } else {
        print('User not found in storage.');
      }

      // Debug: Print updated user data after the update
      print('Data in GetStorage after update:');
      for (var key in keys) {
        print('$key: ${box.read(key)}');
      }

      // Update the user in UserController
      currentUser.setUser({
        ...currentUser.getUser,
        'statusMember': 'active',
      });
    } else {
      print('Failed to sign in as a member');
    }
  }
}
