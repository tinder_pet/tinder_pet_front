import 'package:socket_io_client/socket_io_client.dart' as IO;

class NotificationService {
  late IO.Socket notificationSocket;

  void setupNotificationSocket() {
    notificationSocket = IO.io('ws://localhost:3000', <String, dynamic>{
      'transports': ['websocket'],
      'autoConnect': false,
    });

    notificationSocket.on('connect', (_) {
      print('Connected to notification server');
    });

    // ฟัง event 'newMessage' เมื่อมีการแจ้งเตือนข้อความใหม่
    notificationSocket.on('newMessage', (data) {
      print('New message notification: $data');
      // สามารถเพิ่มการแสดงแจ้งเตือน UI หรือ push notification ที่นี่
    });

    notificationSocket.on('disconnect', (_) {
      print('Disconnected from notification server');
    });

    notificationSocket.connect(); // เชื่อมต่อกับ notificationSocket
  }
}
