// userprofile_service.dart
import 'dart:convert';
import 'package:get_storage/get_storage.dart';
import 'package:http/http.dart' as http;
import 'package:tinder_pet_front/app/data/service/type/location_data.dart';

import 'type/pet.dart';

class UserProfileService {
  final String baseUrl = 'http://localhost:3000';

  Future<Map<String, dynamic>> getUser({
    required String username,
    required String userdes,
    required int userid,
  }) async {
    final url = Uri.parse('$baseUrl/user/$userid');
    try {
      final response = await http.get(
        url,
        headers: {'Content-Type': 'application/json'},
      );

      if (response.statusCode == 200) {
        return jsonDecode(response.body);
      } else {
        throw Exception('Failed to load user profile');
      }
    } catch (e) {
      print('Error: $e');
      rethrow;
    }
  }

  Future<void> updateUserName({
    required int userId,
    required String newUsername,
  }) async {
    final url = Uri.parse('$baseUrl/user/$userId');
    try {
      final response = await http.patch(
        url,
        headers: {'Content-Type': 'application/json'},
        body: jsonEncode({
          'userName': newUsername,
        }),
      );

      if (response.statusCode == 200) {
        // ถ้าอัปเดตสำเร็จ ให้เขียนข้อมูลใหม่ลงใน GetStorage
        final box = GetStorage();
        await box.write('userName', newUsername); // อัปเดตชื่อผู้ใช้ใหม่
        print('User name updated in GetStorage: $newUsername');
      } else {
        throw Exception('Failed to update username');
      }
    } catch (e) {
      print('Error updating username: $e');
      rethrow;
    }
  }

  Future<void> updateUserDescription({
    required int userId,
    required String newDescription,
  }) async {
    final url = Uri.parse('$baseUrl/user/$userId');
    try {
      final response = await http.patch(
        url,
        headers: {'Content-Type': 'application/json'},
        body: jsonEncode({
          if (newDescription != null) 'userDes': newDescription,
        }),
      );

      if (response.statusCode != 200) {
        throw Exception('Failed to update user description');
      }
    } catch (e) {
      print('Error updating user description: $e');
      rethrow;
    }
  }

  Future<void> updateUserLocation({
    required int userId,
    required double latitude,
    required double longitude,
  }) async {
    final url = Uri.parse('$baseUrl/user/$userId/location');
    try {
      final response = await http.patch(
        url,
        headers: {'Content-Type': 'application/json'},
        body: jsonEncode({
          'latitude': latitude,
          'longitude': longitude,
        }),
      );

      if (response.statusCode != 200) {
        throw Exception('Failed to update user location');
      } else {
        print('User location updated successfully.');
      }
    } catch (e) {
      print('Error updating user location: $e');
      rethrow;
    }
  }

// ฟังก์ชันอัปเดตข้อมูลผู้ใช้เพิ่มเติม (Sex, Phone Number, Religion, Nationality, Contact)
  Future<void> updateUserInfo({
    required int userId,
    String? sex,
    String? phoneNumber,
    String? religion,
    String? nationality,
    String? contact,
  }) async {
    final url = Uri.parse('$baseUrl/user/$userId');
    try {
      final response = await http.patch(
        url,
        headers: {'Content-Type': 'application/json'},
        body: jsonEncode({
          if (sex != null) 'sex': sex,
          if (phoneNumber != null) 'phoneNumber': phoneNumber,
          if (religion != null) 'religion': religion,
          if (nationality != null) 'nationality': nationality,
          if (contact != null) 'contact': contact,
        }),
      );

      if (response.statusCode != 200) {
        throw Exception('Failed to update user profile');
      }
    } catch (e) {
      print('Error updating user profile: $e');
      rethrow;
    }
  }

  // Province, District, SubDistrict
  Future<void> updateUserAddress({
    required int userId,
    required String address,
    String? province,
    String? district,
    String? subDistrict,
  }) async {
    final url = Uri.parse('$baseUrl/user/$userId');
    try {
      final response = await http.patch(
        url,
        headers: {'Content-Type': 'application/json'},
        body: jsonEncode({
          'address': address,
          if (province != null) 'province': province,
          if (district != null) 'district': district,
          if (subDistrict != null) 'subDistrict': subDistrict,
        }),
      );

      // print('Response status: ${response.statusCode}');
      // print('Response body: ${response.body}');

      if (response.statusCode != 200) {
        throw Exception('Failed to update user address: ${response.body}');
      }
    } catch (e) {
      print('Error updating user address: $e');
      rethrow;
    }
  }

  Future<void> updateUserImage({
    required int userId,
    required String imagePath,
  }) async {
    final url = Uri.parse('$baseUrl/user/$userId');
    try {
      final response = await http.patch(
        url,
        headers: {'Content-Type': 'application/json'},
        body: jsonEncode({
          'userImage': imagePath, // ใช้ imagePath แทน
        }),
      );

      if (response.statusCode != 200) {
        throw Exception('Failed to update user image');
      }
    } catch (e) {
      print('Error updating user image: $e');
      rethrow;
    }
  }

  Future<LocationData?> getUserLocation(int userId) async {
    try {
      // ทำการเรียก API หรือฐานข้อมูลเพื่อนำข้อมูลตำแหน่งมา
      final response = await http
          .get(Uri.parse('http://localhost:3000/users/$userId/location'));

      if (response.statusCode == 200) {
        // แปลงข้อมูล JSON เป็น LocationData
        final data = json.decode(response.body);
        return LocationData(
          latitude: data['latitude'],
          longitude: data['longitude'],
        );
      } else {
        // จัดการกรณีที่ไม่สามารถดึงข้อมูลได้
        return null;
      }
    } catch (e) {
      print('Error fetching location: $e');
      return null;
    }
  }
}
