import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:tinder_pet_front/app/modules/home/views/match/match.alert.dart';

class MatchService {
  final String baseUrl = 'http://localhost:3000';

  Future<void> interaction({
    required BuildContext context,
    required int pet1,
    required int pet2,
    required bool isLiked, // New parameter
  }) async {
    final url = Uri.parse('$baseUrl/interaction');
    final url2 = Uri.parse('$baseUrl/pet/$pet2');
    try {
      final response = await http.post(
        url,
        headers: {
          'Content-Type': 'application/json',
        },
        body: jsonEncode({'isLiked': isLiked, "pet1": pet1, "pet2": pet2}),
      );
      if (response.statusCode == 201) {
        final responseData = json.decode(response.body);
        if (responseData['message'] == "Match created successfully") {
          // if (response.statusCode == 201) {
          print("User Can Liked Pet Successfully");
          try {
            final response2 = await http.get(
              url2,
              headers: {
                'Content-Type': 'application/json',
              },
            );
            print("response2.statusCode : ${response2.statusCode}");
            if (response2.statusCode == 200) {
              final response2Data = json.decode(response2.body);
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => PetPage(response2data: response2Data),
              ));
            }
          } catch (e) {
            print("Error: $e");
          }
        } else {
          print("Unexpected response: ${responseData['message']}");
        }
      } else {
        print("Failed Liked Pet: ${response.body}");
      }
    } catch (e) {
      print('Error: $e');
    }
  }
}
