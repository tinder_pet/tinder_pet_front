import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:tinder_pet_front/app/data/service/type/pet.dart';

class PetService {
  final String baseUrl;

  PetService(this.baseUrl);

  Future<List<Pet>> fetchNearbyPets(int userId) async {
    final response = await http.get(Uri.parse('$baseUrl/pet/nearby/$userId'));

    if (response.statusCode == 200) {
      List jsonResponse = json.decode(response.body);
      return jsonResponse.map((petData) => Pet.fromJson(petData)).toList();
    } else {
      throw Exception('Failed to load nearby pets');
    }
  }
}
