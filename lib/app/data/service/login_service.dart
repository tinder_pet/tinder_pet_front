import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tinder_pet_front/app/modules/home/controllers/user_controller.dart';

class LoginService {
  final String baseUrl = 'http://localhost:3000';
  final UserController userController = Get.find<UserController>();

Future<Map<String, dynamic>?> loginUser({
  required String email,
  required String password,
}) async {
  final url = Uri.parse('$baseUrl/auth/login');
  final headers = {'Content-Type': 'application/json'};

  try {
    final response = await http.post(
      url,
      headers: headers,
      body: jsonEncode({
        'email': email,
        'password': password,
      }),
    );

    if (response.statusCode == 200) {
      final responseData = jsonDecode(response.body);
      final user = responseData['user'];
      // บันทึกข้อมูลผู้ใช้ใน GetStorage
      final box = GetStorage();
      await box.write('userID', user['userID']);
      await box.write('userName', user['userName']);
      await box.write('email', user['email']);
      await box.write('userDes', user['userDes']); // บันทึก userDes
      await box.write('phoneNumber', user['phoneNumber']); // บันทึก phoneNumber
      await box.write('sex', user['sex']); // บันทึก sex
      await box.write('statusMember', user['statusMember']); // บันทึก statusMember
      await box.write('userImage', user['userImage']); // บันทึก userImage
      await box.write('contact', user['contact']); // บันทึก contact
      await box.write('age', user['age']); // บันทึก age
      await box.write('nationality', user['nationality']); // บันทึก nationality
      await box.write('religion', user['religion']); // บันทึก religion
      await box.write('address', user['address']); // บันทึก address
      await box.write('deletedAt', user['deletedAt']); // บันทึก deletedAt
      await box.write('latitude', user['latitude']); // บันทึก latitude
      await box.write('longitude', user['longitude']); // บันทึก longitude
      box.write('pets', user['pets']); // บันทึกข้อมูลสัตว์เลี้ยงใน GetStorage
      box.write('members', user['members']); // บันทึกข้อมูลสมาชิกใน GetStorage

      // หากมีการส่ง token มา ก็สามารถบันทึกได้ที่นี่
      if (responseData['token'] != null) {
        await _saveToken(responseData['token']);
      }

      final keys = box.getKeys();
      for (var key in keys) {
        print('$key: ${box.read(key)}');
      }

      userController.setUser(user); // อัพเดตข้อมูลผู้ใช้ใน UserController

      return user; // คืนค่าข้อมูลผู้ใช้
    } else {
      Get.snackbar('Login Failed', 'Invalid credentials',
          snackPosition: SnackPosition.BOTTOM);
    }
  } catch (e) {
    Get.snackbar('Login Failed', 'An error occurred during login: $e',
        snackPosition: SnackPosition.BOTTOM);
  }
  return null; // คืนค่า null หากการล็อกอินล้มเหลว
}


  Future<void> _saveToken(String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('token', token);
  }
}
