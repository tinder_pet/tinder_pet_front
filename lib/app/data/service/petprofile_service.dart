import 'dart:convert';
import 'dart:typed_data';
import 'package:get_storage/get_storage.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:get/get.dart';
import 'package:tinder_pet_front/app/routes/app_pages.dart';

class PetprofileService {
  final String baseUrl = 'http://localhost:3000';

  Future<void> registerPet({
    required String userid,
    required String petname,
    required String petdes,
    required String petsex,
    required String petage,
    required String petweight,
    required String petcolor,
    required String petfur,
    required String petbreed,
    required Uint8List? petImages,
    required String? species,
    String vaccine = "-",
    String foodsEaten = "-",
    String parentingStatus = "-",
    String sterileState = "-",
    String healthHistory = "-",
    String specialBehaviors = "-",
    String objective = "both",
  }) async {
    final url = Uri.parse('$baseUrl/pet');
    try {
      var request = http.MultipartRequest('POST', url);

      request.fields['petName'] = petname;
      request.fields['petDes'] = petdes;
      request.fields['sex'] = petsex;
      request.fields['age'] = petage;
      request.fields['weight'] = petweight;
      request.fields['color'] = petcolor;
      request.fields['furLength'] = petfur;
      request.fields['breed'] = petbreed;
      request.fields['userID'] = userid;
      request.fields['species'] = species!;
      request.fields['vaccine'] = vaccine;
      request.fields['foodsEaten'] = foodsEaten;
      request.fields['parentingStatus'] = parentingStatus;
      request.fields['sterileState'] = sterileState;
      request.fields['healthHistory'] = healthHistory;
      request.fields['specialBehaviors'] = specialBehaviors;
      request.fields['objective'] = objective;

      if (petImages != null) {
        request.files.add(
          http.MultipartFile.fromBytes(
            'file',
            petImages,
            filename: 'pet_image.jpg',
            contentType: MediaType('image', 'jpeg'),
          ),
        );
      }

      final response = await request.send();

      if (response.statusCode == 201) {
        print('Pet registered successfully');

        final responseData = await response.stream.bytesToString();
        final decodedResponse = jsonDecode(responseData);
        print('Response: $decodedResponse');
        final petId = decodedResponse['petID'];

        // เรียก API เพื่อดึงภาพแรกของสัตว์เลี้ยง
        final imageResponse =
            await http.get(Uri.parse('$baseUrl/image/pet/$petId/first'));

        String? imageFileName;
        if (imageResponse.statusCode == 200) {
          final imageData = jsonDecode(imageResponse.body);
          imageFileName = imageData['image'] ?? null;
        } else {
          print('Failed to get image: ${imageResponse.body}');
        }

        String? imageUrl;
        if (imageFileName != null) {
          imageUrl = 'http://localhost:3000/image/image/$imageFileName';
          print('New image URL: $imageUrl');
        } else {
          print('imageFileName is null');
        }

        final box = GetStorage();
        final newPet = {
          'petName': petname,
          'petDes': petdes,
          'sex': petsex,
          'age': petage,
          'weight': petweight,
          'color': petcolor,
          'furLength': petfur,
          'breed': petbreed,
          'userID': userid,
          'species': species,
          'vaccine': vaccine,
          'foodsEaten': foodsEaten,
          'parentingStatus': parentingStatus,
          'sterileState': sterileState,
          'healthHistory': healthHistory,
          'specialBehaviors': specialBehaviors,
          'objective': objective,
          'imageUrl': imageUrl
        };

        List<dynamic> pets = box.read('pets') ?? [];
        pets.add(newPet);
        print(newPet);
        await box.write('pets', pets);

        Get.toNamed(Routes.HOME);
      } else {
        final responseBody = await response.stream.bytesToString();
        print('Failed to register pet: $responseBody');
      }
    } catch (e) {
      print('Error: $e');
    }
  }

  Future<void> updatePet({
    required int petId,
    String? petname,
    String? petdes,
    String? petsex,
    String? petage,
    String? petweight,
    String? petcolor,
    String? petfur,
    String? petbreed,
    // required String species,
    String? vaccine,
    String? foodsEaten,
    String? parentingStatus,
    String? sterileState,
    String? healthHistory,
    String? specialBehaviors,
    String? objective,
  }) async {
    final url =
        Uri.parse('$baseUrl/pet/$petId'); // URL สำหรับอัปเดตข้อมูลสัตว์เลี้ยง

    try {
      final response = await http.patch(
        url,
        headers: {'Content-Type': 'application/json'},
        body: jsonEncode({
          'petName': petname ?? '',
          'petDes': petdes ?? '',
          'sex': petsex ?? '',
          'age': petage ?? '',
          'weight': petweight ?? '',
          'color': petcolor ?? '',
          'furLength': petfur ?? '',
          'breed': petbreed ?? '',
          // 'species': species,
          'vaccine': vaccine,
          'foodsEaten': foodsEaten,
          'parentingStatus': parentingStatus,
          'sterileState': sterileState,
          'healthHistory': healthHistory,
          'specialBehaviors': specialBehaviors,
          'objective': objective,
        }),
      );

      if (response.statusCode == 200) {
        print('Pet updated successfully');
      } else {
        print('Failed to update pet: ${response.body}');
      }
    } catch (e) {
      print('Error: $e');
    }
  }
}
