class Pet1 {
  final int petID;
  final String petName;
  final String sex;
  final String species;
  final double weight;
  final String color;
  final String furLength;
  final String breed;
  final String petDes;
  final String vaccine;
  final String foodsEaten;
  final String parentingStatus;
  final String sterileState;
  final String healthHistory;
  final String specialBehaviors;
  final String objective;
  final String image;

  Pet1({
    required this.petID,
    required this.petName,
    required this.sex,
    required this.species,
    required this.weight,
    required this.color,
    required this.furLength,
    required this.breed,
    required this.petDes,
    required this.vaccine,
    required this.foodsEaten,
    required this.parentingStatus,
    required this.sterileState,
    required this.healthHistory,
    required this.specialBehaviors,
    required this.objective,
    required this.image,
  });
}
