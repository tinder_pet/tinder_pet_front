class Pet {
  final int petID;
  final String petName;
  final String sex;
  final double age;
  final String species;
  final double weight;
  final String color;
  final String furLength;
  final String breed;
  final String petDes;
  final String vaccine;
  final String foodsEaten;
  final String parentingStatus;
  final String sterileState;
  final String healthHistory;
  final String specialBehaviors;
  final String objective;
  final User user;
  final List<Image> images;
  final String distance;
  final double? compatibility;

  Pet({
    required this.petID,
    required this.petName,
    required this.sex,
    required this.age,
    required this.species,
    required this.weight,
    required this.color,
    required this.furLength,
    required this.breed,
    required this.petDes,
    required this.vaccine,
    required this.foodsEaten,
    required this.parentingStatus,
    required this.sterileState,
    required this.healthHistory,
    required this.specialBehaviors,
    required this.objective,
    required this.user,
    required this.images,
    required this.distance,
    this.compatibility,
  });

factory Pet.fromJson(Map<String, dynamic> json) {
  var petJson = json['pet'];
  return Pet(
    petID: petJson['petID'],
    petName: petJson['petName'],
    sex: petJson['sex'],
    age: petJson['age'],
    species: petJson['species'],
    weight: petJson['weight'].toDouble(),
    color: petJson['color'],
    furLength: petJson['furLength'],
    breed: petJson['breed'],
    petDes: petJson['petDes'],
    vaccine: petJson['vaccine'],
    foodsEaten: petJson['foodsEaten'],
    parentingStatus: petJson['parentingStatus'],
    sterileState: petJson['sterileState'],
    healthHistory: petJson['healthHistory'],
    specialBehaviors: petJson['specialBehaviors'],
    objective: petJson['objective'],
    user: User.fromJson(petJson['user']),
    images: (petJson['images'] as List)
        .map((image) => Image.fromJson(image))
        .toList(),
    distance: json['distance'],
    compatibility: json['compatibility'] != null 
        ? json['compatibility'].toDouble() 
        : null, 
  );
}

}

class User {
  final int userID;
  final String userName;
  final String email;
  final String phoneNumber;
  final String userDes;
  final String latitude;
  final String longitude;
  final String nationality;
  final String religion;
  final String address;
  final int age;

  User({
    required this.userID,
    required this.userName,
    required this.email,
    required this.phoneNumber,
    required this.userDes,
    required this.latitude,
    required this.longitude,
    required this.nationality,
    required this.religion,
    required this.address,
    required this.age,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      userID: json['userID'],
      userName: json['userName'],
      email: json['email'],
      phoneNumber: json['phoneNumber'],
      userDes: json['userDes'],
      latitude: json['latitude'],
      longitude: json['longitude'],
      nationality: json['nationality'],
      religion: json['religion'],
      address: json['address'],
      age: json['age'],
    );
  }
}

class Image {
  final int imageID;
  final String image;

  Image({
    required this.imageID,
    required this.image,
  });

  factory Image.fromJson(Map<String, dynamic> json) {
    return Image(
      imageID: json['imageID'],
      image: json['image'],
    );
  }
}
