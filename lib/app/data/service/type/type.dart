import 'dart:convert';

class PetResponse {
  final int petID;
  final String petName;
  final String sex;
  final String species;
  final int weight;
  final String color;
  final String furLength;
  final String breed;
  final String petDes;
  final String vaccine;
  final String foodsEaten;
  final String parentingStatus;
  final String sterileState;
  final String healthHistory;
  final String specialBehaviors;
  final String objective;
  final dynamic deletedAt;
  final User user;
  final List<Image> images;

  PetResponse({
    required this.petID,
    required this.petName,
    required this.sex,
    required this.species,
    required this.weight,
    required this.color,
    required this.furLength,
    required this.breed,
    required this.petDes,
    required this.vaccine,
    required this.foodsEaten,
    required this.parentingStatus,
    required this.sterileState,
    required this.healthHistory,
    required this.specialBehaviors,
    required this.objective,
    required this.deletedAt,
    required this.user,
    required this.images,
  });

  PetResponse copyWith({
    int? petID,
    String? petName,
    String? sex,
    String? species,
    int? weight,
    String? color,
    String? furLength,
    String? breed,
    String? petDes,
    String? vaccine,
    String? foodsEaten,
    String? parentingStatus,
    String? sterileState,
    String? healthHistory,
    String? specialBehaviors,
    String? objective,
    dynamic deletedAt,
    User? user,
    List<Image>? images,
  }) {
    return PetResponse(
      petID: petID ?? this.petID,
      petName: petName ?? this.petName,
      sex: sex ?? this.sex,
      species: species ?? this.species,
      weight: weight ?? this.weight,
      color: color ?? this.color,
      furLength: furLength ?? this.furLength,
      breed: breed ?? this.breed,
      petDes: petDes ?? this.petDes,
      vaccine: vaccine ?? this.vaccine,
      foodsEaten: foodsEaten ?? this.foodsEaten,
      parentingStatus: parentingStatus ?? this.parentingStatus,
      sterileState: sterileState ?? this.sterileState,
      healthHistory: healthHistory ?? this.healthHistory,
      specialBehaviors: specialBehaviors ?? this.specialBehaviors,
      objective: objective ?? this.objective,
      deletedAt: deletedAt ?? this.deletedAt,
      user: user ?? this.user,
      images: images ?? this.images,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'petID': petID,
      'petName': petName,
      'sex': sex,
      'species': species,
      'weight': weight,
      'color': color,
      'furLength': furLength,
      'breed': breed,
      'petDes': petDes,
      'vaccine': vaccine,
      'foodsEaten': foodsEaten,
      'parentingStatus': parentingStatus,
      'sterileState': sterileState,
      'healthHistory': healthHistory,
      'specialBehaviors': specialBehaviors,
      'objective': objective,
      'deletedAt': deletedAt,
      'user': user.toMap(),
      'images': images.map((x) => x.toMap()).toList(),
    };
  }

  factory PetResponse.fromMap(Map<String, dynamic> map) {
    return PetResponse(
      petID: map['petID'] as int,
      petName: map['petName'] as String,
      sex: map['sex'] as String,
      species: map['species'] as String,
      weight: map['weight'] as int,
      color: map['color'] as String,
      furLength: map['furLength'] as String,
      breed: map['breed'] as String,
      petDes: map['petDes'] as String,
      vaccine: map['vaccine'] as String,
      foodsEaten: map['foodsEaten'] as String,
      parentingStatus: map['parentingStatus'] as String,
      sterileState: map['sterileState'] as String,
      healthHistory: map['healthHistory'] as String,
      specialBehaviors: map['specialBehaviors'] as String,
      objective: map['objective'] as String,
      deletedAt: map['deletedAt'],
      user: User.fromMap(map['user'] as Map<String, dynamic>),
      images: List<Image>.from((map['images'] as List<dynamic>).map((x) => Image.fromMap(x as Map<String, dynamic>))),
    );
  }

  String toJson() => json.encode(toMap());

  factory PetResponse.fromJson(String source) =>
      PetResponse.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() =>
      'PetResponse(petID: $petID, petName: $petName, sex: $sex, species: $species, weight: $weight, color: $color, furLength: $furLength, breed: $breed, petDes: $petDes, vaccine: $vaccine, foodsEaten: $foodsEaten, parentingStatus: $parentingStatus, sterileState: $sterileState, healthHistory: $healthHistory, specialBehaviors: $specialBehaviors, objective: $objective, deletedAt: $deletedAt, user: $user, images: $images)';

  @override
  bool operator ==(covariant PetResponse other) {
    if (identical(this, other)) return true;

    return other.petID == petID &&
        other.petName == petName &&
        other.sex == sex &&
        other.species == species &&
        other.weight == weight &&
        other.color == color &&
        other.furLength == furLength &&
        other.breed == breed &&
        other.petDes == petDes &&
        other.vaccine == vaccine &&
        other.foodsEaten == foodsEaten &&
        other.parentingStatus == parentingStatus &&
        other.sterileState == sterileState &&
        other.healthHistory == healthHistory &&
        other.specialBehaviors == specialBehaviors &&
        other.objective == objective &&
        other.deletedAt == deletedAt &&
        other.user == user &&
        other.images == images;
  }

  @override
  int get hashCode => petID.hashCode ^
      petName.hashCode ^
      sex.hashCode ^
      species.hashCode ^
      weight.hashCode ^
      color.hashCode ^
      furLength.hashCode ^
      breed.hashCode ^
      petDes.hashCode ^
      vaccine.hashCode ^
      foodsEaten.hashCode ^
      parentingStatus.hashCode ^
      sterileState.hashCode ^
      healthHistory.hashCode ^
      specialBehaviors.hashCode ^
      objective.hashCode ^
      deletedAt.hashCode ^
      user.hashCode ^
      images.hashCode;
}

class User {
  final int userID;
  final String userName;
  final String password;
  final String email;
  final String sex;
  final String phoneNumber;
  final String userDes;
  final String statusMember;
  final String userImage;
  final String contact;
  final int age;
  final String nationality;
  final String religion;
  final String address;
  final dynamic deletedAt;
  final String latitude;
  final String longitude;

  User({
    required this.userID,
    required this.userName,
    required this.password,
    required this.email,
    required this.sex,
    required this.phoneNumber,
    required this.userDes,
    required this.statusMember,
    required this.userImage,
    required this.contact,
    required this.age,
    required this.nationality,
    required this.religion,
    required this.address,
    required this.deletedAt,
    required this.latitude,
    required this.longitude,
  });

  User copyWith({
    int? userID,
    String? userName,
    String? password,
    String? email,
    String? sex,
    String? phoneNumber,
    String? userDes,
    String? statusMember,
    String? userImage,
    String? contact,
    int? age,
    String? nationality,
    String? religion,
    String? address,
    dynamic deletedAt,
    String? latitude,
    String? longitude,
  }) {
    return User(
      userID: userID ?? this.userID,
      userName: userName ?? this.userName,
      password: password ?? this.password,
      email: email ?? this.email,
      sex: sex ?? this.sex,
      phoneNumber: phoneNumber ?? this.phoneNumber,
      userDes: userDes ?? this.userDes,
      statusMember: statusMember ?? this.statusMember,
      userImage: userImage ?? this.userImage,
      contact: contact ?? this.contact,
      age: age ?? this.age,
      nationality: nationality ?? this.nationality,
      religion: religion ?? this.religion,
      address: address ?? this.address,
      deletedAt: deletedAt ?? this.deletedAt,
      latitude: latitude ?? this.latitude,
      longitude: longitude ?? this.longitude,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'userID': userID,
      'userName': userName,
      'password': password,
      'email': email,
      'sex': sex,
      'phoneNumber': phoneNumber,
      'userDes': userDes,
      'statusMember': statusMember,
      'userImage': userImage,
      'contact': contact,
      'age': age,
      'nationality': nationality,
      'religion': religion,
      'address': address,
      'deletedAt': deletedAt,
      'latitude': latitude,
      'longitude': longitude,
    };
  }

  factory User.fromMap(Map<String, dynamic> map) {
    return User(
      userID: map['userID'] as int,
      userName: map['userName'] as String,
      password: map['password'] as String,
      email: map['email'] as String,
      sex: map['sex'] as String,
      phoneNumber: map['phoneNumber'] as String,
      userDes: map['userDes'] as String,
      statusMember: map['statusMember'] as String,
      userImage: map['userImage'] as String,
      contact: map['contact'] as String,
      age: map['age'] as int,
      nationality: map['nationality'] as String,
      religion: map['religion'] as String,
      address: map['address'] as String,
      deletedAt: map['deletedAt'],
      latitude: map['latitude'] as String,
      longitude: map['longitude'] as String,
    );
  }

  String toJson() => json.encode(toMap());

  factory User.fromJson(String source) =>
      User.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() =>
      'User(userID: $userID, userName: $userName, password: $password, email: $email, sex: $sex, phoneNumber: $phoneNumber, userDes: $userDes, statusMember: $statusMember, userImage: $userImage, contact: $contact, age: $age, nationality: $nationality, religion: $religion, address: $address, deletedAt: $deletedAt, latitude: $latitude, longitude: $longitude)';

  @override
  bool operator ==(covariant User other) {
    if (identical(this, other)) return true;

    return other.userID == userID &&
        other.userName == userName &&
        other.password == password &&
        other.email == email &&
        other.sex == sex &&
        other.phoneNumber == phoneNumber &&
        other.userDes == userDes &&
        other.statusMember == statusMember &&
        other.userImage == userImage &&
        other.contact == contact &&
        other.age == age &&
        other.nationality == nationality &&
        other.religion == religion &&
        other.address == address &&
        other.deletedAt == deletedAt &&
        other.latitude == latitude &&
        other.longitude == longitude;
  }

  @override
  int get hashCode => userID.hashCode ^
      userName.hashCode ^
      password.hashCode ^
      email.hashCode ^
      sex.hashCode ^
      phoneNumber.hashCode ^
      userDes.hashCode ^
      statusMember.hashCode ^
      userImage.hashCode ^
      contact.hashCode ^
      age.hashCode ^
      nationality.hashCode ^
      religion.hashCode ^
      address.hashCode ^
      deletedAt.hashCode ^
      latitude.hashCode ^
      longitude.hashCode;
}

class Image {
  final int imageID;
  final String image;
  final dynamic deletedAt;

  Image({
    required this.imageID,
    required this.image,
    this.deletedAt,
  });

  Image copyWith({
    int? imageID,
    String? image,
    dynamic deletedAt,
  }) {
    return Image(
      imageID: imageID ?? this.imageID,
      image: image ?? this.image,
      deletedAt: deletedAt ?? this.deletedAt,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'imageID': imageID,
      'image': image,
      'deletedAt': deletedAt,
    };
  }

  factory Image.fromMap(Map<String, dynamic> map) {
    return Image(
      imageID: map['imageID'] as int,
      image: map['image'] as String,
      deletedAt: map['deletedAt'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Image.fromJson(String source) =>
      Image.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() =>
      'Image(imageID: $imageID, image: $image, deletedAt: $deletedAt)';

  @override
  bool operator ==(covariant Image other) {
    if (identical(this, other)) return true;

    return other.imageID == imageID &&
        other.image == image &&
        other.deletedAt == deletedAt;
  }

  @override
  int get hashCode => imageID.hashCode ^ image.hashCode ^ deletedAt.hashCode;
}
