import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:tinder_pet_front/app/data/service/type/pet.dart';

class HomeService {
  final String baseUrl = 'http://localhost:3000';

  Future<bool> checkUserPets(int userId) async {
    final url = Uri.parse('$baseUrl/pet/nearby/$userId');
    final headers = {'Content-Type': 'application/json'};

    try {
      final response = await http.get(url, headers: headers);

      if (response.statusCode == 200) {
        return true; // User has pets
      } else if (response.statusCode == 404) {
        final responseData = jsonDecode(response.body);
        return responseData['message'] != 'User has no pets';
      } else {
        print('Failed to fetch data: ${response.body}');
        return false;
      }
    } catch (e) {
      print('Error: $e');
      return false;
    }
  }

Future<List<Pet>> fetchNearbyPets(int userId, {Map<String, dynamic>? weights}) async {
  final url = Uri.parse('$baseUrl/pet/nearby-compatible/$userId');
  final headers = {'Content-Type': 'application/json'};
  
   final body = jsonEncode(weights != null ? {'weights': weights} : {});
  print('Sending request to $url with body: $body'); // ล็อกข้อมูลที่ส่งไป

  try {
    final response = await http.post(url, headers: headers, body: body);
    
    print('Response status: ${response.statusCode}'); // ล็อกสถานะการตอบกลับ
    print('Response body: ${response.body}'); // ล็อกเนื้อหาการตอบกลับ

    if (response.statusCode == 201) {
      final responseData = jsonDecode(response.body) as List;
      if (responseData.isNotEmpty) {
        List<Pet> petsList = responseData.map((item) {
          if (item != null) {
            return Pet.fromJson(item);
          } else {
            print('Received null item from API');
            return null; 
          }
        }).whereType<Pet>().toList(); 

        return petsList; 
      } else {
        print('No pets found in response data');
        return [];
      }
    } else {
      print('Failed to fetch data: ${response.body}');
      return [];
    }
  } catch (e) {
    print('Error: $e');
    return [];
  }
}


}
